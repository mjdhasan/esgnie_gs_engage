# ESGnie API

This is an API for unstructured data sourcing and processing, that can take source unstructured documents from the web,
extract and structure relevant data, and store the structured data in Athena tables that can be connected to BI tools.

It is designed to perform the following key tasks:
1) Sourcing corporate disclosures, such as sustainability and annual reports
2) Processing unstructured pdf documents
3) Standardizing quntitative variable names and units
4) Text categorisation and analysis using QA models
5) Creating AWS Athena databases to store and query data for BI
6) An API to expose these modules to the end-user through various endpoints

More details on each module
1) Document Sourcingis handled in 'download_genie' module
    - download_genie has a few key submodules including
        + find_homepage.py: identifies the official url of a company, given a company name
        + download_files.py: searches the company's official url for disclosures related to a given keyword
2) Document processing is handled by 'pdf_genie' module
    - pdf_genie has a few key submodules inlcluding
        + text extraction from pdf files, along with the text coordinates (x0, y0, x1, y1)
        + text segmentation based on the layout analysis of the pdf files to separate passages, tables, etc
        + identification of quantitative values
        + structuring quantitative values into a standardized form (variable, unit, date, value, pagenum)
3) Variable unit standardization is done by 'std_genie' module
    - std_genie module has a few key submodules inlcluding
        +  predict_var_unit_std, which predicts the standardized variable name and unit for a given input string.
4) Text categorisation & analysis is handled by 'qa' module
    - qa module has a few key submodules including
        + extractive_qa, which runs extractive question answering
        + implied_qa, which checks whether a passage implies something or not
        + mixed_qa, which can run a variety of QA tasks, such as extractive QA, multiple choice, etc.
5) Cloud data storage is handled by 'aws_genie' module
    - aws_genie module has a few key submodules, including
        + db_upload, which uploads data from a local dir to s3 in parquet format, and creates athena tables out of it
        + aws_genie includes utility functions to recognise column type, even if all values are saved as string
        + aws_genie includes utility functions to standardise column names across data files based on column values
6) API code is handled by main.py, which creates all the end points
