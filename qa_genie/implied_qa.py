from init import DIR_MODELS
from allennlp.predictors.predictor import Predictor
import allennlp_models.tagging
import dill
import numpy as np


## roberta snli
with open(f'{DIR_MODELS}/textual_entailment/roberta_snli_textual_entailment.pkl', 'rb') as file:
    roberta_snli = dill.load(file)
    file.close()


def implied_qa(q, passage, model='roberta_snli'):
    answer = eval(model).predict(q, passage)
    if 'label' in answer.keys():
        answer_text = answer['label']
    if 'probs' in answer.keys():
        prob_names = ['entailment', 'contradiction', 'neutral']
        dict_probs = {}
        for i in np.arange(0, len(prob_names)):
            dict_probs[prob_names[i]] = answer['probs'][i]
        if 'label' in answer.keys():
            label_score = dict_probs[answer['label']]
    return {'passage': passage,
            'q': q,
            'answer': answer_text,
            'score': label_score,
            'type': 'implied_qa'}