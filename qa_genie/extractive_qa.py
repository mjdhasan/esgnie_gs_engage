
from init import DIR_MODELS
import dill
from allennlp.predictors.predictor import Predictor
## transformer_qa
with open(f'{DIR_MODELS}/transformer_qa/transformer_qa.pkl', 'rb') as file:
    transformer_qa = dill.load(file)
    file.close()


def extractive_qa(q, passage, model='transformer_qa'):
    answer = eval(model).predict(q, passage)
    if 'best_span_str' in answer.keys():
        answer_text = answer['best_span_str']
    if 'best_span_scores' in answer.keys():
        answer_score = answer['best_span_scores']
    return {'passage': passage,
            'q': q,
            'answer': answer_text,
            'score': answer_score,
            'type': 'extractive_qa'}
