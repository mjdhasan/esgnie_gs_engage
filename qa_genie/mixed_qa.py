
import requests

API_URL = "" ## proprietary model url remove
headers = "" ## proprietary model access headers removed


def mixed_qa(q, passage):
    payload = f"passage: {passage}. \n\n {q}"
    response = requests.post(API_URL, headers=headers, json=payload)
    response = response.json()
    if response:
        answer = {'answer': response[0]['generated_text']}
    else:
        answer = {'answer': 'answer not found'}
    return answer
