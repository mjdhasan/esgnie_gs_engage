import pandas as pd
import numpy as np
import xgboost as xgb
import pickle
import boto3
from io import StringIO
import json
from sklearn.model_selection import train_test_split
from sentence_transformers import SentenceTransformer


# Function to get a model
def get_unpickled_model(location):
    ## propritary model location removed
    pass


# Function to get a csv file from s3 bucket
def get_data(bucket, file_path, s3_client=s3_client):
    csv_string = s3_client.get_object(Bucket=bucket, Key=file_path)['Body'].read().decode('utf-8')
    return csv_string



def predict_result(csv_or_dataframe, prediction):
    """
    Predict standardized variable names and units for a given input
    :param csv_or_dataframe:
    :param prediction:
    :return:
    """
    df_results = pd.DataFrame()
    # if trying to predict unit_std
    if prediction == 'unit_std':
        ## propriety unit_std prdiction code removed
        pass
    # if trying to predict variable_std
    elif prediction == 'variable_std':
        ## proprietary variable_std prediction code removed
        pass
    # if trying to predict variable_subcategory
    elif prediction == 'variable_subcategory':
        ## proprietary variable_subcategory prediction code removed
        pass
    # if trying to predict variable_subcategory_2
    elif prediction == 'variable_subcategory_2':
        ## proprietary variable_subcategory_2 prediction code removed
        pass
    # Unknown 'prediction' input by the user
    else:
        df_results = pd.DataFrame()
        print(
            "Unknown 'prediction' input for predict_result(csv_or_dataframe, prediction)."
            "\nPlease use either 'unit_std' or 'variable_std' or 'variable_subcategory' or 'variable_subcategory_2' "
            "as the 'prediction' input!")
    return df_results

