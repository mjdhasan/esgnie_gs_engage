import platform


if 'Linux' in platform.platform():
    HOME_DIR = '/ebs-0'
    platform_user = ''
    ## dir for web scraping output
    DIR_SCRAPERS = f'{HOME_DIR}/data'
elif 'i386-64bit' in platform.platform():
    ROOT_DIR = f'~/data'
    platform_user = 'majid'
    ## dir for web scraping output
    DIR_SCRAPERS = f'/Users/{platform_user}/Dropbox/data/scrapers'

## selected sectors for demo data
l_sics_sector = ['Food & Beverage',
                 'Infrastructure',
                 'Resource Transformation',
                 'Renewable Resources & Alternative Energy',
                 'Extractives & Minerals Processing']