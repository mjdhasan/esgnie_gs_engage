"""
Sync local dir with s3
"""
import argparse
import subprocess
from init import DIR_SCRAPERS
import os



if __name__ == "__main__":
    ## Create the parser
    arg_parser = argparse.ArgumentParser(prog='sync_with_s3',
                                         usage='%(prog)s [options]',
                                         description='sync local dir with s3')
    ## add arg: root dir
    arg_parser.add_argument('-r', '--root_dir',
                            metavar='root_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/app.esgbook.com',
                            help='root dir')
    ## add arg: sync from/to s3
    arg_parser.add_argument('-t', '--to_dir',
                            metavar='to_dir',
                            nargs='?',
                            type=str,
                            default=f'',
                            help='s3 dir to sync data to')
    ## add arg: sync from/to s3
    arg_parser.add_argument('-f', '--from_dir',
                            metavar='from_dir',
                            nargs='?',
                            type=str,
                            default=f'',
                            help='s3 dir to sync data from')
    ## parse args
    args, unknown_args = arg_parser.parse_known_args()
    print(args)
    ## set root dir
    root_dir = args.root_dir
    os.makedirs(root_dir, exist_ok=True)
    ## set to_dir
    to_dir = args.to_dir
    ## set from_dir
    from_dir = args.from_dir
    ## sync dir from s3
    if from_dir != '':
        subprocess.call(['aws', 's3', 'sync', from_dir, root_dir])
    ## sync dir to s3
    if to_dir != '':
        subprocess.call(['aws', 's3', 'sync', root_dir, to_dir])

## /ebs-0/venvs/aws-genie/python /ebs-0/code/aws-genie/sync_with_s3.py -r /ebs-0/data/app.esgbook.com -f s3://app.esgbook.com