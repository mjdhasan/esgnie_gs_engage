from init import DIR_SCRAPERS
from utils.util import presign_s3_url, list_full_path
import pandas as pd
import os
import argparse


if __name__ == "__main__":
    ## Create the parser
    arg_parser = argparse.ArgumentParser(prog='presign_urls',
                                         usage='%(prog)s [options]',
                                         description='s3 tasks')
    ## add arg: root dir
    arg_parser.add_argument('-r', '--root_dir',
                            metavar='root_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/green_climate_fund',
                            help='root dir')
    ## add arg: root dir
    arg_parser.add_argument('-o', '--output_dir',
                            metavar='output_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/green_climate_fund/_uploads/csv/_presigned_urls',
                            help='root dir')
    ## add arg: root dir
    arg_parser.add_argument('-b', '--s3_bucket',
                            metavar='s3_bucket',
                            nargs='?',
                            type=str,
                            default='www.greenclimate.fund',
                            help='s3 bucket to search files in')
    ## add arg: file extensions
    arg_parser.add_argument('-e', '--ext',
                            metavar='ext',
                            nargs='*',
                            type=str,
                            default=['.png'],
                            help='file extension')
    ## add arg: file keyword
    arg_parser.add_argument('-k', '--keywords',
                            metavar='keywords',
                            nargs='*',
                            type=str,
                            default=['/img/'],
                            help='keywords to search for in file path')
    ## add arg: file keyword
    arg_parser.add_argument('-t', '--time_expiry',
                            metavar='time_expiry',
                            nargs='?',
                            type=int,
                            default=604800,
                            help='expiry time for pre-signed url')
    ## add arg: prefix for text file
    arg_parser.add_argument('-v', '--verbose',
                            metavar='verbose',
                            nargs='?',
                            type=bool,
                            default=False,
                            help='run in verbose mode')
    ## add arg: parallel
    arg_parser.add_argument('-p', '--parallel',
                            metavar='parallel',
                            nargs='?',
                            type=int,
                            default=1,
                            help='run in parallel mode')
    ## add arg: n_cpu frac
    arg_parser.add_argument('-n', '--n_cpu_frac',
                            metavar='n_cpu_frac',
                            nargs='?',
                            type=float,
                            default=0.5,
                            help='number of CPUs to use for parallel processing')
    ## add arg: max files to process
    arg_parser.add_argument('-m', '--max_files',
                            metavar='max_files',
                            nargs='?',
                            type=int,
                            default=-1,
                            help='number of CPUs to use for parallel processing')
    ## add arg: reverse mode
    arg_parser.add_argument('-rev', '--reverse',
                            metavar='reverse',
                            nargs='?',
                            type=bool,
                            default=False,
                            help='loop files in reverse order')
    ## parse args
    args, unknown_args = arg_parser.parse_known_args()
    print(args)
    ## set root dir
    root_dir = args.root_dir
    ## set root dir
    output_dir = args.output_dir
    os.makedirs(output_dir, exist_ok=True)
    ## set root dir
    s3_bucket = args.s3_bucket
    ## set root dir
    extensions = args.ext
    ## set root dir
    keywords = args.keywords
    ## set root dir
    time_expiry = args.time_expiry
    ## set root dir
    verbose = args.verbose
    ## max_files
    max_files = args.max_files
    ## parallel flag
    parallel = bool(args.parallel)
    ## parallel flag
    n_cpu_frac = args.n_cpu_frac
    ## set reverse flag
    reverse = args.reverse
    """
    Generate pre-signed urls for all images
    """
    ## read all png files locally
    l_files = [file for file in list_full_path(root_dir, recursive=True)
               if (os.path.splitext(file)[1] in extensions)]
    l_files = [file for file in l_files if any([keyword in file for keyword in keywords])]
    l_keys = [file.replace(root_dir + '/', '') for file in l_files]
    ## generate pre-signed urls
    l_url = []
    for key_num, key in enumerate(l_keys):
        if verbose:
            print(f"{key_num}/{len(l_keys)}: {key}")
        try:
            url = presign_s3_url(bucket=s3_bucket, key=key, expires_in=time_expiry)
            l_url = l_url + [url]
        except Exception as e:
            if verbose:
                print('unable to get pre-signed url')
                print(e)
    ## create df
    df_url = pd.DataFrame()
    df_url['file'] = l_files
    df_url['key'] = l_keys
    df_url['url'] = l_url
    df_url['bucket'] = s3_bucket
    df_url['img'] = [f'<img src="{url}" width="400" height="450"/>' for url in df_url['url']]
    ## write to csv
    df_url.to_csv(f"{output_dir}/pre-signed_urls.csv", index=False)
    # ## example call
    # python
    # presigned_urls.py -r /Users/majid/Dropbox/data/scrapers/green_climate_fund -o /Users/majid/Dropbox/data/scrapers/green_climate_fund/_uploads/csv -b
    # www.greenclimate.fund -e .png -k /img/ -v True

