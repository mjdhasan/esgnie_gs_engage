"""
Upload data to demo db
"""

import argparse
import os
import time
import re
import boto3
from init import DIR_SCRAPERS, l_sics_sector
import pandas as pd
import numpy as np
from utils.util import take_unique_list, list_full_path
from utils.util import write_to_s3, get_col_types, get_numeric_value_and_unit
from aws_genie.utils.util import clean_col, convert_cols_to_numeric, list_athena_tables, read_df, merge_categories, \
    create_table, check_query_status, agg_by_categories, create_athena_query


def create_athena_tables(root_dir,
                         const_file,
                         bucket,
                         s3_root,
                         db_name,
                         catalog_name):
    """
    Uploads data to athena tables. Table names by design contain a version number.
    New tables are created by increasing the version number by 1. If a table does not exit,
    it is saved as {table_name}_v0

    :param root_dir: local root dir where data files are saved
    :param const_file: constituent file, with static info to be merged with uploaded data
    :param bucket: s3 bucket to upload data to
    :param s3_root: s3 root key to upload data to
    :param db_name: athen db name to create tables in
    :param catalog_name: aws catalog name for db_name
    :return:
    """
    ## get a dictionary of files stored in root_dir
    l_files = list_full_path(root_dir, recursive=True)
    dict_files = {}
    for file in l_files:
        filename = os.path.split('/')[-1]
        dict_files[filename] = file
    ## loop over dict_files, and read df
    dict_df = read_df(dict_files)
    ## merge any themes (revenue, emissions, climate) on each df, by file
    l_files = list_full_path(f"{root_dir}/_lists")
    for file in l_files:
        df_themes = pd.read_csv(file)
        dict_df = merge_categories(dict_df, df_themes, on=['file'], how='left')
    ## merge sics
    df_cdp_sics = pd.read_csv(const_file)
    df_cdp_sics.columns = [clean_col(col) for col in df_cdp_sics.columns]
    ## merge any company categories, like sectors, industries on the data
    dict_df = merge_categories(dict_df, df_cdp_sics, on=['company_name'], how='left')
    ## convert all cols to numeric types
    dict_df = convert_cols_to_numeric(dict_df)
    ## upload dict_df to s3, and create athena table
    df_tables = list_athena_tables(db_name=db_name, catalog_name=catalog_name)
    dict_response = {}
    for key_num, key in enumerate(list(dict_df.keys())):
        print(f"{key_num}/{len(list(dict_df.keys()))}: {key}")
        ## set upload key
        upload_key = dict_df[key]['file'].unique().tolist()[0].split('/')[-1].replace('.csv', '')
        ## upload data to s3
        write_to_s3(dict_df[key],
                    bucketName=bucket,
                    keyName=f'{s3_root}/{key}/{upload_key}.parquet',
                    format='.parquet')
        ## create ddl query
        if df_tables[df_tables['name_base'] == key].shape[0] > 0:
            name_ver_latest = int(df_tables[df_tables['name_base'] == key]['name_ver_latest'].max() + 1)
        else:
            name_ver_latest = 0
        athena_table_name = f'{db_name}.{key}_v{name_ver_latest}'
        s3_loc = f's3://{bucket}/{s3_root}/{key}/'
        query = create_athena_query(dict_df[key], athena_table_name, s3_loc)
        ## create table
        response = create_table(query)
        ## add response to dict_response
        dict_response[key] = response


if __name__ == "__main__":
    ## Create the parser
    arg_parser = argparse.ArgumentParser(prog='upload data to athena',
                                         usage='%(prog)s [options]',
                                         description='find the homepage of a given entity')
    ## add arg: root dir
    arg_parser.add_argument('-r', '--root_dir',
                            metavar='root_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/_analysis',
                            help='root dir')
    ## add arg: root dir
    arg_parser.add_argument('-db', '--db_dir',
                            metavar='db_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/_db',
                            help='db dir')
    ## add arg: list of companies
    arg_parser.add_argument('-l', '--list_of_companies',
                            metavar='list_of_companies',
                            nargs='?',
                            type=str,
                            default=f"{DIR_SCRAPERS}/sector-maps/cdp/cdp_sics_maps.csv",
                            help='the path to a csv file containing the list of companies')
    ## add arg: root dir
    arg_parser.add_argument('-b', '--bucket',
                            metavar='bucket',
                            nargs='?',
                            type=str,
                            default='db-genie',
                            help='s3 bucket to upload data to')
    ## add arg: s3 data upload key
    arg_parser.add_argument('-s', '--s3_root',
                            metavar='s3_root',
                            nargs='?',
                            type=str,
                            default='demo/data/esg/env',
                            help='s3 root for uploading files')
    ## add arg: s3 data upload key
    arg_parser.add_argument('-db', '--db_name',
                            metavar='db_name',
                            nargs='?',
                            type=str,
                            default='esg_data_demo',
                            help='athena database name')
    ## add arg: verbose
    arg_parser.add_argument('-v', '--verbose',
                            metavar='verbose',
                            nargs='?',
                            type=int,
                            default=1,
                            help='verbose mode')
    ## parse args
    args, unknown_args = arg_parser.parse_known_args()
    print(args)
    ## set root dir
    root_dir = args.root_dir
    ## set db dir
    db_dir = args.db_dir
    ## set db dir
    bucket = args.bucket
    ## set db dir
    s3_root = args.s3_root
    ## set set key for s3 data upload
    key_data = args.key_data
    ## set set key for s3 data upload
    db_name = args.db_name
    ## set set key for s3 data upload
    catalog_name = args.catalog_name
    ## verbose mode
    verbose = bool(args.verbose)
    ## constituents file
    const_file = args.list_of_companies
    """
    Create athena tables for local data files stored in root_dir
    """
    create_athena_tables(root_dir=root_dir,
                         const_file=const_file,
                         bucket=bucket,
                         s3_root=s3_root,
                         db_name=db_name,
                         catalog_name=catalog_name)
    ## list athena tables after new uploads
    df_tables = list_athena_tables(db_name=db_name, catalog_name=catalog_name)

