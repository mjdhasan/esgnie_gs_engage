import boto3
import os
import io
import re
import pandas as pd
import numpy as np
from collections import Counter
from quantulum3 import parser as qparser
import time


def clean_col(col):
    col = col.replace('?', '')
    col = col.replace('-', '_')
    col = col.replace(',', '_')
    col = col.replace(' ', '_')
    col = col.replace('(', '')
    col = col.replace(')', '')
    col = col.replace(']', '')
    col = col.replace('[', '')
    col = col.replace(':', '')
    col = col.replace('/', '_or_')
    col = col.replace('%', 'pct')
    col = col.replace('+', 'and')
    col = col.lower()
    col = re.sub('_$', '', col)
    col = re.sub('\.$', '', col)
    col = re.sub('or_or', 'or', col)
    col = re.sub('__', '_', col)
    return col


def remove_non_ascii_cols(df_merged):
    l_col = []
    l_col_nonascii = []
    for col in df_merged.columns:
        l_nonascii = [chr for chr in col if not chr.isascii()]
        for chr in l_nonascii:
            if chr in col:
                l_col_nonascii = l_col_nonascii + [col]
                col = col.replace(chr, '-')
        l_col = l_col + [col]
    l_col_nonascii = take_unique_list(l_col_nonascii)
    df_merged = df_merged.drop(columns=l_col_nonascii)
    return df_merged


def list_athena_tables(db_name='esg_data_demo',
                       catalog_name='AWSDataCatalog'):
    athena_client = boto3.client('athena')
    ## list table metadata
    resp = athena_client.list_table_metadata(CatalogName=catalog_name,
                                             DatabaseName=db_name)
    # len(resp['TableMetadataList'])
    df_table = pd.DataFrame()
    df_table['name'] = [table['Name'] for table in resp['TableMetadataList']]
    df_table['name_ver'] = [int(name.split('_v')[-1]) for name in df_table['name']]
    df_table['name_base'] = [name.split('_v')[0] for name in df_table['name']]
    df_table['name_ver_latest'] = df_table.groupby(['name_base'])['name_ver'].transform(max)
    return df_table


def create_athena_query(df, athena_table_name, s3_loc):
    query = f'CREATE EXTERNAL TABLE {athena_table_name} (\n '
    for col_num, col in enumerate(df.columns):
        if df[col].dtype in ['object', 'str']:
            col_type_ = 'STRING'
        elif df[col].dtype in ['float']:
            col_type_ = 'DOUBLE'
        query = query + f"{col} {col_type_}"
        if col_num < len(df.columns) - 1:
            query = query + ',\n '
        else:
            query = query + '\n)\n'
    query = query + f'STORED AS PARQUET \n LOCATION \'{s3_loc}\'\n' \
                    f'tblproperties ("parquet.compression"="SNAPPY");'
    return query


def create_table(query, output_location='s3://athena-genie/esg_data_demo/queries/'):
    athena_client = boto3.client('athena')
    resp = athena_client.start_query_execution(
        QueryString=query,
        ResultConfiguration={'OutputLocation': output_location}
    )
    return resp


def check_query_status(response):
    athena_client = boto3.client('athena')
    execution = athena_client.get_query_execution(QueryExecutionId=response['QueryExecutionId'])
    # status = execution['QueryExecution']['Status']['State']
    # print(status)
    return execution


def delete_old_tables(db_name='esg_data_demo',
                      catalog_name='AWSDataCatalog',
                      output_location='s3://athena-genie/esg_data/queries/'):
    athena_client = boto3.client('athena')
    ## list table metadata
    resp = athena_client.list_table_metadata(CatalogName=catalog_name,
                                             DatabaseName=db_name)
    len(resp['TableMetadataList'])
    df_table = pd.DataFrame()
    df_table['name'] = [table['Name'] for table in resp['TableMetadataList']]
    df_table['name_ver'] = [int(name.split('_v')[-1]) for name in df_table['name']]
    df_table['name_base'] = [name.split('_v')[0] for name in df_table['name']]
    df_table['name_ver_latest'] = df_table.groupby(['name_base'])['name_ver'].transform(max)
    l_name = df_table[df_table['name_ver'] < df_table['name_ver_latest']]['name'].unique().tolist()
    dict_resp = {}
    for table_rm in l_name:
        resp = athena_client.start_query_execution(
            QueryString=f'DROP TABLE {table_rm}',
            QueryExecutionContext={
                'Database': db_name
            },
            ResultConfiguration={'OutputLocation': output_location}
        )
        dict_resp[table_rm] = resp
        time.sleep(5)
    return dict_resp


def merge_categories(dict_df, df_categories, on=None, how='left'):
    if on is None:
        on = ['file']
    for key in dict_df.keys():
        dict_df[key] = pd.merge(left=dict_df[key],
                                right=df_categories,
                                on=on,
                                how=how)
    return dict_df


def convert_cols_to_numeric(dict_df):
    for key_num, key in enumerate(dict_df.keys()):
        print(f"{key_num}/{len(dict_df.keys())}: {key}")
        ## get column types
        df_col_types = get_col_types(dict_df[key])
        ## set digit types to float
        for col in df_col_types[df_col_types['col_type'] == 'digit']['col_name'].unique().tolist():
            try:
                l_val, l_unit = get_numeric_value_and_unit(dict_df[key][col].tolist())
                dict_df[key][col] = l_val
            except ValueError as e:
                print(col)
                print(e)
    return dict_df


def agg_by_categories(df, cat_cols, agg_funcs, numeric_cols=None):
    ## identify numeric colums
    if numeric_cols is None:
        df_col_types = get_col_types(df)
        numeric_cols = df_col_types[df_col_types['col_type'] == 'digit']['col_name'].unique().tolist()
    for num_col in numeric_cols:
        for cat_col in cat_cols:
            for f_agg_name in agg_funcs:
                output_col_name = f"{num_col}_{f_agg_name}_by_{cat_col}"
                if re.search('\d+q', f_agg_name, re.IGNORECASE):
                    q = int(f_agg_name.lower().split('q')[0]) / 100
                    df[output_col_name] = \
                        df.groupby([cat_col])[num_col].transform(lambda x: np.quantile(x, q))
                elif 'mean' in f_agg_name:
                    df[output_col_name] = \
                        df.groupby([cat_col])[num_col].transform(lambda x: np.nanmean(x))
                elif 'sd' in f_agg_name:
                    df[output_col_name] = \
                        df.groupby([cat_col])[num_col].transform(lambda x: np.nanstd(x))
                elif 'sum' in f_agg_name:
                    df[output_col_name] = \
                        df.groupby([cat_col])[num_col].transform(lambda x: np.nansum(x))
                elif 'min' in f_agg_name:
                    df[output_col_name] = \
                        df.groupby([cat_col])[num_col].transform(lambda x: np.nanmin(x))
                elif 'max' in f_agg_name:
                    df[output_col_name] = \
                        df.groupby([cat_col])[num_col].transform(lambda x: np.nanmax(x))
    return df


def benchmark_values(df, cat_cols, agg_funcs, numeric_cols=None):
    ## identify numeric colums
    if numeric_cols is None:
        df_col_types = get_col_types(df)
        numeric_cols = df_col_types[df_col_types['col_type'] == 'digit']['col_name'].unique().tolist()
    for num_col in numeric_cols:
        for cat_col in cat_cols:
            for f_agg_name in agg_funcs:
                output_col_name = f"{num_col}_relative_to_{cat_col}_{f_agg_name}"
                if re.search('\d+q', f_agg_name, re.IGNORECASE):
                    q = int(f_agg_name.lower().split('q')[0]) / 100
                    df[output_col_name] = \
                        df[num_col] / df.groupby([cat_col])[num_col].transform(lambda x: np.quantile(x, q)) - 1
                elif 'mean' in f_agg_name:
                    df[output_col_name] = \
                        df[num_col] / df.groupby([cat_col])[num_col].transform(lambda x: np.nanmean(x)) - 1
                elif 'sd' in f_agg_name:
                    df[output_col_name] = \
                        df[num_col] / df.groupby([cat_col])[num_col].transform(lambda x: np.nanstd(x)) - 1
                elif 'sum' in f_agg_name:
                    df[output_col_name] = \
                        df[num_col] / df.groupby([cat_col])[num_col].transform(lambda x: np.nansum(x)) - 1
                elif 'min' in f_agg_name:
                    df[output_col_name] = \
                        df[num_col] / df.groupby([cat_col])[num_col].transform(lambda x: np.nanmin(x)) - 1
                elif 'max' in f_agg_name:
                    df[output_col_name] = \
                        df[num_col] / df.groupby([cat_col])[num_col].transform(lambda x: np.nanmax(x)) - 1
    return df


def read_df(dict_files: dict):
    dict_df = {}
    for key, file in dict_files.items():
        dict_df[key] = pd.read_csv(file, low_memory=False)
        dict_df[key]['file'] = file
        ## get non-ascii columns
        dict_df[key] = remove_non_ascii_cols(dict_df[key])
        ## clean col names
        dict_df[key].columns = [clean_col(col) for col in dict_df[key].columns]
        ## rename database to db_name
        dict_df[key] = dict_df[key].rename(columns={'database': 'db_name'})
        ## drop duplicate columns
        dict_df[key] = dict_df[key].loc[:, (~dict_df[key].columns.duplicated()).tolist()]
    return dict_df


def get_numeric_value_and_unit(l_value):
    l_num_value = []
    l_unit = []
    ## proprietary function (to extract numeric value and unit from string columns) removed
    return l_num_value, l_unit


def classify_value_type(val, dgt_th_up=0.7, dgt_th_lo=0.1):
    val = str(val)
    ## proprietary function (to get value type: numeric, chr, int from a given string) removed
    return val_type


def get_first_quants_in_list(l_val):
    l_quants = []
    for val in l_val:
        quants_ = qparser.parse(str(val))
        if quants_:
            quants_ = [quants_[0].value]
            l_quants = l_quants + quants_
    return l_quants


def classify_col_type(l_col_val,
                      dgt_th_up_for_val=0.7,
                      dgt_th_lo_for_val=0.1,
                      dgt_th_up_for_col=0.7,
                      dgt_th_lo_for_col=0.1,
                      th_for_seq=0.7):
    def get_prop(type):
        prop = df_val_type[df_val_type['type'] == type]['prop'].tolist()
        if prop:
            prop = prop[0]
        else:
            prop = 0
        return prop

    l_val_type = [classify_value_type(val, dgt_th_up_for_val, dgt_th_lo_for_val) for val in l_col_val]
    df_val_type = pd.DataFrame(list(Counter(l_val_type).items()),
                               columns=['type', 'count'])
    df_val_type['tot_count'] = df_val_type['count'].sum()
    df_val_type['prop'] = df_val_type['count'] / df_val_type['tot_count']
    prop_dgt = get_prop('digit')
    prop_chr = get_prop('chr')
    prop_chrdgt = get_prop('chr-digit')
    l_prop = [prop_dgt, prop_chr, prop_chrdgt]
    if prop_dgt == max(l_prop):
        col_type = 'digit'
    elif prop_chr == max(l_prop):
        if prop_dgt >= dgt_th_up_for_col:
            col_type = 'digit'
        elif prop_dgt <= dgt_th_lo_for_col:
            col_type = 'chr'
        else:
            ## check if the numbers are in a sequence, if so set it as chr type
            l_first_quants = get_first_quants_in_list(l_col_val)
            if sum(np.diff(l_first_quants) == 1) >= th_for_seq * len(l_first_quants):
                col_type = 'chr'
            else:
                col_type = 'digit'
    elif prop_chrdgt == max(l_prop):
        ## here we need to check if characters are of type unit
        col_type = 'chr-digit'
    else:
        col_type = 'misc'
    return col_type


def get_col_types(df_table,
                  dgt_th_up_for_val=0.7,
                  dgt_th_lo_for_val=0.1,
                  dgt_th_up_for_col=0.7,
                  dgt_th_lo_for_col=0.1,
                  th_for_seq=0.7,
                  verbose=False):
    df_coltype = pd.DataFrame()
    for col_num, col in enumerate(df_table.columns):
        if verbose:
            print(f"{col_num}/{len(df_table.columns)}: {col}")
        # df_table[col].dtype
        ## find non-null values in the column
        l_col_val = df_table[df_table[col].notnull()][col].tolist()
        ## find col type
        col_type = classify_col_type(l_col_val,
                                     dgt_th_up_for_val=dgt_th_up_for_val,
                                     dgt_th_lo_for_val=dgt_th_lo_for_val,
                                     dgt_th_up_for_col=dgt_th_up_for_col,
                                     dgt_th_lo_for_col=dgt_th_lo_for_col,
                                     th_for_seq=th_for_seq)
        ## df of col type
        df = pd.DataFrame({'col_name': [col], 'col_type': [col_type]})
        ## append to df_coltype
        df_coltype = pd.concat([df_coltype, df], axis=0).reset_index(drop=True) #  df_coltype.append(df)
    ## reset index
    df_coltype = df_coltype.reset_index(drop=True)
    return df_coltype


def write_csv_to_s3(df, file, bucket):
    s3_client = boto3.client("s3")
    with io.StringIO() as csv_buffer:
        df.to_csv(csv_buffer, index=False)
        response = s3_client.put_object(
            Bucket=bucket, Key=file, Body=csv_buffer.getvalue()
        )
        status = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
    return status


def write_to_s3(df, format, bucketName, keyName):
    s3_client = boto3.client("s3")
    if format == '.parquet':
        out_buffer = io.BytesIO()
        df.to_parquet(out_buffer, index=False)
    elif format == '.csv':
        out_buffer = io.StringIO()
        df.to_parquet(out_buffer, index=False)
    response = s3_client.put_object(Bucket=bucketName,
                                    Key=keyName,
                                    Body=out_buffer.getvalue())
    status = response.get("ResponseMetadata", {}).get("HTTPStatusCode")
    return status


def take_unique_list(seq) -> list:
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


def list_full_path(d, recursive=False):
    l_full_path = []
    for path in os.listdir(d):
        if recursive and os.path.isdir(os.path.join(d, path)):
            # print(path)
            l_full_path = l_full_path + list_full_path(d=os.path.join(d, path), recursive=recursive)
        else:
            l_full_path = l_full_path + [os.path.join(d, path)]
    return l_full_path


def presign_s3_url(bucket, key, expires_in=604800, region='us-west-2'):
    ## Get the service client.
    s3 = boto3.client('s3')
    response = s3.get_bucket_location(Bucket=bucket)
    s3 = boto3.client('s3', region_name=response['LocationConstraint'])
    ## Generate the URL to get 'key-name' from 'bucket-name'
    url = s3.generate_presigned_url(
        ClientMethod='get_object',
        Params={
            'Bucket': bucket,
            'Key': key
        },
        ExpiresIn=expires_in
    )
    return url