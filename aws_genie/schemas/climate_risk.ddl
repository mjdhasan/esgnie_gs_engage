CREATE EXTERNAL TABLE esg_data_demo.climate_risk_v1 (
    report_name STRING,
    company_name STRING,
    identifier STRING,
    where_in_the_value_chain_does_the_risk_driver_occur STRING,
    climate_risk_type_mapped_to_traditional_financial_services_industry_risk_classification STRING,
    company_specific_description STRING,
    time_horizon STRING,
    likelihood STRING,
    magnitude_of_impact STRING,
    description_of_response_and_explanation_of_cost_calculation STRING,
    comment STRING,
    risk_type STRING,
    primary_climate_related_risk_driver STRING,
    potential_financial_impact STRING,
    explanation_of_financial_impact STRING,
    management_method STRING,
    cost_of_management STRING,
    type_of_financial_impact STRING,
    question_number STRING,
    question_text STRING,
    company_risk_id STRING,
    theme STRING,
    sub_theme STRING,
    db_name STRING,
    framework STRING,
    report_year STRING
)
STORED AS PARQUET
LOCATION 's3://db-genie/demo/data/esg/env/climate_risk/'
tblproperties ("parquet.compression"="SNAPPY");