CREATE EXTERNAL TABLE esg_data_demo.emission_intensity_v0 (
    report_name STRING,
    company_name STRING,
    section_label STRING,
    pct_change_from_previous_year STRING,
    direction_of_change STRING,
    intensity_figure STRING,
    metric_denominator STRING,
    metric_denominator_unit_total STRING,
    metric_numerator_gross_global_combined_scope_1_and_2_emissions_metric_tons_co2e STRING,
    reason_for_change STRING,
    scope_2_figure_used STRING,
    theme STRING,
    sub_theme STRING,
    db_name STRING,
    framework STRING,
    report_year STRING
)
STORED AS PARQUET
LOCATION 's3://db-genie/demo/data/esg/env/emission_intensity/'
tblproperties ("parquet.compression"="SNAPPY");