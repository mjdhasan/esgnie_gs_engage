CREATE EXTERNAL TABLE esg_data_demo.green_revenue_v1 (
    company_name STRING,
    report_name STRING,
    question_number STRING,
    question_text STRING,
    section_label_c45a STRING,
    pct_of_total_portfolio_value STRING,
    pct_revenue_from_low_carbon_products_in_the_reporting_year STRING,
    provide_details_of_your_products_and_or_services_that_you_classify_as_low_carbon_products_or_that_enable_a_third_party_to_avoid_ghg_emissions STRING,
    are_these_low_carbon_products_or_do_they_enable_avoided_emissions STRING,
    asset_classes_or_product_types STRING,
    comment STRING,
    description_of_product_or_group_of_products STRING,
    level_of_aggregation STRING,
    taxonomy_project_or_methodology_used_to_classify_products_as_low_carbon_or_to_calculate_avoided_emissions STRING,
    section_label_c610 STRING,
    pct_change_from_previous_year STRING,
    direction_of_change STRING,
    intensity_figure STRING,
    metric_denominator STRING,
    metric_denominator_unit_total STRING,
    metric_numerator_gross_global_combined_scope_1_and_2_emissions STRING,
    metric_numerator_gross_global_combined_scope_1_and_2_emissions_metric_tons_co2e STRING,
    reason_for_change STRING,
    scope_2_figure_used STRING,
    theme STRING,
    sub_theme STRING,
    db_name STRING,
    framework STRING,
    report_year STRING
)
STORED AS PARQUET
LOCATION 's3://db-genie/demo/data/esg/env/green_revenue/'
tblproperties ("parquet.compression"="SNAPPY");