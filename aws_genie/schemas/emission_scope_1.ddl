CREATE EXTERNAL TABLE esg_data_demo.emission_scope_1_v3 (
    report_name STRING,
    response_year STRING,
    company_name STRING,
    category STRING,
    date STRING,
    end_year_of_reporting_period STRING,
    start_date STRING,
    end_date STRING,
    comment STRING,
    variable STRING,
    value STRING,
    value_num STRING,
    unit STRING,
    scope_num STRING,
    value_date STRING,
    theme STRING,
    sub_theme STRING,
    db_name STRING,
    framework STRING,
    report_year STRING
)
STORED AS PARQUET
LOCATION 's3://db-genie/demo/data/esg/env/emission_scope_1/'
tblproperties ("parquet.compression"="SNAPPY");