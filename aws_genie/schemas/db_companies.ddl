CREATE EXTERNAL TABLE coverage.esg_env (
    company_name STRING,
    db_name STRING,
    name_sics STRING,
    company_isin STRING,
    primary_sics_sector STRING,
    primary_sics_industry STRING,
    country STRING,
    region STRING,
    industry STRING,
    market_cap STRING,
    theme STRING,
    sub_theme STRING,
    framework STRING
)
STORED AS PARQUET
LOCATION 's3://db-genie/demo/coverage/esg/env/'
tblproperties ("parquet.compression"="SNAPPY");