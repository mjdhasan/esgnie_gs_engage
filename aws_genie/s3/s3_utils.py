import pandas as pd
import boto3
import os
import pickle


def read_data(dataset, bucket):
    prefix = ''
    suffix = ''
    if dataset == 'company_info':
        prefix = '_database/company_info'
    elif dataset == 'countries':
        prefix = '_database/countries'
    elif dataset == 'environment':
        prefix = '_database/env_and_energy'
    elif dataset == 'entities':
        prefix = '_database/entities'
    elif dataset == 'doc_type':
        prefix = '_database/doc-type'
    elif dataset == 'doc_img':
        prefix = '_database/doc-img'
    else:
        output = {'message': 'dataset not recognised.'}
    ## read s3 files
    l_files = [key for key in get_matching_s3_keys(bucket, prefix, suffix=suffix)]
    ## read s3 content
    l_content = read_s3_file_contents(bucket, l_files, sep='|')
    df_content = pd.concat(l_content)
    output = {'message': 'succeeded',
              'data': df_content.to_dict('split')}
    return output


def get_matching_s3_objects(bucket, prefix="", suffix=""):
    """
    Generate objects in an S3 bucket.

    :param bucket: Name of the S3 bucket.
    :param prefix: Only fetch objects whose key starts with
        this prefix (optional).
    :param suffix: Only fetch objects whose keys end with
        this suffix (optional).
    """
    s3 = boto3.client('s3')
    paginator = s3.get_paginator("list_objects_v2")

    kwargs = {'Bucket': bucket}

    # We can pass the prefix directly to the S3 API.  If the user has passed
    # a tuple or list of prefixes, we go through them one by one.
    if isinstance(prefix, str):
        prefixes = (prefix,)
    else:
        prefixes = prefix

    for key_prefix in prefixes:
        kwargs["Prefix"] = key_prefix

        for page in paginator.paginate(**kwargs):
            try:
                contents = page["Contents"]
            except KeyError:
                break

            for obj in contents:
                key = obj["Key"]
                if key.endswith(suffix):
                    yield obj


def get_matching_s3_keys(bucket, prefix="", suffix=""):
    """
    Generate the keys in an S3 bucket.

    :param bucket: Name of the S3 bucket.
    :param prefix: Only fetch keys that start with this prefix (optional).
    :param suffix: Only fetch keys that end with this suffix (optional).
    """
    for obj in get_matching_s3_objects(bucket, prefix, suffix):
        yield obj["Key"]


def read_s3_file_contents(bucket, l_files, sep='|'):
    ## define s3 client
    s3_client = boto3.client('s3')
    l_content = []
    for file in l_files:
        try:
            ## get object and file (key) from bucket
            obj = s3_client.get_object(Bucket=bucket, Key=file)
            ## get file ext
            file_ext = os.path.splitext(file)[1]
            ## get file content
            file_content = None
            if file_ext == '.pickle':
                file_content = pickle.loads(obj['Body'].read())
            elif file_ext == '.csv':
                file_content = pd.read_csv(obj['Body'], sep=sep)
            ## add to list
            if file_content is not None:
                l_content = l_content + [file_content]
        except Exception as e:
            print(f'unable to read {file}')
            print(e)
    return l_content
