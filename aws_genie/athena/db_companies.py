"""
Parquet table test
"""

import boto3
import os
import time
import pyarrow as pa
import pyarrow.parquet as pq
# ## read parquet file
# df = pa.parquet.read_table('/tmp/file.parquet')
# df.column_names
# dir(df['value'])
# df['value'].type
## lower-case all column names, replace ' ' and '-' with '_'
## set up athena
ath = boto3.client('athena')
resp = ath.start_query_execution(
    QueryString='create database coverage',
    ResultConfiguration={'OutputLocation': 's3://athena-genie/coverage/queries/'}
)
execution = ath.get_query_execution(QueryExecutionId=resp['QueryExecutionId'])
# execution['QueryExecution']['Status']['State']
## read create table query (see ref 2)
schema = 'db_companies.ddl'
with open(f'./schemas/{schema}') as ddl:
    query = ddl.read()
    ddl.close()
## create table
resp = ath.start_query_execution(
    QueryString=query,
    ResultConfiguration={'OutputLocation': 's3://athena-genie/coverage/queries/'}
)
execution = ath.get_query_execution(QueryExecutionId=resp['QueryExecutionId'])
status = execution['QueryExecution']['Status']['State']
print(status)
# ## list table metadata
# resp = ath.list_table_metadata(CatalogName='AWSDataCatalog',
#                                DatabaseName='parquet_test')
# # len(resp['TableMetadataList'])
# # [table['Name'] for table in resp['TableMetadataList']]
# ## set table
# table = resp['TableMetadataList'][0]['Name']
table = 'esg_env'
## query a table
resp = ath.start_query_execution(
    QueryString=f'SELECT * FROM {table} limit 100',
    QueryExecutionContext={
        'Database': 'coverage'
    },
    ResultConfiguration={'OutputLocation': 's3://athena-genie/coverage/queries/'}
)
execution = ath.get_query_execution(QueryExecutionId=resp['QueryExecutionId'])
execution['QueryExecution']['Status']['State']
results = ath.get_query_results(QueryExecutionId=resp['QueryExecutionId'])
results['ResultSet']['Rows']
