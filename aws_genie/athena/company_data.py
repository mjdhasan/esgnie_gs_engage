"""
Parquet table test
"""

import boto3
import os
import time

import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq
# ## read parquet file
# df = pa.parquet.read_table('/tmp/file.parquet')
# df.column_names
# dir(df['value'])
# df['value'].type
## lower-case all column names, replace ' ' and '-' with '_'
## set up athena
ath = boto3.client('athena')
resp = ath.start_query_execution(
    QueryString='create database esg_data_demo',
    ResultConfiguration={'OutputLocation': 's3://athena-genie/esg_data_demo/queries/'}
)
execution = ath.get_query_execution(QueryExecutionId=resp['QueryExecutionId'])
# execution['QueryExecution']['Status']['State']
## read create table query (see ref 2)
schema = 'green_revenue.ddl'
with open(f'./schemas/{schema}') as ddl:
    query = ddl.read()
    ddl.close()
## create table
resp = ath.start_query_execution(
    QueryString=query,
    ResultConfiguration={'OutputLocation': 's3://athena-genie/esg_data_demo/queries/'}
)
execution = ath.get_query_execution(QueryExecutionId=resp['QueryExecutionId'])
status = execution['QueryExecution']['Status']['State']
print(status)
# ## list table metadata
# resp = ath.list_table_metadata(CatalogName='AWSDataCatalog',
#                                DatabaseName='parquet_test')
# len(resp['TableMetadataList'])
# [table['Name'] for table in resp['TableMetadataList']]
# ## set table
# table = resp['TableMetadataList'][0]['Name']
table = 'green_revenue_v1'
## query a table
resp = ath.start_query_execution(
    QueryString=f'SELECT * FROM {table} limit 100',
    QueryExecutionContext={
        'Database': 'esg_data_demo'
    },
    ResultConfiguration={'OutputLocation': 's3://athena-genie/esg_data/queries/'}
)
execution = ath.get_query_execution(QueryExecutionId=resp['QueryExecutionId'])
execution['QueryExecution']['Status']['State']
results = ath.get_query_results(QueryExecutionId=resp['QueryExecutionId'])
results['ResultSet']['Rows']
"""
Delete old tables
"""
## list table metadata
resp = ath.list_table_metadata(CatalogName='AWSDataCatalog',
                               DatabaseName='esg_data_demo')
len(resp['TableMetadataList'])
df_table = pd.DataFrame()
df_table['name'] = [table['Name'] for table in resp['TableMetadataList']]
df_table['name_ver'] = [int(name.split('_v')[-1]) for name in df_table['name']]
df_table['name_base'] = [name.split('_v')[0] for name in df_table['name']]
df_table['name_ver_latest'] = df_table.groupby(['name_base'])['name_ver'].transform(max)
l_name = df_table[df_table['name_ver'] < df_table['name_ver_latest']]['name'].unique().tolist()
for table_rm in l_name:
    resp = ath.start_query_execution(
        QueryString=f'DROP TABLE {table_rm}',
        QueryExecutionContext={
            'Database': 'esg_data_demo'
        },
        ResultConfiguration={'OutputLocation': 's3://athena-genie/esg_data/queries/'}
    )
    time.sleep(5)
## list table metadata
resp = ath.list_table_metadata(CatalogName='AWSDataCatalog',
                               DatabaseName='esg_data_demo')
len(resp['TableMetadataList'])
[table['Name'] for table in resp['TableMetadataList']]