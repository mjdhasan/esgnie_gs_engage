from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


def launch_browser(name='Chrome', headless=False, print_page=False, directory=None, proxy=None,
                   remote_debugging_port=-1, update_path=True):
    driver = None
    # open browser
    if name == 'Chrome':
        # driver = webdriver.Chrome(get_webdriver_path(driver_name='chromedriver'))
        if update_path:
            driver_path = ChromeDriverManager().install()
        else:
            driver_path = '/usr/bin/chromedriver'
        s = Service(driver_path)
        options = webdriver.ChromeOptions()
        # WINDOW_SIZE = "1920,1080"
        # options.add_argument("--window-size=%s" % WINDOW_SIZE)
        # options.add_argument("--no-sandbox")
        if headless:
            # options = webdriver.ChromeOptions()
            options.headless = True
        if proxy is not None:
            options.add_argument(f'--proxy-server={proxy}')
        if remote_debugging_port > 0:
            options.add_argument(f'--remote-debugging-port={remote_debugging_port}')
        if print_page:
            appState = {
                "recentDestinations": [
                    {
                        "id": "Save as PDF",
                        "origin": "local",
                        "account": ""
                    }
                ],
                "selectedDestinationId": "Save as PDF",
                "version": 2
            }
            profile = {
                "download.default_directory": directory,
                "download.prompt_for_download": False,
                "download.directory_upgrade": True,
                "plugins.always_open_pdf_externally": True,
                'printing.print_preview_sticky_settings.appState': json.dumps(appState),
                'savefile.default_directory': directory
            }
            options = webdriver.ChromeOptions()
            options.headless = True
            options.add_experimental_option('prefs', profile)
            options.add_argument('--kiosk-printing')
        ## set options in the driver
        driver = webdriver.Chrome(executable_path=driver_path, options=options)
        # driver = webdriver.Chrome(service=s, options=options)
    elif name == 'Firefox':
        # driver_path = get_webdriver_path(driver_name='geckodriver')
        driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver')
    elif name == 'Safari':
        driver = webdriver.Safari(executable_path='/usr/bin/safaridriver')

    return driver


def safely_launch_browser(name, headless, verbose=True, port=8088):
    web_driver = None
    try:
        web_driver = launch_browser(name, headless=headless, port=port)
    except Exception as e:
        if verbose:
            print(e)
    return web_driver