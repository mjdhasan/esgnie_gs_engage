import os


def list_full_path(d, recursive=False):
    l_full_path = []
    for path in os.listdir(d):
        if recursive and os.path.isdir(os.path.join(d, path)):
            # print(path)
            l_full_path = l_full_path + list_full_path(d=os.path.join(d, path), recursive=recursive)
        else:
            l_full_path = l_full_path + [os.path.join(d, path)]
    return l_full_path