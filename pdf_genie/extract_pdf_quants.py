"""
Extract quant values from pdf files
"""

import argparse
from init import DIR_SCRAPERS
import pandas as pd
import numpy as np
import multiprocessing as mp
import os
from pdf_genie.utils.util import list_full_path, list_pdf_dir, list_pdf_files
from pdf_genie.parsers.pdf_words import write_words, list_words_files, write_quants
from pdf_genie.parsers.pdf_words import safely_write_words, safely_write_quants, safely_structure_words
from pdf_genie.parsers.pdf_words import structure_words, safely_get_value_context


def extract_pdf_quants(root_dir, file_types, max_files=-1, parallel=0, verbose=1, n_cpu_frac=1, reverse=0):
    """

    :param root_dir:
    :param file_types:
    :param max_files:
    :param parallel:
    :param verbose:
    :param n_cpu_frac:
    :param reverse:
    :return:
    """
    """
    Write df_words files
    """
    if 'words' in file_types:
        l_dir = [root_dir]
        ## get list of companies for which pdf reports have been dowloaded
        l_dir_pdf = list_pdf_dir(l_dir)
        ## list all pdf files
        l_files = list_pdf_files(l_dir_pdf)
        ## reverse files
        if reverse:
            l_files.reverse()
        ## truncate by max_files
        if max_files > 0:
            l_files = l_files[:max_files]
        ## parallel loop
        if parallel:
            n_cpu = int(np.ceil(n_cpu_frac * mp.cpu_count()))
            pool = mp.Pool(n_cpu)
            results = pool.map(safely_write_words, l_files)
            pool.close()
        else:
            ## loop over each file, and save df_words
            for file_num, file in enumerate(l_files):
                if verbose > 0:
                    print(f"{file_num}/{len(l_files)}: {file}")
                try:
                    write_words(file)
                except Exception as e:
                    if verbose > 0:
                        print('unable to read words file')
                        print(e)
    """
    Write quants files
    """
    if 'quants' in file_types:
        ## get list of companies mathcing input tickers
        l_dir = [root_dir]
        ## get list of companies for which pdf reports have been dowloaded
        l_dir_pdf = list_pdf_dir(l_dir)
        ## get all df_words files
        l_files = list_words_files(l_dir_pdf)
        ## reverse files
        if reverse:
            l_files.reverse()
        ## truncate by max_files
        if max_files > 0:
            l_files = l_files[:max_files]
        if parallel:
            n_cpu = int(np.ceil(n_cpu_frac * mp.cpu_count()))
            # n_files_chunk = int(np.ceil(len(l_files) / n_cpu))
            # ll_files = [l_files[i:(i + n_files_chunk)]
            #             for i in range(0, len(l_files), n_files_chunk)]
            pool = mp.Pool(n_cpu)
            # l_pred = pool.map(search_docs, ll_docs)
            results = pool.map(safely_write_quants, l_files)
            pool.close()
        else:
            ## loop over all files and compute quant ratios
            for file_num, file in enumerate(reversed(l_files)):
                if verbose > 0:
                    print(f"{file_num}/{len(l_files)}: {file}")
                try:
                    df = write_quants(file)
                except Exception as e:
                    if verbose > 0:
                        print(file)
                        print(e)
    """
    Write df_cells files
    """
    if 'cells' in file_types:
        ## get all df_quants files
        l_files = [file for file in list_full_path(root_dir, recursive=True) if 'df_quants' in file]
        ## reverse files
        if reverse:
            l_files.reverse()
        ## truncate by max_files
        if max_files > 0:
            l_files = l_files[:max_files]
        if parallel:
            n_cpu = int(np.ceil(n_cpu_frac * mp.cpu_count()))
            pool = mp.Pool(n_cpu)
            results = pool.map(safely_structure_words, l_files)
            pool.close()
        else:
            for file_num, file in enumerate(l_files):
                parent_dir = os.path.split(file)[0]
                filename = file.split('/')[-1]
                cells_file = f"{parent_dir}/{filename.replace('df_quants', 'df_cells')}"
                if os.path.isfile(cells_file):
                    if verbose > 0:
                        print('output file already exists: skipping')
                    continue
                try:
                    df_quants = pd.read_csv(file)
                    ## structure cells
                    df_cells, df_cells_merged, df_cells_flt = structure_words(df=df_quants)
                    ## write df_cells
                    df_cells.to_csv(cells_file, index=False)
                except Exception as e:
                    if verbose > 0:
                        print('unable to structure_words()')
                        print(e)
    """
    Value context files
    """
    if 'context' in file_types:
        ## get all words-segments files
        l_files = [file for file in list_full_path(root_dir, recursive=True) if '_words-segments.csv' in file]
        ## reverse files
        if reverse:
            l_files.reverse()
        ## truncate by max_files
        if max_files > 0:
            l_files = l_files[:max_files]
        if parallel:
            n_cpu = int(np.ceil(n_cpu_frac * mp.cpu_count()))
            pool = mp.Pool(n_cpu)
            results = pool.map(safely_get_value_context, l_files)
            pool.close()
        else:
            for file_num, file in enumerate(l_files):
                parent_dir = os.path.split(file)[0]
                filename = file.split('/')[-1]
                df_val, df_header_context, df_left_context = safely_get_value_context(file, verbose=verbose)



if __name__ == "__main__":
    ## Create the parser
    arg_parser = argparse.ArgumentParser(prog='extract_quants',
                                         usage='%(prog)s [options]',
                                         description='extract quants from pdf files')
    ## add arg: root dir
    arg_parser.add_argument('-r', '--root_dir',
                            metavar='root_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/www.sasb.org',
                            help='root dir')
    ## add arg: max files
    arg_parser.add_argument('-m', '--max_files',
                            metavar='max_files',
                            nargs='?',
                            type=int,
                            default=-1,
                            help='run in verbose mode')
    ## add arg: verbose
    arg_parser.add_argument('-v', '--verbose',
                            metavar='verbose',
                            nargs='?',
                            type=int,
                            default=1,
                            help='run in verbose mode')
    ## add arg: parallel
    arg_parser.add_argument('-p', '--parallel',
                            metavar='parallel',
                            nargs='?',
                            type=int,
                            default=0,
                            help='run in parallel mode')
    ## add arg: n_cpu frac
    arg_parser.add_argument('-n', '--n_cpu_frac',
                            metavar='n_cpu_frac',
                            nargs='?',
                            type=float,
                            default=0.5,
                            help='number of CPUs to use for parallel processing')
    ## add arg: reverse mode
    arg_parser.add_argument('-rev', '--reverse',
                            metavar='reverse',
                            nargs='?',
                            type=int,
                            default=0,
                            help='loop files in reverse order')
    ## add arg: words mode
    arg_parser.add_argument('-f', '--file_types',
                            metavar='file_types',
                            nargs='+',
                            type=str,
                            default=['words'],
                            help='type of files to write')
    ## parse args
    args, unknown_args = arg_parser.parse_known_args()
    print(args)
    ## set root dir
    root_dir = args.root_dir
    ## max files to process
    max_files = args.max_files
    ## parallel flag
    parallel = bool(args.parallel)
    ## number of cpu
    n_cpu_frac = args.n_cpu_frac
    ## set verbose flag
    verbose = bool(args.verbose)
    ## set reverse flag
    reverse = bool(args.reverse)
    ## set file types
    file_types = args.file_types
    ## run extract pdf quants
    extract_pdf_quants(root_dir, file_types, max_files, parallel, verbose, n_cpu_frac, reverse)





