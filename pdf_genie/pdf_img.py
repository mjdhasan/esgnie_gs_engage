"""
Take images of pdf files
"""
import argparse
from init import DIR_SCRAPERS
import pdf2image
import shutil
import os
# import pdfplumber
from utils.util import list_full_path, list_pdf_dir, list_pdf_files


if __name__ == "__main__":
    ## Create the parser
    arg_parser = argparse.ArgumentParser(prog='extract_quants',
                                         usage='%(prog)s [options]',
                                         description='take pdf images')
    ## add arg: root dir
    arg_parser.add_argument('-r', '--root_dir',
                            metavar='root_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/www.sasb.org',
                            help='root dir')
    ## parse args
    args, unknown_args = arg_parser.parse_known_args()
    print(args)
    ## set root dir
    root_dir = args.root_dir
    ## list of pdf dir
    l_pdf_dir = list_pdf_dir([root_dir])
    l_files = list_pdf_files(l_pdf_dir)
    l_files = [file for file in l_files if '/pdf/' in file]
    # file = [file for file in l_files if 'fp177' in file][0]
    # l_files.reverse()
    for file_num, file in enumerate(l_files):
        print(f"{file_num}/{len(l_files)}: {file}")
        img_dir = '/'.join(file.split('/')[:-2]) + '/img'
        # if os.path.isdir(img_dir):
        #     l_files_pagenum = [file for file in list_full_path(img_dir, recursive=True) if 'pagenum-' in file]
        #     for file_rm in l_files_pagenum:
        #         os.remove(file_rm)
        # if os.path.isdir(img_dir):
        #     shutil.rmtree(img_dir)
        os.makedirs(img_dir, exist_ok=True)
        filename = file.split('/')[-1].replace('.pdf', '')
        # try:
        #     ## read pdf file
        #     pdf_doc = pdfplumber.open(file)
        #     for pdf_page in pdf_doc.pages:
        #         ## output file
        #         output_file = f"{img_dir}/{filename}_pagenum-{pdf_page.page_number}.png"
        #         if os.path.isfile(output_file):
        #             print(f'{output_file} already exists: skipping')
        #             continue
        #         ## save image to file
        #         pdf_page.to_image(resolution=300).save(output_file)
        #     ## close pdf_page
        #     pdf_doc.close()
        # except Exception as e:
        #     print('could not process this pdf file')
        #     print(e)
        l_img_files = list_full_path(img_dir, recursive=True)
        l_img_files = [file for file in l_img_files if filename in file]
        if len(l_img_files) > 30:
            print('output dir already exists: skipping')
            continue
        try:
            out = pdf2image.convert_from_path(file,
                                              output_file=filename,
                                              output_folder=img_dir,
                                              fmt='png')
        except Exception as e:
            print(e)

# 'nohup /ebs-0/venvs/pdf-genie/bin/python /ebs-0/code/pdf-genie/pdf_img.py -r /ebs-0/data/sasb-genie > /ebs-0/logs/pdf_img_sasb.txt &'