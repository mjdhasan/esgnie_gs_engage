import platform


if 'Linux' in platform.platform():
    HOME_DIR = '/home/ubuntu'
    platform_user = ''
    ## dir for web scraping output
    DIR_SCRAPERS = f'/ebs-0/data'
elif 'i386-64bit' in platform.platform():
    ROOT_DIR = f'~/data'
    platform_user = 'majid'
    ## dir for web scraping output
    DIR_SCRAPERS = f'/Users/{platform_user}/Dropbox/data/scrapers'