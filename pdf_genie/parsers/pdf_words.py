####-------------------------------
#### Author: Majid Hasan
#### Title: Parse PDF words
#### Date: 2021-08-15
####-------------------------------

import os
import pickle
from uuid import uuid4
from pdf_genie.utils.util import list_full_path, \
    get_quants_surface, get_quants_ratio, \
    merge_cells, match_lists, \
    identify_cols_within_row, get_interval_overlap, regex_year, \
    classify_value_type, regex_multiplier, get_words_df_above
import pandas as pd
import numpy as np
import pdfplumber
from fuzzywuzzy import fuzz
from pdf_genie.utils.util import take_unique_list


def find_closest_contiguous_left_text(df_quants, val_mask):
    val_row = df_quants[val_mask]['row_num'].tolist()[0]
    val_xbar = df_quants[val_mask]['xbar'].tolist()[0]
    ## mask for row
    quants_row_mask = (df_quants['row_num'] == val_row)
    ## proprietary code (to find relevant text to the left of value) removed
    return df_quants[quants_row_mask &
                     (df_quants['x1'] < df_quants.loc[val_mask, 'x0'].tolist()[0]) &
                     (df_quants['val_type'] == 'chr') &
                     (df_quants['text'].isin(l_left_text))
                     ]


def find_header(df_quants, word_row, header_types: list = None, year_flag=1):
    if header_types is None:
        header_types = ['digit']
    ## find all cells above this
    df_above = get_words_df_above(df_quants, word_row=word_row).copy()
    df_above['text_len'] = [len(text) for text in df_above['text']]
    if df_above.shape[0] <= 0:
        return df_above
    ## proprietary code (to find header) removed
    return df_above[possible_header_mask]


def get_words_df(l_words):
    df_words = pd.DataFrame()
    for word in l_words:
        df_word = pd.DataFrame(list(word.items())).transpose()
        df_word.columns = df_word.loc[0, :].values.tolist()
        df_word = df_word.loc[1:, :]
        df_words = df_words.append(df_word)
    df_words = df_words.reset_index(drop=True)
    df_words['xbar'] = (df_words['x0'] + df_words['x1']) / 2
    df_words['ybar'] = (df_words['top'] + df_words['bottom']) / 2
    return df_words


def assign_rows_by_word_coords(df_words, max_itr=1000):
    df_words = df_words.sort_values(['ybar', 'xbar'])
    df_words['row_num'] = np.nan
    l_rows = list(df_words.iterrows())
    table_row_num = 0
    while (len(l_rows) > 0) and table_row_num < max_itr:
        ## get the first row out of all unidentified rows
        row = l_rows[0][1]
        ## overlapping y mask
        overlapping_y_mask = (df_words['row_num'].isna()) & \
                             (df_words['top'] <= row.get('bottom')) & \
                             (df_words['bottom'] >= row.get('top'))
        ## set row number for all words with overlapping y
        df_words.loc[overlapping_y_mask, 'row_num'] = table_row_num
        ## find df_words with missing row_num
        df_words_un = df_words.loc[df_words['row_num'].isna(), :].copy()
        ## get rows of unidentified data
        l_rows = list(df_words_un.iterrows())
        ## update table_row_num
        table_row_num = table_row_num + 1
    return df_words


def list_words_files(l_dir_pdf):
    l_files = []
    for dir in l_dir_pdf:
        dir_files = list_full_path(dir, recursive=True)
        dir_files = [file for file in dir_files if 'df_words' in file]
        l_files = l_files + dir_files
    return l_files


def get_page_quants_ratios(l_text):
    ## get text for all quants
    l_quant_surface = [get_quants_surface(text) for text in l_text if get_quants_surface(text)]
    ## proportion of quants for the whole page
    quants_ratio_page = len(l_quant_surface) / len(l_text)
    return quants_ratio_page


def write_words(file):
    dir = '/'.join(file.split('/')[:-2])
    pdf_dir = f"{dir}/pdf"
    csv_dir = f"{dir}/csv"
    os.makedirs(csv_dir, exist_ok=True)
    ## get filename
    filename = file.split('/')[-1].replace('.pdf', '')
    ## proprietary function (to write words coordinates) removed
    ## pass



def safely_write_words(file, verbose=False):
    try:
        return write_words(file)
    except Exception as e:
        if verbose:
            print(f'unable to write words for {file}')
            print(e)



def write_quants(file):
    csv_dir = '/'.join(file.split('/')[:-1])
    filename = file.split('/')[-1].replace('.csv', '').replace('df_words_', '')
    ## if the output file already exists, continue to the next iteration
    if os.path.isfile(f"{csv_dir}/df_quants_{filename}.csv"):
        print('output file already exists; skipping')
        return pd.read_csv(f"{csv_dir}/df_quants_{filename}.csv")
    ## read file
    with open(file, 'r') as f:
        df = pd.read_csv(f)
        f.close()
    df['text'] = df['text'].map(str)
    df['quants_ratio_page'] = get_page_quants_ratios(df['text'].map(str).tolist())
    ## proportion of quants by row
    if 'pagenum' not in df.columns:
        col = match_lists(df.columns.tolist(), ['pagenum'])
        if fuzz.partial_ratio(col, 'pagenum') > 80:
            df.rename(columns={col: 'pagenum'}, inplace=True)
    df_quants_ratio_row = df.groupby(['file', 'pagenum', 'row_num']
                                     ).apply(lambda x: get_quants_ratio(x['text'].values.tolist())
                                             ).reset_index()
    level_cols = [col for col in df_quants_ratio_row.columns if 'level_' in col]
    df_quants_ratio_row.drop(columns=level_cols, inplace=True)
    ## merge number of quants by row back into the df
    df = pd.merge(left=df, right=df_quants_ratio_row,
                  how='left', on=['file', 'pagenum', 'row_num'])
    ## add filename to df_keywords
    df['file'] = file
    file_comp = file.split('/')
    file_comp = [comp for comp in file_comp if comp != '']
    file_comp = str(file_comp[-1]).replace('df_words_', '').replace('.csv', '').split('_pagenum-')
    df['filename'] = file_comp[0]
    df['pagenum'] = file_comp[-1]
    df['id'] = df['filename'].map(str) + df['pagenum'].map(str) + df['text'].map(str)
    df['id'] = [f"{id}-{uuid4()}" for id in df['id']]
    df.to_csv(f"{csv_dir}/df_quants_{filename}.csv", index=False)
    return df


def safely_write_quants(file, verbose=True):
    try:
        return write_quants(file)
    except Exception as e:
        if verbose:
            print(f'unable to write quants for {file}')
            print(e)


def safely_structure_words(file, verbose=True):
    try:
        cells_file = file.replace('df_quants_', 'df_cells_')
        df_quants = pd.read_csv(file)
        ## structure cells
        df_cells, df_cells_merged, df_cells_flt = structure_words(df=df_quants)
        ## write df_cells
        df_cells.to_csv(cells_file, index=False)
    except Exception as e:
        if verbose > 0:
            print('unable to structure_words()')
            print(e)


def calc_chr_area(df_words):
    df_words['w'] = df_words['x1'] - df_words['x0']
    df_words['h'] = df_words['bottom'] - df_words['top']
    df_words['n_chr'] = [len(word) for word in df_words['text']]
    df_words['w_chr'] = df_words['w'] / df_words['n_chr']
    df_words['chr_area'] = df_words['w_chr'] * df_words['h']
    return df_words


def get_section_summary(df_words, n_page_min=2, n_pages_q=0.90, dist_to_width_ratio=8, chr_area_q=0.98, n_rows_section=10):
    ## count number of pages a word appears on
    df_words['n_pages_by_word'] = df_words.groupby(['parent_folder', 'filename', 'text']
                                                   )['pagenum'].transform('nunique')
    df_words['n_words_by_page'] = df_words.groupby(['parent_folder', 'filename', 'pagenum']
                                                   )['text'].transform('count')
    # df_words['pagenum'].max()
    # df_words['n_pages_by_word'].quantile(np.arange(0, 1.05, 0.05))
    l_freq_words = df_words[(df_words['n_pages_by_word'] >= df_words['n_pages_by_word'].quantile(n_pages_q)) &
                            (df_words['n_pages_by_word'] >= n_page_min)
                            ]['text'].unique().tolist()
    doc_header = ' '.join(l_freq_words)
    df_words = df_words[~df_words['text'].isin(l_freq_words)]
    df_words = df_words.reset_index()
    df_words = df_words.sort_values(['top', 'x0', 'bottom', 'x1'])
    ## generate cell numbers within rows, grouped by file, pagenum
    df_cells = df_words.groupby(['file', 'pagenum']
                                ).apply(lambda x: arrange_rows_in_cells(x, dist_to_width_ratio=dist_to_width_ratio)
                                        )
    df_cells = df_cells.reset_index(drop=True)
    if df_cells.shape[0] <= 0:
        return pd.DataFrame()
    ## calculate character area for each cell text
    df_cells = calc_chr_area(df_cells)
    ## big words
    chr_area_mask = df_cells['chr_area'] > df_cells['chr_area'].quantile(chr_area_q)
    df_sections = pd.DataFrame()
    for idx in list(df_cells[chr_area_mask].index):
        ## find words in rows below the big word
        text = df_cells.loc[idx, 'text']
        file = df_cells.loc[idx, 'file']
        pagenum = df_cells.loc[idx, 'pagenum']
        row_num = df_cells.loc[idx, 'row_num']
        cell_num = df_cells.loc[idx, 'cell_num']
        cell_chr_area = df_cells.loc[idx, 'chr_area']
        cell_text = df_cells.loc[df_cells['cell_num'] == cell_num, 'text']
        cell_width = df_cells.loc[idx, 'x1'] - df_cells.loc[idx, 'x0']
        cell_height = df_cells.loc[idx, 'bottom'] - df_cells.loc[idx, 'top']
        cell_x0 = df_cells.loc[idx, 'x0']
        cell_x1 = df_cells.loc[idx, 'x1']
        cell_top = df_cells.loc[idx, 'top']
        cell_bot = df_cells.loc[idx, 'bottom']
        col_within_row = df_cells.loc[idx, 'col_within_row']
        l_text = df_cells[(df_cells['pagenum'] == pagenum) &
                          (df_cells['row_num'] > row_num) &
                          (df_cells['row_num'] < row_num + n_rows_section) &
                          (df_cells['col_within_row'] == col_within_row)
                          ]['text'].tolist()
        section_text = ' '.join(l_text)
        ## create df_
        df_ = pd.DataFrame([
            [file, pagenum, row_num, cell_num, col_within_row,
             text, cell_text, section_text,
             cell_chr_area,
             cell_x0, cell_x1, cell_top, cell_bot, cell_width, cell_height
             ]
        ],
            columns=['file', 'pagenum', 'row_num', 'cell_num', 'col_within_row',
                     'text', 'cell_text', 'section_text',
                     'cell_chr_area',
                     'cell_x0', 'cell_x1', 'cell_top', 'cell_bot', 'cell_width', 'cell_height'])
        df_sections = df_sections.append(df_)
    ## add document header
    if df_sections.shape[0] > 0:
        df_sections['doc_header'] = doc_header
    return df_sections


def arrange_rows_in_cells(df, dist_to_width_ratio=2):
    ## loop over each row and identify cells
    df_cells = pd.DataFrame()
    for row_num in df['row_num'].unique().tolist():
        df_row = df[df['row_num'] == row_num].copy()
        ## identify cells within row
        df_row = identify_cols_within_row(df_row, dist_to_width_ratio)
        df_row = df_row[df_row['text'].notnull()]
        ## join text within the same cell
        df_row = df_row.groupby(['file', 'filename', 'pagenum', 'row_num', 'col_within_row']
                                ).apply(lambda x: merge_cells(df=x)
                                        ).reset_index()
        # df_row['col_within_row'].unique()
        # df_row[df_row['col_within_row'] == 0]['text'].tolist()
        df_cells = df_cells.append(df_row)
    ## reset index
    df_cells = df_cells.reset_index(drop=True)
    ## add cell number based on pagenum-row_num-col_within_row
    df_cells['cell_num'] = df_cells['pagenum'].map(int).map(str) + '-' + \
                           df_cells['row_num'].map(int).map(str) + '-' + \
                           df_cells['col_within_row'].map(int).map(str)
    ## add cell width
    df_cells['cell_width'] = df_cells['x1'] - df_cells['x0']
    # ## check df
    # df_cells[['row_num', 'col_within_row', 'text']].values.tolist()
    ## remove rows with na text
    df_cells = df_cells[df_cells['text'].notnull()]
    return df_cells


def get_cells_to_merge(df_cells, cell_num):
    l_cells = []
    l_cell_text = []
    curr_cell_action = df_cells[df_cells['cell_num'] == cell_num]['next_row_cell_action'].values.tolist()[0]
    next_cell_num = df_cells[df_cells['cell_num'] == cell_num]['next_row_cell'].values.tolist()[0]
    l_cells = l_cells + [cell_num]
    curr_cell_text = df_cells[df_cells['cell_num'] == cell_num]['text'].values.tolist()[0]
    # next_cell_text = df_cells[df_cells['cell_num'] == next_cell_num]['text'].values.tolist()[0]
    l_cell_text = l_cell_text + [curr_cell_text]
    # next_cell_action = df_cells[df_cells['cell_num'] == next_cell_num]['next_row_cell_action'].values.tolist()[0]
    if curr_cell_action > 0:
        l_cells_, l_cell_text_ = get_cells_to_merge(df_cells, next_cell_num)
        l_cells = l_cells + l_cells_
        l_cell_text = l_cell_text + l_cell_text_
    return l_cells, l_cell_text


def merge_cells_vertically(df_cells):
    ## set cell cols equal
    for col in ['x0', 'x1', 'top', 'bottom', 'text']:
        df_cells[f"cell_{col}"] = df_cells[col]
    l_cells_to_merge = df_cells[(df_cells['next_row_cell_action'] == 1)]['cell_num'].unique().tolist()
    while l_cells_to_merge:
        ## start from the first cell
        cell_num = l_cells_to_merge[0]
        ## get the list of cells that need to be merged with the selected cell
        l_cells, l_cell_text = get_cells_to_merge(df_cells, cell_num)
        ## change the next_row_cell_action from 1 to 0
        df_cells.loc[(df_cells['cell_num'].isin(l_cells)) &
                     (df_cells['next_row_cell_action'] == 1),
                     'next_row_cell_action'] = 0
        ## change the text field for merged cells
        cell_condition = df_cells['cell_num'].isin(l_cells)
        df_cells.loc[cell_condition, 'cell_text'] = ' '.join(l_cell_text)
        ## take the region across all l_cells and set it as the boundary for each cell
        df_cells.loc[cell_condition, 'cell_x0'] = df_cells[cell_condition]['x0'].min()
        df_cells.loc[cell_condition, 'cell_x1'] = df_cells[cell_condition]['x1'].max()
        df_cells.loc[cell_condition, 'cell_top'] = df_cells[cell_condition]['top'].min()
        df_cells.loc[cell_condition, 'cell_bottom'] = df_cells[cell_condition]['bottom'].max()
        ## update list of cells that remains to be merged
        l_cells_to_merge = df_cells[df_cells['next_row_cell_action'] == 1]['cell_num'].unique().tolist()
    ## re-compute xbar, ybar
    df_cells['cell_xbar'] = (df_cells['x0'] + df_cells['x1']) / 2
    df_cells['cell_ybar'] = (df_cells['top'] + df_cells['bottom']) / 2
    df_cells['cell_width'] = df_cells['x1'] - df_cells['x0']
    df_cells['cell_height'] = df_cells['bottom'] - df_cells['top']
    return df_cells


def structure_words(df, dist_to_width_ratio=2):
    ## proprietary function to combine words into cells
    pass


def filter_redundant_rows(df_cells):
    ## take the terminal text next_row_cell_action = -1
    l_text = df_cells[df_cells['next_row_cell_action'] == -1]['cell_text'].unique()
    ## a sanity check
    if len(df_cells[df_cells['next_row_cell_action'] == -1]['cell_text'].unique()) == len(df_cells['cell_text'].unique()):
        print(
            'len of unique text strings in terminal cells matches the len of unique text strings across all cells')
    else:
        print("Warning: len of unique text strings in terminal cells does not match "
              "the len of unique text strings across all cells")
    ## loop over all text strings and find every row for each text string
    l_rows = []  ## list to store rows with max cells
    for text in l_text:
        ## find all rows containing the text
        l_text_rows = df_cells[df_cells['cell_text'] == text]['row_num'].unique().tolist()
        ## find number of cells in each row
        l_num_cells = []
        # l_row_text_len = []
        for row_num in l_text_rows:
            num_cells = len(df_cells[df_cells['row_num'] == row_num]['cell_num'].unique().tolist())
            l_num_cells = l_num_cells + [num_cells]
        ## find the row_num with max cells
        l_num_cells = np.array(l_num_cells)
        max_cells = max(l_num_cells)
        row_with_max_cells = []
        if max_cells == 1:
            row_with_max_cells = [l_text_rows[-1]]
        else:
            idx_max_cells = np.where(l_num_cells == max_cells)[0].tolist()
            row_with_max_cells = [l_text_rows[idx] for idx in idx_max_cells]
        l_rows = l_rows + row_with_max_cells
    ## take unique list of rows
    l_rows = take_unique_list(l_rows)
    print(f"len of rows with max cells: {len(l_rows)}")
    print(f"len of total rows: {len(df_cells['row_num'].unique())}")
    ## filter df over selected rows
    return df_cells[df_cells['row_num'].isin(l_rows)]


def classify_page_regions(df):
    df = df.sort_values(['row_num', 'x0', 'x1'])
    ## get number of cells in each row
    df_n_cells = df.groupby(['row_num'])['cell_num'].nunique().reset_index()
    df_n_cells.rename(columns={'cell_num': 'n_cells'}, inplace=True)
    df = pd.merge(left=df, right=df_n_cells, on=['row_num'], how='left')
    ## find number of cells above and below each row
    df = df.sort_values(['row_num'])
    df['n_cells_above'] = df['n_cells'].shift(1)
    df['n_cells_below'] = df['n_cells'].shift(-1)
    ## sort df
    df = df.sort_values(['row_num', 'cell_x0', 'cell_x1'])
    ## if number of cells in the row, or the one above or below it, are >1
    df.loc[(df['n_cells'] > 1) | (df['n_cells_above'] > 1) | (df['n_cells_below'] > 1), 'multi_cell_flag'] = 0
    ## if there is only one cell in the row, set single_cell_flag to 1
    df.loc[df['n_cells'] == 1, 'single_cell_flag'] = 1
    ## compute vertical distance b/w successive rows
    df = df.sort_values(['ybar', 'cell_num'])
    df_row_dist = df.groupby(['row_num']
                             ).apply(lambda x: pd.DataFrame({'row_num': x['row_num'].unique(),
                                                             'row_top': [x['top'].median()],
                                                             'row_bottom': [x['bottom'].median()],
                                                             'row_ybar': [x['ybar'].median()]})
                                     ).reset_index(drop=True)
    df_row_dist['row_delta_y'] = df_row_dist['row_top'].shift(-1) - df_row_dist['row_bottom']
    median_row_delta_y = df_row_dist['row_delta_y'].median()
    median_row_width = (df_row_dist['row_bottom'] - df_row_dist['row_top']).median()
    ## remove overlapping cols, except index
    col_idx = ['row_num']
    for col in df_row_dist.columns:
        if (col in df.columns) and (col not in col_idx):
            df.drop(columns=col, inplace=True)
    ## merge y dist df to df
    df = pd.merge(left=df, right=df_row_dist, on=col_idx, how='left')
    ## row delta y as a fraction of median row dist
    df['row_delta_y_frac'] = df['row_delta_y'] / median_row_width
    ##--------------------------------
    ## identify text and table regions
    ##--------------------------------
    ## get the full width of page
    max_width = df['cell_x1'].max() - df['cell_x0'].min()
    max_height = df['cell_bottom'].max() - df['cell_top'].min()
    ## set the width of each cell relative to total page width
    df['cell_xbar'] = (df['cell_x0'] + df['cell_x1']) / 2
    df['cell_ybar'] = (df['cell_top'] + df['cell_bottom']) / 2
    df['cell_width'] = df['cell_x1'] - df['cell_x0']
    df['cell_height'] = df['cell_bottom'] - df['cell_top']
    df['cell_width_frac'] = df['cell_width'] / max_width
    df['cell_height_frac'] = df['cell_height'] / max_height
    ## set the x-coord of each cell relative to total width
    df['cell_x0_frac'] = (df['cell_x0'] - df['cell_x0'].min()) / max_width
    df['cell_x1_frac'] = (df['cell_x1'].max() - df['cell_x1']) / max_width
    df['cell_xbar_frac'] = (df['cell_xbar'] - df['cell_x0'].min()) / max_width
    ## set the y-coord of each cell relative to total width
    df['cell_top_frac'] = (df['cell_top'] - df['cell_top'].min()) / max_height
    df['cell_bottom_frac'] = (df['cell_bottom'].max() - df['cell_bottom']) / max_height
    df['cell_ybar_frac'] = (df['cell_ybar'] - df['cell_top'].min()) / max_height
    return df


def get_numeric_value_context(val_id):
    ## proprietary function (to get context of numeric value) removed
    pass


def get_value_context(file, verbose=False, overwrite=True, filename_replace=None):
    if filename_replace is None:
        filename_replace = ['df_quants_', 'df_words_', 'df_cells_']
    pagenum = file.split('pagenum-')[-1].replace('.csv', '')
    filename = os.path.split(file)[-1]
    for freplace in filename_replace:
        filename = filename.replace(freplace, '')
    filename = filename.split('_pagenum-')[0]
    ## company folder
    comp_folder = '/'.join(file.split('/')[:-2])
    ## read quants file
    df_quants = pd.read_csv(file)
    df_quants = df_quants.reset_index(drop=True)
    if df_quants.shape[0] <= 0:
        if verbose:
            print('no values in df_quants')
        return pd.DataFrame(), pd.DataFrame(), pd.DataFrame()
    ## add id
    df_quants['id'] = [str(row.get('filename')) + '-' +
                       str(row.get('pagenum')) + '-' +
                       str(row.get('row_num')) + '-' +
                       str(row.get('left')) + '-' +
                       str(row.get('right')) + '-' +
                       str(row.get('top')) + '-' +
                       str(row.get('bottom'))
                       for row_num, row in df_quants.iterrows()]
    ## get value types
    df_quants['val_type'] = [classify_value_type(val) for val in df_quants['text']]
    ## add year flag
    df_quants['year_flag'] = [1 if regex_year(val) else 0 for val in df_quants['text']]
    ## get all digit values
    # post_rev_mask = df_quants['row_num'] >= l_rows_revenue[0]
    relevant_digit_mask = (df_quants['val_type'] == 'digit')  ## & (post_rev_mask)
    l_digit_values = df_quants[relevant_digit_mask]['text'].tolist()
    l_digit_id = df_quants[relevant_digit_mask]['id'].tolist()
    if not l_digit_values:
        if verbose:
            print('no digit values in df_quants')
        return pd.DataFrame(), pd.DataFrame(), pd.DataFrame()
    ## sort values
    df_quants = df_quants.sort_values(['xy_group', 'top', 'x0', 'bottom', 'x1'])
    ## add width and height columns
    df_quants['h'] = df_quants['bottom'] - df_quants['top']
    df_quants['w'] = df_quants['x1'] - df_quants['x0']
    df_quants['n_chr'] = [len(text) for text in df_quants['text']]
    df_quants['chr_w'] = df_quants['w'] / df_quants['n_chr']
    ## set df to save values
    df_val_all = df_header_context_all = df_left_context_all = pd.DataFrame()
    for val_num, val_id in enumerate(l_digit_id):
        df_val = df_header_context = df_left_text = df_left_context = df_header = pd.DataFrame()
        if verbose:
            print(f"{val_num}/{len(l_digit_id)}: {val_id }")
        try:
            df_val, df_header_context, df_left_context = get_numeric_value_context(val_id)
            if df_val.shape[0] > 0:
                df_val.to_csv(value_file, index=False)
            if df_header_context.shape[0] > 0:
                df_header_context.to_csv(value_header_file, index=False)
            if df_left_context.shape[0] > 0:
                df_left_context.to_csv(value_left_file, index=False)
            df_val_all = pd.concat([df_val_all, df_val])
            df_header_context_all = pd.concat([df_header_context_all, df_header_context])
            df_left_context_all = pd.concat([df_left_context_all, df_left_context])
        except Exception as e:
            if verbose:
                print(f"unable to process this value: {cell_val}")
                print(e)
    return df_val_all, df_header_context_all, df_left_context_all


def safely_get_value_context(file, verbose=False, overwrite=True, filename_replace=None):
    try:
        return get_value_context(file, verbose=verbose, overwrite=overwrite, filename_replace=filename_replace)
    except Exception as e:
        if verbose:
            print('unable to structure value')
            print(e)
        return pd.DataFrame(), pd.DataFrame(), pd.DataFrame()


def count_keywords(files: list,
                   keywords: list,
                   text_col='text', count_col='text'):
    # l_files = df['file'].unique().tolist()
    df = pd.DataFrame()
    for file in files:
        df_ = pd.read_csv(file)
        df_['file'] = file
        df = pd.concat([df, df_])
    df = df.reset_index(drop=True)
    keywords = [str(keyword).lower() for keyword in keywords]
    df[text_col] = df[text_col].map(str)
    df[text_col] = [text.lower() for text in df[text_col]]
    keyword_count = {}
    for keyword in keywords:
        keyword_count[keyword] = len(df.loc[df[text_col].str.contains(keyword), count_col].tolist())
    df_keyword_count = pd.DataFrame(keyword_count.items(), columns=['keyword', 'count'])
    df_keyword_count['count_sum'] = df_keyword_count[['keyword', 'count']].drop_duplicates()['count'].sum()
    df_keyword_count['total_words'] = df.shape[0]
    df_keyword_count['count_frac'] = df_keyword_count['count_sum'] / df_keyword_count['total_words']
    df_keyword_count['count_col'] = count_col
    df_keyword_count['text_col'] = text_col
    return df_keyword_count
