"""
Generic utility functions
"""
import os
import re
import pandas as pd
import numpy as np
from fuzzywuzzy import fuzz
from quantulum3 import parser as qparser
from dateparser.search import search_dates
import unicodedata
from quantulum3 import parser as qparser
import numpy as np


def get_numeric_value_and_unit(l_value):
    l_num_value = []
    l_unit = []
    for value in l_value:
        value = str(value)
        value = value.replace('(', '-').replace(')', '')
        # ## get value in brackets
        # is_bracket, val_bracket, val_out = get_value_in_brackets(value)
        # if is_bracket:
        #     value = '-' + val_bracket
        value_quants = qparser.parse(value)
        if value_quants:
            num_value = value_quants[0].value
            value_unit = value_quants[0].unit.name

        else:
            num_value = np.nan
            value_unit = 'dimensionless'
        l_num_value = l_num_value + [num_value]
        l_unit = l_unit + [value_unit]
    return l_num_value, l_unit


def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')


def get_words_df_above(df_words, word_row):
    ## get the coords of the word
    left = word_row.get('x0')
    top = word_row.get('top')  ## df_word['top']
    right = word_row.get('x1')  ## df_word['x1']
    bottom = word_row.get('bottom')  ## df_word['bottom']
    xbar = word_row.get('xbar')  ## df_word['xbar']
    ybar = word_row.get('ybar')  ## df_word['ybar']
    ## get all values to the below of the cell
    df_above = df_words[(df_words['bottom'] < top) &
                        (df_words['x0'] < right) &
                        (df_words['x1'] > left)
                        ]
    return df_above


def regex_multiplier(value):
    multiplier = ''
    l_dgt = re.findall('\d', str(value))
    l_dgt_unq = take_unique_list(l_dgt)
    if l_dgt_unq == ['0']:
        multiplier = len(l_dgt)
    return multiplier


def classify_value_type(val, dgt_th_up=0.7, dgt_th_lo=0.1):
    val = str(val)
    len_chr = len(re.findall('[a-z]', str(val).lower()))
    len_dgt = len(re.findall('\d', str(val).lower()))
    len_val = len(val)
    len_misc = len_val - (len_chr + len_dgt)
    l_types = [len_chr, len_dgt, len_misc]
    if max(l_types) == len_dgt:
        val_type = 'digit'
    elif max(l_types) == len_chr:
        if len_dgt >= dgt_th_up * len_val:
            val_type = 'digit'
        elif len_dgt <= dgt_th_lo * len_val:
            val_type = 'chr'
        else:
            val_type = 'chr-digit'
    else:
        val_type = 'misc'
    return val_type


def get_quant_to_text_ratio(text):
    len_text = len(text)
    l_quants = qparser.parse(text)
    len_quant = 0
    for quant in l_quants:
        len_quant = len_quant + len(quant.surface)
    return len_quant / len_text


def match_lists(list_1, list_2):
    """
    find the best match between two lists
    :param list_1:
    :param list_2:
    :return:
    """
    l_pairs = [(item_1, item_2) for item_1 in list_1 for item_2 in list_2]
    l_match = [fuzz.ratio(pair[0], pair[1]) / 100 for pair in l_pairs]
    # l_match_spacy = [get_spacy_sim(pair[0], pair[1]) for pair in l_pairs]
    # l_match_sbert = [get_sbert_sim(pair[0], pair[1]) for pair in l_pairs]
    # l_match = [(l_match_fuzz[i] + l_match_spacy[i]) / 2 for i in np.arange(0, len(l_match_spacy), 1)]
    # l_match_ensemble = [ensemble_word_similarity(pair[0], pair[1]) for pair in l_pairs]
    # l_match = l_match_ensemble
    best_match = l_pairs[l_match.index(max(l_match))]  # [0]
    return best_match


def list_comp_dir(df_constituents, global_root, min_match=95):
    l_dir = [f"{global_root}/{subdir}" for subdir in os.listdir(global_root)
             if match_list_with_str(str_=subdir,
                                    list_=df_constituents['ticker'].map(str).tolist())[1] > min_match]
    return l_dir


def list_pdf_dir(l_dir, verbose=False, include_pattern='', exclude_pattern='/_excluded/'):
    l_dir_pdf = []
    for dir_num, comp_dir in enumerate(l_dir):
        if verbose:
            print(f"{dir_num}/{len(l_dir)}: {comp_dir}")
        l_pdf = list_pdf_files([comp_dir], include_pattern=include_pattern, exclude_pattern=exclude_pattern)
        if l_pdf:
            l_parent_dir = take_unique_list(['/'.join(pdf.split('/')[:-2]) for pdf in l_pdf])
            l_dir_pdf = l_dir_pdf + l_parent_dir
    return l_dir_pdf


def list_pdf_files(l_dir, include_pattern='', exclude_pattern='/_excluded/'):
    l_files = []
    for dir in l_dir:
        l_files_ = [file for file in list_full_path(f"{dir}", recursive=True)
                    if ('.pdf' in os.path.splitext(file)[1]) and
                    (exclude_pattern not in file)]
        l_files = l_files + l_files_
    return l_files


def match_list_with_str(list_, str_):
    """
    find the best match between two lists
    :param list_1:
    :param list_2:
    :return:
    """
    l_match = [fuzz.token_set_ratio(str_, item_) for item_ in list_]
    best_match = list_[l_match.index(max(l_match))]
    return best_match, max(l_match)


def take_unique_list(seq) -> list:
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


def list_full_path(d, recursive=False):
    l_full_path = []
    for path in os.listdir(d):
        if recursive and os.path.isdir(os.path.join(d, path)):
            # print(path)
            l_full_path = l_full_path + list_full_path(d=os.path.join(d, path), recursive=recursive)
        else:
            l_full_path = l_full_path + [os.path.join(d, path)]
    return l_full_path


def get_quants_surface(text):
    l_quants = qparser.parse(text=text)
    # l_quants[0].surface
    l_surface = [quant.surface for quant in l_quants]
    return l_surface


def get_quants_ratio(l_text):
    ## get all words with quants in it
    l_quant_surface = [1 if get_quants_surface(text) else 0 for text in l_text]
    ## get all words containing digits
    l_digits = [1 if re.search('\d+', text) else 0 for text in l_text]
    ## get all words containing years
    l_years = [1 if regex_year(text) else 0 for text in l_text]
    ## get all words containing years
    l_dates = [1 if search_dates(text) else 0 for text in l_text]
    ## proportion of quants for the whole page
    # quants_ratio = len(l_quant_surface) / len(l_text)
    return pd.DataFrame({
        'quants_ratio': [np.sum(l_quant_surface) / len(l_text)],
        'digits_ratio': [np.sum(l_digits) / len(l_text)],
        'years_ratio': [np.sum(l_digits) / len(l_text)],
        'dates_ratio': [np.sum(l_dates) / len(l_text)],
    })


def merge_cells(df):
    df = df.sort_values(['x0', 'x1'])
    text = ' '.join(df['text'].tolist())
    x0 = df['x0'].min()
    x1 = df['x1'].max()
    top = df['top'].min()
    bottom = df['bottom'].max()
    xbar = (x0 + x1) / 2
    ybar = (top + bottom) / 2
    upright = df['upright'].unique().tolist()
    direction = df['upright'].unique().tolist()
    return pd.DataFrame({
        'text': text,
        'x0': x0,
        'x1': x1,
        'top': top,
        'bottom': bottom,
        'xbar': xbar,
        'ybar': ybar,
        'upright': upright,
        'direction': direction
    })


def regex_year(text, return_raw_matches=False):
    """
    Return a list of years in text
    :param text:
    :return:
    """
    l_year = []
    res = re.findall('(20|fy|cy|year|yr)(\d+)', text.lower())
    l_year = [t[0] + t[1] for t in res]
    res = []
    for year in l_year:
        # res_ = re.findall(f'({year}-|{year}/)(\d+)', text.lower())
        res_ = re.findall(f'({year}-|{year}/)' + r'(\d{2}|\d{4})', text.lower())
        res = res + res_
    raw_matches = []
    for elem in res:
        raw_matches = raw_matches + [''.join(elem)]
    if return_raw_matches:
        return raw_matches
    for t in res:
        if len(t[1]) == 2:
            l_year = l_year + [t[0][:-3] + t[1]]
        elif len(t[1]) == 4:
            l_year = l_year + [t[1]]
        else:
            l_year = l_year + [t[1]]
    return take_unique_list(l_year)


def identify_cols_within_row(df_row, dist_to_width_ratio=2):
    df_row = df_row.sort_values(['x0', 'x1']).reset_index(drop=True)
    df_row['cell_flag'] = 0
    df_row['text'] = df_row['text'].map(str)
    ## find x-distance between words
    df_row['word_width'] = df_row['x1'] - df_row['x0']
    df_row['word_width'] = df_row['word_width'].abs()
    ## word lenght
    df_row['word_length'] = [len(word) for word in df_row['text']]
    ## avg character width
    df_row['chr_width'] = df_row['word_width'] / df_row['word_length']
    median_chr_width = df_row['chr_width'].median()
    df_row['dist_bw_words'] = df_row['x0'] - df_row.shift(1)['x1']
    df_row['dist_bw_words'] = df_row['dist_bw_words'].abs()
    # df_row['dist_bw_words'].median()
    # df_row[['dist_bw_words', 'dist_to_width_ratio', 'word_width', 'text']]
    df_row['dist_to_width_ratio'] = df_row['dist_bw_words'] / median_chr_width  ## df_row['word_width']
    ## set first word as a new cell
    df_row.loc[0, 'cell_flag'] = 1
    ## compute quant/text ratio for cells
    df_row['quant_to_text_ratio'] = [get_quant_to_text_ratio(str(text)) for text in df_row['text']]
    ## set all words that are sufficiently separated from the previous word as a new cell
    df_row.loc[(df_row['dist_to_width_ratio'] > dist_to_width_ratio), 'cell_flag'] = 1
    # df_row[['text', 'cell_flag']]
    # ## set a smaller threshold for quants to separate words
    # df_row.loc[(df_row['quant_to_text_ratio'] > 0.85) &
    #            (df_row['dist_to_width_ratio'] > 0.75),
    #            'cell_flag'] = 1
    # ## set all words that are sufficiently separated from the previous word as a new cell
    # df_row.loc[((df_row['dist_to_width_ratio'] > 0.75) |
    #            (df_row['dist_bw_words'] > 2 * median_chr_width)) &
    #            (df_row['dist_bw_words'] > 2 * median_chr_width),
    #            'cell_flag'] = 1
    ## use a smaller threshold for quantitative numbers
    # df_row.loc[(df_row['quant_to_text_ratio'] > 0.85) &
    #            ((df_row['dist_to_width_ratio'] > 0.5) |
    #            (df_row['dist_bw_words'] > 2 * df_row['chr_width'])) &
    #            (df_row['dist_bw_words'] > 2 * median_chr_width),
    #            'cell_flag'] = 1
    ## set words with na cell_flag to 0 (not a new cell)
    df_row.loc[df_row['cell_flag'].isnull(), 'cell_flag'] = 0
    ## shift col num up by 1 everytime cell flag is 1 (words between two words with cell flag 1 are all one cell)
    df_row['col_within_row'] = df_row['cell_flag'].cumsum()
    return df_row


def get_words_df_below(df_words, word_row):
    # ## get the coords of the word
    # df_word = df_words.iloc[[fuzz.partial_ratio(word, text) > 70 for text in df_words['text'].values],
    #           :].copy()
    # return df_words[(df_words['ybar'] > df_word['top'].values[0]) &
    #                 (df_words['ybar'] < df_word['bottom'].values[0]) &
    #                 (df_words['xbar'] < df_word['x0'].values[0])
    #                 ]
    left = word_row.get('x0')
    top = word_row.get('top')  ## df_word['top']
    right = word_row.get('x1')  ## df_word['x1']
    bottom = word_row.get('bottom')  ## df_word['bottom']
    xbar = word_row.get('xbar')  ## df_word['xbar']
    ybar = word_row.get('ybar')  ## df_word['ybar']
    ## get all values to the below of the cell
    df_below = df_words[(df_words['top'] > bottom) &
                        (df_words['x0'] < right) &
                        (df_words['x1'] > left)
                        ]
    return df_below


def get_words_df_above(df_words, word_row):
    ## get the coords of the word
    left = word_row.get('x0')
    top = word_row.get('top')  ## df_word['top']
    right = word_row.get('x1')  ## df_word['x1']
    bottom = word_row.get('bottom')  ## df_word['bottom']
    xbar = word_row.get('xbar')  ## df_word['xbar']
    ybar = word_row.get('ybar')  ## df_word['ybar']
    ## get all values to the below of the cell
    df_above = df_words[(df_words['bottom'] < top) &
                        (df_words['x0'] < right) &
                        (df_words['x1'] > left)
                        ]
    return df_above


def get_words_df_left(df_words, word_row):
    # ## get the coords of the word
    # df_word = df_words.iloc[[fuzz.partial_ratio(word, text) > 70 for text in df_words['text'].values],
    #           :].copy()
    # return df_words[(df_words['ybar'] > df_word['top'].values[0]) &
    #                 (df_words['ybar'] < df_word['bottom'].values[0]) &
    #                 (df_words['xbar'] < df_word['x0'].values[0])
    #                 ]
    left = word_row.get('x0')
    top = word_row.get('top')  ## df_word['top']
    right = word_row.get('x1')  ## df_word['x1']
    bottom = word_row.get('bottom')  ## df_word['bottom']
    xbar = word_row.get('xbar')  ## df_word['xbar']
    ybar = word_row.get('ybar')  ## df_word['ybar']
    ## get all values to the left of the cell
    # df_left = df_words[(df_words['x1'] < left) &
    #                    (df_words['ybar'] > (top - (bottom - top) / 2)) &
    #                    (df_words['ybar'] < bottom + (bottom - top) / 2)
    #                    ]
    df_left = df_words[(df_words['x1'] < left) &
                       (df_words['top'] < bottom) &
                       (df_words['bottom'] > top)
                       ]
    return df_left


def get_words_df_right(df_words, word_row):
    # df_word = df_words.iloc[[fuzz.partial_ratio(word, text) > 85 for text in df_words['text'].values],
    #           :].copy()
    # ## ignore words that are too far from each other
    # return df_words[(df_words['bottom'] > np.min(df_word['top'].values)) &
    #                 (df_words['top'] < np.max(df_word['bottom'].values)) &
    #                 (df_words['xbar'] > np.max(df_word['x1'].values))
    #                 ]
    left = word_row.get('x0')
    top = word_row.get('top')  ## df_word['top']
    right = word_row.get('x1')  ## df_word['x1']
    bottom = word_row.get('bottom')  ## df_word['bottom']
    xbar = word_row.get('xbar')  ## df_word['xbar']
    ybar = word_row.get('ybar')  ## df_word['ybar']
    ## get all values to the below of the cell
    df_right = df_words[(df_words['top'] < bottom) &
                        (df_words['bottom'] > top) &
                        (df_words['x0'] > right)
                        ]
    return df_right


def word_forms(word, sep='|'):
    """
    given a word, create alternative forms, e.g. word, Word, WORD
    :param word: input word
    :param sep: separator to join various forms of the word, defaults to '|'
    :return:
    """
    l_words = [str(word).lower(), word.capitalize(), word.upper()]
    return sep.join(l_words)


def get_interval_overlap(a, b):
    return max(0, min(a[1], b[1]) - max(a[0], b[0]))