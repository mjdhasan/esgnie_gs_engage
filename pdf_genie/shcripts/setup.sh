#!/bin/bash
python -m pip install --upgrade pip
pip install pandas
pip install numpy
pip install quantulum3
pip install pdfplumber
pip install fuzzywuzzy
pip install dateparser