"""
Segment words files
"""

from init import DIR_SCRAPERS
import argparse
import numpy as np
import os
import multiprocessing as mp
from img_genie.parsers.img_parser import safely_write_word_segments
from img_genie.utils.util import list_files_matching_pattern


def segment_word_files(root_dir,
                       file_pattern='df_quants_',
                       verbose=True,
                       parallel=False,
                       n_cpu_frac=1,
                       max_files=-1,
                       reverse=0):
    ## get list of companies mathcing input tickers
    l_dir = [f"{root_dir}/{subdir}" for subdir in os.listdir(root_dir) if os.path.isdir(f"{root_dir}/{subdir}")]
    ## get all df_words files
    l_files = list_files_matching_pattern(l_dir, pattern=file_pattern)
    ## reverse files
    if reverse:
        l_files.reverse()
    ## truncate by max_files
    if max_files > 0:
        l_files = l_files[:max_files]
    ## parallel loop
    if parallel:
        n_cpu = int(np.ceil(n_cpu_frac * mp.cpu_count()))
        if verbose:
            print(f'running parallel loop with {n_cpu} cores')
        pool = mp.Pool(n_cpu)
        # l_pred = pool.map(search_docs, ll_docs)
        results = pool.map(safely_write_word_segments, l_files)
        pool.close()
    ## sequential loop
    else:
        if verbose:
            print(f'running sequential loop')
        for file_num, file in enumerate(l_files):
            if verbose:
                print(f"{file_num}/{len(l_files)}: {file}")
            safely_write_word_segments(file)


if __name__ == "__main__":
    ## Create the parser
    arg_parser = argparse.ArgumentParser(prog='segment_image',
                                         usage='%(prog)s [options]',
                                         description='extract quants from pdf files')
    ## add arg: root dir
    arg_parser.add_argument('-r', '--root_dir',
                            metavar='root_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/www.sasb.org',
                            help='root dir')
    ## add arg: root dir
    arg_parser.add_argument('-f', '--file_pattern',
                            metavar='file_pattern',
                            nargs='?',
                            type=str,
                            default=f'df_quants',
                            help='file pattern to match for reading words')
    ## add arg: prefix for text file
    arg_parser.add_argument('-v', '--verbose',
                            metavar='verbose',
                            nargs='?',
                            type=int,
                            default=0,
                            help='run in verbose mode')
    ## add arg: parallel
    arg_parser.add_argument('-p', '--parallel',
                            metavar='parallel',
                            nargs='?',
                            type=int,
                            default=0,
                            help='run in parallel mode')
    ## add arg: n_cpu frac
    arg_parser.add_argument('-n', '--n_cpu_frac',
                            metavar='n_cpu_frac',
                            nargs='?',
                            type=float,
                            default=0.5,
                            help='number of CPUs to use for parallel processing')
    ## add arg: max files to process
    arg_parser.add_argument('-m', '--max_files',
                            metavar='max_files',
                            nargs='?',
                            type=int,
                            default=-1,
                            help='number of CPUs to use for parallel processing')
    ## add arg: reverse mode
    arg_parser.add_argument('-rev', '--reverse',
                            metavar='reverse',
                            nargs='?',
                            type=int,
                            default=0,
                            help='loop files in reverse order')
    ## parse args
    args, unknown_args = arg_parser.parse_known_args()
    print(args)
    ## set root dir
    root_dir = args.root_dir
    ## set root dir
    file_pattern = args.file_pattern
    ## set root dir
    verbose = bool(args.verbose)
    ## max_files
    max_files = args.max_files
    ## parallel flag
    parallel = bool(args.parallel)
    ## parallel flag
    n_cpu_frac = args.n_cpu_frac
    ## set reverse flag
    reverse = bool(args.reverse)
    """
    Write page segments
    """
    segment_word_files(root_dir,
                       file_pattern=file_pattern,
                       verbose=verbose,
                       parallel=parallel,
                       n_cpu_frac=n_cpu_frac,
                       max_files=max_files,
                       reverse=reverse)


