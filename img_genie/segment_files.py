"""
Segment pdf files
"""

from init import DIR_SCRAPERS
import argparse
import numpy as np
import os
import multiprocessing as mp
from img_genie.parsers.img_parser import write_page_segments, safely_write_page_segments
from img_genie.utils.util import list_full_path, take_unique_list, list_pdf_dir, list_pdf_files


def write_img_segments(root_dir,
                       verbose,
                       max_files,
                       parallel,
                       n_cpu_frac,
                       reverse):
    ## get list of companies for which pdf reports have been dowloaded
    l_dir_pdf = list_pdf_dir([root_dir])
    ## list all pdf files
    l_files = list_pdf_files(l_dir_pdf)
    if verbose:
        print(f"total number of files: {len(l_files)}")
    ## reverse files
    if reverse:
        l_files.reverse()
    ## truncate files to max_files
    if max_files > 0:
        l_files = l_files[:max_files]
    ## parallel loop
    if parallel:
        n_cpu = int(np.ceil(n_cpu_frac * mp.cpu_count()))
        if verbose:
            print(f'running parallel loop with {n_cpu} cores')
        pool = mp.Pool(n_cpu)
        results = pool.map(safely_write_page_segments, l_files)
        pool.close()
    ## sequential loop
    else:
        if verbose:
            print(f'running sequential loop')
        for file_num, file in enumerate(l_files):
            try:
                write_page_segments(file, verbose=verbose)
            except Exception as e:
                print(e)


if __name__ == "__main__":
    ## Create the parser
    arg_parser = argparse.ArgumentParser(prog='segment_image',
                                         usage='%(prog)s [options]',
                                         description='extract quants from pdf files')
    ## add arg: root dir
    arg_parser.add_argument('-r', '--root_dir',
                            metavar='root_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/www.sasb.org',
                            help='root dir')
    ## add arg: prefix for text file
    arg_parser.add_argument('-v', '--verbose',
                            metavar='verbose',
                            nargs='?',
                            type=int,
                            default=0,
                            help='run in verbose mode')
    ## add arg: parallel
    arg_parser.add_argument('-p', '--parallel',
                            metavar='parallel',
                            nargs='?',
                            type=int,
                            default=1,
                            help='run in parallel mode')
    ## add arg: n_cpu frac
    arg_parser.add_argument('-n', '--n_cpu_frac',
                            metavar='n_cpu_frac',
                            nargs='?',
                            type=float,
                            default=0.5,
                            help='number of CPUs to use for parallel processing')
    ## add arg: max files to process
    arg_parser.add_argument('-m', '--max_files',
                            metavar='max_files',
                            nargs='?',
                            type=int,
                            default=-1,
                            help='max number of files to process')
    ## add arg: reverse mode
    arg_parser.add_argument('-rev', '--reverse',
                            metavar='reverse',
                            nargs='?',
                            type=int,
                            default=0,
                            help='loop files in reverse order')
    ## parse args
    args, unknown_args = arg_parser.parse_known_args()
    print(args)
    ## set root dir
    root_dir = args.root_dir
    ## set root dir
    verbose = bool(args.verbose)
    ## max_files
    max_files = args.max_files
    ## parallel flag
    parallel = bool(args.parallel)
    ## parallel flag
    n_cpu_frac = args.n_cpu_frac
    ## set reverse flag
    reverse = bool(args.reverse)
    """
    Write img segments
    """
    write_img_segments(root_dir=root_dir,
                       verbose=verbose,
                       max_files=max_files,
                       parallel=parallel,
                       n_cpu_frac=not n_cpu_frac,
                       reverse=reverse)



