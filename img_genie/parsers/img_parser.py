"""
Function for parsing document images
"""

from init import MAX_FILENAME_LEN
import cv2
import os
import pytesseract
import pdfplumber
import pandas as pd
import numpy as np
from img_genie.utils.util import calc_word_density


def locate_lines(l_n_overlap, grid, zero_level=0):
    """
    locate empty spaces to draw grid lines
    :param l_n_overlap:
    :param grid:
    :param zero_level:
    :return:
    """
    df_overlap = pd.DataFrame()
    df_overlap['n_overlap'] = l_n_overlap
    # gradient of word density
    df_overlap['switch'] = df_overlap['n_overlap'].diff(periods=1)
    df_overlap['grid'] = grid[1:]
    # starting point of zero density region
    l_zero_start = df_overlap[(df_overlap['n_overlap'] <= zero_level) &
                              (df_overlap['switch'] <= -1)
                              ]['grid'].tolist()
    # ending point of zero density region
    l_zero_end = []
    for zero in l_zero_start:
        if df_overlap[(df_overlap['grid'] > zero) & (df_overlap['switch'] >= 1)].shape[0] > 0:
            zero_end = df_overlap[(df_overlap['grid'] > zero) &
                                  (df_overlap['switch'] >= 1)]['grid'].min()
        else:
            zero_end = df_overlap[(df_overlap['grid'] > zero) &
                                  (df_overlap['n_overlap'] <= zero_level)]['grid'].max()
        l_zero_end = l_zero_end + [zero_end]
    # mean of zero density region
    l_zero_mean = np.round((np.array(l_zero_start) + np.array(l_zero_end)) / 2)
    df_zero_region = pd.DataFrame()
    df_zero_region['region_start'] = l_zero_start
    df_zero_region['region_end'] = l_zero_end
    df_zero_region['region_width'] = df_zero_region['region_end'] - df_zero_region['region_start']
    df_zero_region['region_mean'] = l_zero_mean
    df_zero_region['density_level'] = zero_level
    return df_zero_region


def get_empty_xy_regions(df_group, line_width=1, zero_level=0):
    ## create grid
    x_grid = np.arange(df_group['left'].min(), df_group['right'].max(), line_width)
    y_grid = np.arange(df_group['top'].min(), df_group['bottom'].max(), line_width)
    ## x overlap
    l_n_overlap_x = calc_word_density(df=df_group, grid=x_grid, orientation='x')
    l_n_overlap_y = calc_word_density(df=df_group, grid=y_grid, orientation='y')
    ## lines with zero word density
    df_zero_x = locate_lines(l_n_overlap=l_n_overlap_x, grid=x_grid, zero_level=zero_level)
    df_zero_y = locate_lines(l_n_overlap=l_n_overlap_y, grid=y_grid, zero_level=zero_level)
    ## fill na values
    df_zero_x.loc[df_zero_x['region_start'].isnull(), 'region_start'] = df_group['left'].min()
    df_zero_x.loc[df_zero_x['region_end'].isnull(), 'region_end'] = df_group['right'].max()
    df_zero_y.loc[df_zero_y['region_start'].isnull(), 'region_start'] = df_group['top'].min()
    df_zero_y.loc[df_zero_y['region_end'].isnull(), 'region_end'] = df_group['bottom'].max()
    df_zero_x['region_width'] = df_zero_x['region_end'] - df_zero_x['region_start']
    df_zero_x['region_mean'] = (df_zero_x['region_end'] + df_zero_x['region_start']) / 2
    df_zero_y['region_width'] = df_zero_y['region_end'] - df_zero_y['region_start']
    df_zero_y['region_mean'] = (df_zero_y['region_end'] + df_zero_y['region_start']) / 2
    return df_zero_x, df_zero_y


def compute_group_area(df_stats, group_col='xy_group'):
    df_group_area = df_stats.groupby([group_col]
                                     ).apply(lambda x:
                                             (x['right'].max() - x['left'].min()) *
                                             (x['bottom'].max() - x['top'].min())
                                             ).reset_index()
    df_group_area.columns = [group_col, f'{group_col}_area']
    df_stats = pd.merge(left=df_stats, right=df_group_area,
                        on=[group_col], how='left')
    return df_stats


def remove_cc_equal_group(df_group, df_stats):
    group_width = (df_group['right'].max() - df_group['left'].min())
    group_height = (df_group['bottom'].max() - df_group['top'].min())
    group_area = group_width * group_height
    # group_area_frac = group_width * group_height / img_area
    ## check if there is a cc with the same area as the group; if so, remove it.
    if df_group[df_group['bbox_area'] == group_area].shape[0] > 0:
        cc_group = df_group[df_group['bbox_area'] == group_area]['cc_num'].tolist()
        df_group = df_group[~df_group['cc_num'].isin(cc_group)]
        df_stats.loc[df_stats['cc_num'].isin(cc_group), 'x_expand'] = 0
        df_stats.loc[df_stats['cc_num'].isin(cc_group), 'y_expand'] = 0
        df_stats.loc[df_stats['cc_num'].isin(cc_group), 'xy_expand'] = 0
    return df_group, df_stats


def analyze_cc(img, connectivity=8, ltype=cv2.CV_32S):
    ## proprietary function (to do image analysis) removed
    pass


def get_stats_df(stats):
    ## get df of stats
    df_stats = pd.DataFrame(stats, columns=['x', 'y', 'w', 'h', 'area'])
    df_stats['left'] = df_stats['x']
    df_stats['right'] = df_stats['x'] + df_stats['w']
    df_stats['top'] = df_stats['y']
    df_stats['bottom'] = df_stats['y'] + df_stats['h']
    df_stats['bbox_area'] = (df_stats['right'] - df_stats['left']) * (df_stats['bottom'] - df_stats['top'])
    df_stats['cc_num'] = df_stats.index
    return df_stats


def write_page_segments(file, verbose=False, overwrite=True):
    parent_folder = '/'.join(file.split('/')[:-2])
    csv_folder = os.path.join(parent_folder, 'csv')
    os.makedirs(csv_folder, exist_ok=True)
    img_folder = os.path.join(parent_folder, 'img')
    os.makedirs(img_folder, exist_ok=True)
    filename = os.path.split(file)[-1]
    filename = os.path.splitext(filename)[0]
    try:
        plumber_doc = pdfplumber.open(file)
    except Exception as e:
        if verbose:
            print('unable to read pdf doc')
            print(e)
        return e
    for pdf_page_num, pdf_page in enumerate(plumber_doc.pages):
        if verbose:
            print(f"{pdf_page_num}/{len(plumber_doc.pages)}: {file}")
        output_file = f"{csv_folder}/{filename}_pagenum-{pdf_page.page_number}_segments.csv"
        output_file_text = f"{csv_folder}/{filename}_pagenum-{pdf_page.page_number}_segment-text.csv"
        output_file_img = f"{img_folder}/{filename}_pagenum-{pdf_page.page_number}.png"
        if os.path.isfile(output_file) and os.path.isfile(output_file_text) and (not overwrite):
            if verbose:
                print('output files already exists: skipping')
            continue
        pdf_page.to_image(resolution=500).save(f'{output_file_img}')
        pdf_img = cv2.imread(f'{output_file_img}')
        ## segment image
        try:
            ## perform cc analysis
            numLabels, labels, stats, centroids = analyze_cc(img=pdf_img)
            ## get df_stats
            df_stats = get_stats_df(stats)
            df_segments = segment_page(df_stats)
            df_segments = compute_group_area(df_segments, group_col='xy_group')
            if df_segments.shape[0] > 0:
                df_segments.to_csv(output_file, index=False)
            ## write text for each segment
            df_segment_text = pd.DataFrame()
            for group_ in df_segments['xy_group'].unique().tolist():
                try:
                    df_ = df_segments[df_segments['xy_group'] == group_]
                    segment_img_ = pdf_img[max([0, int(df_['top'].min() * 0.98)]):int(df_['bottom'].max() * 1.02),
                                   max([0, int(df_['left'].min() * 0.98)]):int(df_['right'].max() * 1.02), :]
                    df_text_ = pytesseract.image_to_data(segment_img_,
                                                         config='--psm 1',
                                                         output_type=pytesseract.Output.DICT)
                    df_text_ = pd.DataFrame(df_text_)
                    df_text_['xy_group'] = group_
                    df_segment_text = pd.concat([df_segment_text, df_text_], axis=0)
                except Exception as e:
                    print('unable to get segment text')
                    print(e)
                ## write to csv
                if df_segment_text.shape[0] > 0:
                    df_segment_text.to_csv(output_file_text, index=False)
        except Exception as e:
            print('unable to segment image')
            print(e)


def flag_internal_gap_width(df_zero, gap_thresh=3):
    max_width = df_zero['region_width'].max()
    median_gap_width = df_zero['region_width'].median()
    gap_size_internal = max_width / median_gap_width
    return gap_size_internal > gap_thresh


def flag_to_split_group(df_group, df_zero, orientation, cc_w, col_end, col_start,
                        min_gap_width, min_cc_width_multiple, width_before_after_range, gap_thresh):
    l_gap_width = sorted(df_zero['region_width'], reverse=True)
    max_width = 0
    region_midpoint = np.nan
    mask_before = [False] * len(df_group)
    mask_after = [False] * len(df_group)
    for max_width in l_gap_width:
        # max_width_frac = max_width / pdf_img.shape[0]
        ## get region start, midpoint, and end
        region_midpoint = df_zero[df_zero['region_width'] == max_width]['region_mean'].tolist()[0]
        region_start = df_zero[df_zero['region_width'] == max_width]['region_start'].tolist()[0]
        region_end = df_zero[df_zero['region_width'] == max_width]['region_end'].tolist()[0]
        ## median cc width
        median_cc_width = df_group[cc_w].median()
        ## df before the max width empty region
        mask_before = df_group[col_end] < region_end
        ## width before
        width_before = region_end - df_group[mask_before][col_start].min()
        ## mask for ccs after the empty region
        mask_after = df_group[col_start] > region_start
        ## width after
        width_after = df_group[mask_after][col_end].max() - region_start
        ## total width
        group_width = width_before + width_after
        ## create flags to check whether or not to split the df
        internal_gap_width_flag = flag_internal_gap_width(df_zero, gap_thresh=gap_thresh)
        abs_min_width_flag = max_width > min_gap_width
        gap_to_cc_width_flag = max_width > min_cc_width_multiple * median_cc_width
        width_before_after_flag = (width_after / group_width < max(width_before_after_range)) and \
                                  (width_after / group_width > min(width_before_after_range))
        split_flag = internal_gap_width_flag and \
                     gap_to_cc_width_flag and \
                     abs_min_width_flag and \
                     width_before_after_flag
        if split_flag:
            return split_flag, max_width, region_midpoint, mask_before, mask_after
    return False, max_width, region_midpoint, mask_before, mask_after


def split_img_along_an_axis(df_group, df_zero,
                            orientation: str = 'y',
                            group_col: str = 'y_group',
                            gap_thresh: float = 3,
                            min_cc_width_multiple: float = 2,
                            min_gap_width: float = 15,
                            width_before_after_range: tuple = (0.1, 0.9)):
    if group_col not in df_group.columns:
        df_group[group_col] = ''
    ## set start and end cols for ccs
    col_end = 'bottom'
    col_start = 'top'
    cc_w = 'h'
    if orientation == 'x':
        col_end = 'right'
        col_start = 'left'
        cc_w = 'w'
    ## find largest empty regions
    split_flag, \
    max_width, \
    region_midpoint, \
    mask_before, \
    mask_after = flag_to_split_group(df_group, df_zero, orientation, cc_w, col_end, col_start,
                                     min_gap_width, min_cc_width_multiple, width_before_after_range, gap_thresh)
    ## store max width in df_group
    df_group[f'max_empty_width_{orientation}'] = df_group[f'max_empty_width_{orientation}'] + '.' + str(max_width)
    if split_flag:
        ## set ccs before to x_group = 1
        df_group.loc[mask_before, group_col] = df_group.loc[mask_before, group_col] + '1.'
        ## set ccs after to x_group = 2
        df_group.loc[mask_after, group_col] = df_group.loc[mask_after, group_col] + '2.'
    else:
        # print(f"max_width, {max_width}, less than median_cc_width, {median_cc_width}")
        df_group[f'{orientation}_expand'] = 0
    return df_group, region_midpoint, max_width


def segment_page(df_stats,
                 gap_thresh: float = 3,
                 min_cc_width_multiple: float = 2,
                 min_gap_width: float = 15,
                 width_before_after_range: tuple = (0.1, 0.9),
                 verbose: bool = False):
    ## proprietary function (to segment image) removed
    pass


def safely_write_word_segments(file,
                               text_replace='df_quants_',
                               gap_thresh=0.5,
                               min_cc_width_multiple=0.5,
                               min_gap_width=0.5,
                               width_before_after_range=(0.1, 0.9),
                               verbose=False):
    file_dir = '/'.join(file.split('/')[:-1])
    filename_in = file.split('/')[-1].replace('.csv', '')
    if len(filename_in) > MAX_FILENAME_LEN:
        filename_in = filename_in[:MAX_FILENAME_LEN]
    filename_out = filename_in.replace(text_replace, '') + '_words-segments.csv'
    output_file = f"{file_dir}/{filename_out}"
    # output_file = file.replace(text_replace, '').replace('.csv', '_words-segments.csv')
    if os.path.isfile(output_file):
        if verbose:
            print('output file already exists: skipping')
            return {'message': f'Skipping: output file, {output_file}, already exists.'}
    df_segments = pd.DataFrame()
    try:
        df_ = pd.read_csv(file)
        ## add left right
        if 'left' not in df_.columns:
            df_['left'] = df_['x0']
        if 'right' not in df_.columns:
            df_['right'] = df_['x1']
        ## add height
        if 'h' not in df_.columns:
            df_['h'] = df_['bottom'] - df_['top']
        ## add width
        if 'w' not in df_.columns:
            df_['w'] = df_['right'] - df_['left']
        ## add bbox area
        if 'bbox_area' not in df_.columns:
            df_['bbox_area'] = (df_['right'] - df_['left']) * (df_['bottom'] - df_['top'])
        ## add cc_num
        if 'cc_num' not in df_.columns:
            df_['cc_num'] = df_.index.tolist()
        df_segments = segment_page(df_,
                                   gap_thresh,
                                   min_cc_width_multiple,
                                   min_gap_width,
                                   width_before_after_range,
                                   verbose)
        df_segments = compute_group_area(df_segments, group_col='xy_group')
    except Exception as e:
        if verbose:
            print(f"unable to segment data")
            print(e)
        return {'message': f'Error: {e}'}
    if df_segments.shape[0] > 0:
        df_segments.to_csv(output_file, index=False)
        if verbose:
            print(f'wrote output file: {output_file}')
        return {'message': f'Written: {output_file}'}
    else:
        return {'message': f'Not Written: segments data has shape {df_segments.shape}'}



def safely_write_page_segments(file, verbose=False):
    try:
        return write_page_segments(file, verbose)
    except Exception as e:
        if verbose:
            print(f"unable to segment page for {file}")
            print(e)