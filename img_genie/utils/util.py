import numpy as np
import os
import re


def list_pdf_dir(l_dir, verbose=False):
    l_dir_pdf = []
    for dir_num, comp_dir in enumerate(l_dir):
        if verbose:
            print(f"{dir_num}/{len(l_dir)}: {comp_dir}")
        l_pdf = list_pdf_files([comp_dir])
        if l_pdf:
            l_parent_dir = take_unique_list(['/'.join(pdf.split('/')[:-2]) for pdf in l_pdf])
            l_dir_pdf = l_dir_pdf + l_parent_dir
    return l_dir_pdf


def list_pdf_files(l_dir):
    l_files = []
    for dir in l_dir:
        l_files_ = [file for file in list_full_path(f"{dir}", recursive=True) if '.pdf' in os.path.splitext(file)[1]]
        l_files = l_files + l_files_
    return l_files


def list_files_matching_pattern(l_dir, pattern: str = ''):
    l_files = []
    for dir in l_dir:
        if os.path.isdir(dir):
            dir_files = [file for file in list_full_path(dir, recursive=True) if re.search(pattern, file)]
            # dir_files = [file for file in dir_files if 'df_words' in file]
            l_files = l_files + dir_files
    return l_files


def list_full_path(d, recursive=False):
    l_full_path = []
    for path in os.listdir(d):
        if recursive and os.path.isdir(os.path.join(d, path)):
            # print(path)
            l_full_path = l_full_path + list_full_path(d=os.path.join(d, path), recursive=recursive)
        else:
            l_full_path = l_full_path + [os.path.join(d, path)]
    return l_full_path


def take_unique_list(seq) -> list:
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


def intersect(i1, i2):
    return max(i1[0], i2[0]) < min(i1[1], i2[1])


def find_intersection(intervals, i):
    return [j for j in intervals if intersect(i, j)]


def calc_word_density(df, grid, orientation='x', density=False):
    """
    calculate number of words that overlap with a grid interval
    :param df:
    :param grid:
    :param orientation:
    :return:
    """
    l_words = []
    # for each x/y interval, find the number of words that overlap with it
    if orientation == 'x':
        l_words = [(row['left'], row['right']) for row_num, row in df.iterrows()]
    if orientation == 'y':
        l_words = [(row['top'], row['bottom']) for row_num, row in df.iterrows()]
    # calc num of words that overlap with a line
    l_n_overlap = []
    for i in np.arange(1, len(grid), 1):
        n_overlap = len(find_intersection(l_words, (grid[i - 1], grid[i])))
        if density:
            density_overlap = n_overlap / len(l_words)
            l_n_overlap = l_n_overlap + [density_overlap]
        else:
            l_n_overlap = l_n_overlap + [n_overlap]

    return l_n_overlap