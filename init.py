import platform


if 'Linux' in platform.platform():
    HOME_DIR = '/home/ubuntu'
    platform_user = ''
    ## dir for web scraping output
    DIR_SCRAPERS = f'{HOME_DIR}/data'
    ## dir for web scraping output
    DIR_MODELS = f'/ebs-0/models'
    DIR_OUTPUT = '/ebs-0/data/api'
    DIR_VENV = '/ebs-0/venvs'
    DIR_CODE = '/ebs-0/code'
    UPDATE_CHROME_PATH = False
elif 'i386-64bit' in platform.platform():
    ROOT_DIR = f'~/data'
    platform_user = 'majid'
    ## dir for web scraping output
    DIR_SCRAPERS = f'/Users/{platform_user}/Dropbox/data/scrapers'
    ## dir for web scraping output
    DIR_MODELS = f'/Users/{platform_user}/Dropbox/models'
    DIR_OUTPUT = '/Users/majid/Dropbox/data/scrapers/_api'
    DIR_VENV = '/Users/majid/.virtualenvs'
    DIR_CODE = '/Users/majid/Dropbox/code/ESGenie-suite'
    UPDATE_CHROME_PATH = True


AUTHORISED_USERS_FILE = f'{DIR_SCRAPERS}/authorised_users.csv'
MAX_FILENAME_LEN = 175