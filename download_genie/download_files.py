"""
Title: Download files from a given url
Author: Majid Hasan
Description:
    - search an entity name
    - find homepage of the entity
"""

from download_genie.init import L_EXT_DOWNLOAD
import requests
import argparse
import os
from init import DIR_SCRAPERS
import pandas as pd
from urllib.parse import urlparse
from download_genie.utils.util import launch_browser, safely_launch_browser, create_google_query, take_unique_list, list_full_path
from download_genie.utils.util import make_generic_search_url, parse_search_pages, download_file_from_href, slugify
import re
from bs4 import BeautifulSoup
import time

# L_EXT_DOWNLOAD = ['.pdf', '.doc', '.docx', '.html', '.txt', '.ppt', '.xls', '.xlsx', '.pptx']


# def make_generic_search_url_old(url):
#     if 'http' not in url:
#         url = 'http://' + url
#         url = '*.' + '.'.join(url.split('.')[1:])
#     return url


def read_url_files(root_dir, l_name, file_name, name_column):
    df_name_url = pd.DataFrame()
    for name in l_name:
        name_dir = f"{root_dir}/{name}"
        if not os.path.isdir(name_dir):
            continue
        l_files_url = [file for file in list_full_path(name_dir, recursive=True)
                       if (file_name in file)]
        for file in l_files_url:
            df_ = pd.read_csv(file)
            df_name_url = pd.concat([df_name_url, df_])  #  df_name_url.append(df_)
    df_name_url = df_name_url.reset_index(drop=True)
    l_col_url = [col for col in df_name_url.columns if 'url' in col]
    df_name_url = df_name_url[[name_column] + l_col_url]
    df_name_url = df_name_url.melt(id_vars=[name_column],
                                   var_name='variable',
                                   value_name='url_')
    df_name_url = df_name_url.rename(columns={'url_': 'url'})
    df_name_url = df_name_url.drop_duplicates([name_column, 'url']).reset_index(drop=True)
    return df_name_url


def find_page_urls(keyword, url, filetype='pdf', l_year=None, max_pagenum=2, l_ext_download=L_EXT_DOWNLOAD):
    if l_year is None:
        l_year = []
    ## create search url for each year
    l_search_url = []
    if l_year:
        for year in l_year:
            # search_url = create_google_query(keywords=[year, keyword], site=url, filetype=filetype)
            search_url_1 = create_google_query(keywords=[year, keyword], site=url.replace('*.', ''),
                                               filetype=filetype)
            l_search_url = l_search_url + [search_url_1]  # [search_url] +
    else:
        search_url_1 = create_google_query(keywords=[keyword], site=url.replace('*.', ''),
                                           filetype=filetype)
        l_search_url = l_search_url + [search_url_1]
    ## check if google is blocking us
    ## parse search pages
    df_results = parse_search_pages(l_search_url, max_pagenum, l_ext=l_ext_download)
    # if df_results.shape[0] == 0:
    #     l_search_url = []
    #     # search_url = create_google_query(keywords=[keyword], site=url, filetype=filetype)
    #     search_url_1 = create_google_query(keywords=[keyword], site=url.replace('*.', ''), filetype=filetype)
    #     l_search_url = l_search_url + [search_url_1]  # [search_url] +
    #     df_results = parse_search_pages(l_search_url, max_pagenum, l_ext=L_EXT_DOWNLOAD)
    ## reset index
    df_results = df_results.reset_index(drop=True)
    return df_results


if __name__ == "__main__":
    ## Create the parser
    arg_parser = argparse.ArgumentParser(prog='download_files_from_url',
                                         usage='%(prog)s [options]',
                                         description='find the homepage of a given entity')
    ## add arg: root dir
    arg_parser.add_argument('-r', '--root_dir',
                            metavar='root_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/app.esgbook.com',
                            help='root dir')
    ## add arg: list of companies
    arg_parser.add_argument('-l', '--list_of_companies',
                            metavar='list_of_companies',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/constituents/esgbook_companies.csv',
                            help='the path to a csv file containing the list of companies')
    ## add arg: min number of files (to skip a folder)
    arg_parser.add_argument('-min', '--min_files',
                            metavar='min_files',
                            nargs='?',
                            type=int,
                            default=1,
                            help='min number of files present (to skip a folder)')
    ## add arg: list of years
    arg_parser.add_argument('-y', '--years',
                            metavar='years',
                            nargs='+',
                            type=str,
                            default=[], # '2021', '2020', '2019'
                            help='list of years for which to download files')
    ## add arg: filetype
    arg_parser.add_argument('-ftype', '--file_type',
                            metavar='file_type',
                            nargs='?',
                            type=str,
                            default='pdf',
                            help='filetype')
    ## add arg: search keyword
    arg_parser.add_argument('-k', '--keyword',
                            metavar='keyword',
                            nargs='?',
                            type=str,
                            default='sustainability',
                            help='keyword to search for')
    ## add arg: column name to use
    arg_parser.add_argument('-fname', '--file_name',
                            metavar='file_name',
                            nargs='?',
                            type=str,
                            default='url_google-search',
                            help='pattern to search in the url filename')
    ## add arg: column name to use
    arg_parser.add_argument('-n', '--name_column',
                            metavar='name_column',
                            nargs='?',
                            type=str,
                            default='name',
                            help='name column to use for writing output')
    ## add arg: max pagenum
    arg_parser.add_argument('-maxp', '--max_pagenum',
                            metavar='max_pagenum',
                            nargs='?',
                            type=int,
                            default=2,
                            help='max pagenum to search in google')
    ## add arg: max pagenum
    arg_parser.add_argument('-maxc', '--max_constituents',
                            metavar='max_constituents',
                            nargs='?',
                            type=int,
                            default=-1,
                            help='max number of constituets to source data for')
    ## add arg: verbose mode
    arg_parser.add_argument('-v', '--verbose',
                            metavar='verbose',
                            nargs='?',
                            type=int,
                            default=1,
                            help='verbose mode')
    ## add arg: max pagenum
    arg_parser.add_argument('-rev', '--reverse',
                            metavar='reverse',
                            nargs='?',
                            type=int,
                            default=0,
                            help='reverse mode')
    ## parse args
    args, unknown_args = arg_parser.parse_known_args()
    print(args)
    ## set root dir
    root_dir = args.root_dir
    ## constituents file
    const_file = args.list_of_companies
    ## min files
    min_files = args.min_files
    ## list of years
    l_year = args.years
    ## file type
    filetype = args.file_type
    ## file name
    file_name = args.file_name
    ## keyword
    keyword = args.keyword
    ## name column
    name_column = args.name_column
    ## max pagenum
    max_pagenum = args.max_pagenum
    ## max pagenum
    max_constituents = args.max_constituents
    ## verbose mode
    verbose = bool(args.verbose)
    ## reverse mode
    reverse = bool(args.reverse)
    ## constituents data
    df_names = pd.read_csv(const_file)
    if max_constituents > 0:
        l_names_ = df_names[name_column].unique().tolist()[:max_constituents]
        df_names = df_names[df_names[name_column].isin(l_names_)].reset_index(drop=True)
    if verbose:
        print(f'df_names.shape: {df_names.shape}')
        print(f"number of unique names in df_names: {len(df_names[name_column].unique())}")
    ## read url_match.csv files
    df_name_url = read_url_files(root_dir, df_names[name_column].unique().tolist(), file_name,  name_column)
    df_name_url = df_name_url.drop_duplicates().reset_index(drop=True)
    if reverse:
        df_name_url = df_name_url[::-1]
    if verbose:
        print(f'df_name_url.shape: {df_name_url.shape}')
        print(f"number of unique names in df_name_url: {len(df_name_url[name_column].unique())}")
        print(f"number of unique url in df_name_url: {len(df_name_url['url'].unique())}")
    """
    Download info by year and filetype
    """
    sleep_time = 5
    for row_num, row in list(df_name_url.iterrows()):
        if verbose:
            print(f"{row_num}/{df_name_url.shape[0]}: {row.get(name_column)}; {row.get('url')}")
        try:
            ## get url
            url = row.get('url')
            # url = urlparse(url).netloc
            url = make_generic_search_url(url)
            url_name = slugify(url)
            name = row.get(name_column).replace('/', '|')
            name_dir = f"{root_dir}/{name}/{url_name}/www.google.com"
            name_dir_csv = f"{name_dir}/csv"
            name_dir_pdf = f"{name_dir}/pdf"
            os.makedirs(name_dir_csv, exist_ok=True)
            os.makedirs(name_dir_pdf, exist_ok=True)
            l_pdf_files = [file for file in list_full_path(name_dir_pdf, recursive=True)
                           if '.pdf' in os.path.splitext(file)[1]]
            if len(l_pdf_files) >= min_files:
                if verbose:
                    print(f'>{min_files} pdf files already exist: skipping')
                continue
            ## create search url for each year
            l_search_url = []
            if l_year:
                for year in l_year:
                    # search_url = create_google_query(keywords=[year, keyword], site=url, filetype=filetype)
                    search_url_1 = create_google_query(keywords=[year, keyword], site=url.replace('*.', ''),
                                                       filetype=filetype)
                    l_search_url = l_search_url + [search_url_1]  # [search_url] +
            else:
                search_url_1 = create_google_query(keywords=[keyword], site=url.replace('*.', ''),
                                                   filetype=filetype)
                l_search_url = l_search_url + [search_url_1]
            ## check if google is blocking us
            ## parse search pages
            df_results = parse_search_pages(l_search_url, max_pagenum, l_ext=L_EXT_DOWNLOAD)
            # if df_results.shape[0] == 0:
            #     l_search_url = []
            #     # search_url = create_google_query(keywords=[keyword], site=url, filetype=filetype)
            #     search_url_1 = create_google_query(keywords=[keyword], site=url.replace('*.', ''), filetype=filetype)
            #     l_search_url = l_search_url + [search_url_1]  # [search_url] +
            #     df_results = parse_search_pages(l_search_url, max_pagenum, l_ext=L_EXT_DOWNLOAD)
            ## reset index
            df_results = df_results.reset_index(drop=True)
            if verbose:
                print(f'{df_results.shape[0]} search results found')
            if df_results.shape[0] > 0:
                ## create output file
                if l_year:
                    output_file = f"{name_dir_csv}/{row.get(name_column)}_google-result-href_year-" \
                                  f"{l_year[-1]}-{l_year[0]}_filetype-{filetype}.csv"
                else:
                    output_file = f"{name_dir_csv}/{row.get(name_column)}_google-result-href_filetype-{filetype}.csv"
                ## write search results to csv
                df_results.to_csv(output_file, index=False)
                ## loop over each href and download
                l_href = df_results['href'].unique().tolist()
                l_href = [str(href) for href in l_href if str(href) not in ['nan', '']]
                ## download all href from search results
                for href_num, href in enumerate(l_href):
                    if verbose:
                        print(f"downloading {href_num}/{len(l_href)}: {href}")
                    try:
                        # download_file_from_href(href, name_dir_pdf)
                        href_name = slugify(href)
                        resp = requests.get(href)
                        with open(f'{name_dir_pdf}/{href_name}.pdf', 'wb') as f:
                            f.write(resp.content)
                            f.close()
                    except Exception as e:
                        if verbose:
                            print(e)
        except Exception as e:
            if verbose:
                print(f'unable to process {row.get(name_column)}')
                print(e)


# [shutil.rmtree(f"{root_dir}/{sd}") for sd in l_sd if sd not in ['sasb-genie', 'constituents', 'ports', 'roads', 'solar_plants', 'wind_farms', 'airports_v0', 'airports', 'api']]