import platform


if 'Linux' in platform.platform():
    HOME_DIR = '/home/ubuntu'
    platform_user = ''
    ## dir for web scraping output
    DIR_SCRAPERS = f'{HOME_DIR}/data'
    UPDATE_CHROME_PATH = False
elif 'i386-64bit' in platform.platform():
    ROOT_DIR = f'~/data'
    platform_user = 'majid'
    ## dir for web scraping output
    DIR_SCRAPERS = f'/Users/{platform_user}/Dropbox/data/scrapers'
    UPDATE_CHROME_PATH = True

LEN_FILENAME_MAX = 175
L_EXT_DOWNLOAD = ['.pdf', '.doc', '.docx', '.html', '.txt', '.ppt', '.xls', '.xlsx', '.pptx']
L_COMMON_DOMAIN = ['google', 'facebook', 'twitter', 'linkedin', 'yahoo', 'forbes', 'instagram',
                   'wikipedia', 'bloomberg', 'cnn', 'cnbc', 'reuter', 'businesswire',
                   'crunchbase', 'sec.gov']
