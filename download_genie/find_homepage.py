"""
Find entity homepage
Author: Majid Hasan
Description:
    - search an entity name
    - find homepage of the entity
"""


import argparse
import os
import numpy as np
from fuzzywuzzy import fuzz
from init import DIR_SCRAPERS, L_COMMON_DOMAIN
import pandas as pd
from urllib.parse import urlparse
from download_genie.utils.util import launch_browser, create_google_query, get_href_list, take_unique_list, list_full_path
from download_genie.utils.util import get_best_matching_substring


def find_best_matching_url(page_source,
                           name,
                           search_url,
                           q_sim=0.9, q_freq=1):
    """
    Find best matching urls given a company name
    :param page_source:
    :param name:
    :param search_url:
    :param q_sim:
    :param q_freq:
    :return:
    """
    ## get list of urls from search results
    l_url = get_href_list(page_source, search_url)
    if not l_url:
        return ()
    l_url = [url for url in l_url
             if all([domain not in url for domain in L_COMMON_DOMAIN])]
    l_base_url = [urlparse(url).netloc for url in l_url]
    ## find base url of each search result
    l_unique_url = take_unique_list(l_base_url)
    l_count = [l_base_url.count(name) for name in l_unique_url]
    l_match = []
    for url in l_unique_url:
        if len(url) > len(name):
            l_match = l_match + [get_best_matching_substring(name, url)[1]]
        else:
            l_match = l_match + [get_best_matching_substring(url, name)[1]]
    df_match = pd.DataFrame()
    df_match['url'] = l_unique_url
    df_match['match'] = l_match
    df_match['count'] = l_count
    df_match_max = df_match[df_match['match'] >= df_match['match'].quantile(q_sim)].reset_index(drop=True)
    df_match_max.loc[:, 'match_type'] = 'sim'
    df_count_max = df_match[df_match['count'] >= df_match['count'].quantile(q_freq)].reset_index(drop=True)
    df_count_max.loc[:, 'match_type'] = 'count'
    return pd.concat([df_match_max, df_count_max])



def create_output_file(root_dir, ticker, name):
    output_file = ''
    if ticker is not None:
        os.makedirs(f"{root_dir}/{ticker}", exist_ok=True)
        output_file = f"{root_dir}/{ticker}/url_google-search.csv"
    elif name is not None:
        os.makedirs(f"{root_dir}/{name}", exist_ok=True)
        output_file = f"{root_dir}/{name}/url_google-search.csv"
    return output_file


def find_homepage(name: str,
                  name_column: str = 'name',
                  verbose: bool = True) -> pd.DataFrame:
    """
    Find candidates for company's homepage

    :param name: company name
    :param name_column: column name to use for company name in output data
    :param verbose: verbose mode
    :return: dataframe with company name and candidate urls for homepage
    """
    ## proprietary function removed
    df_url = pd.DataFrame()
    return df_url


def write_home_urls(root_dir,
                  constituent_file,
                  name_column='name',
                  overwrite=False,
                  verbose=True) -> None:
    """
    Write company homepage urls in company folders inside root_dir
    :param root_dir: root dir to write company urls in
    :param constituent_file: file containing list of constituents (company names)
    :param name_column: column name to use to read company names (in constituent_file)
    :param overwrite: overwrite existing url files
    :param verbose: verbose mode
    :return:
    """
    ## set company
    df_names = pd.read_csv(constituent_file)
    df_names.dropna(axis=0, how='all', inplace=True)
    df_names.dropna(axis=1, how='all', inplace=True)
    df_names.columns = [str(col).lower() for col in df_names.columns]
    ## get list of names
    l_name_rows = list(df_names.iterrows())
    for row_num, row in l_name_rows:
        if verbose:
            print(f"{row_num}/{len(l_name_rows)}: {row.get(name_column)}")
        url = row.get('url')
        os.makedirs(f"{root_dir}/{row.get(name_column)}", exist_ok=True)
        output_file = f"{root_dir}/{row.get(name_column)}/url_google-search.csv"
        if output_file == '':
            print(f"No output file could be set for {row.get(name_column)}; skipping to the next iteration.")
            continue
        if os.path.isfile(output_file) and (not overwrite):
            print(f"output file already exists; skipping to the next iteration.")
            continue
        web_driver = None
        try:
            df_url = find_homepage(row.get(name_column))
            ## write to csv
            df_url.to_csv(output_file, index=False)
        except Exception as e:
            print(f"unable to process {row.get(name_column)}")
            print(e)
            if web_driver is not None:
                try:
                    web_driver.quit()
                except Exception as e:
                    print(e)



if __name__ == "__main__":
    ## Create the parser
    arg_parser = argparse.ArgumentParser(prog='find_homepage',
                                     usage='%(prog)s [options]',
                                     description='find the homepage of a given entity')
    ## add arg: list of companies
    arg_parser.add_argument('-r', '--root_dir',
                            metavar='root_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/airports_top100',
                            help='the path to a csv file containing the list of companies')
    ## add arg: list of companies
    arg_parser.add_argument('-l', '--list_of_companies',
                            metavar='list_of_companies',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/constituents/airports/airports_top100.csv',
                            help='the path to a csv file containing the list of companies')
    ## add arg: column name to use
    arg_parser.add_argument('-n', '--name_column',
                            metavar='name_column',
                            nargs='?',
                            type=str,
                            default='name',
                            help='name column to use for writing output')
    ## add arg: overwrite flag
    arg_parser.add_argument('-overwrite', '--overwrite',
                            metavar='overwrite',
                            nargs='?',
                            type=int,
                            default=1,
                            help='name column to use for writing output')
    ## add arg: overwrite flag
    arg_parser.add_argument('-v', '--verbose',
                            metavar='verbose',
                            nargs='?',
                            type=int,
                            default=1,
                            help='verbose')
    ## parse args
    args, unknown_args = arg_parser.parse_known_args()
    ## print args
    print(args)
    ## set root dir
    root_dir = args.root_dir
    ## set constituent file
    constituent_file = f"{args.list_of_companies}"
    ## name column
    name_column = args.name_column
    ## name column
    overwrite = bool(args.overwrite)
    ## name column
    verbose = bool(args.verbose)
    """
    Find and homepage
    """
    write_home_urls(root_dir,
                    constituent_file,
                    name_column,
                    overwrite,
                    verbose)
