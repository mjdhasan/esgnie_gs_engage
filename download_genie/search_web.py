"""
Search web
"""
import time
from bs4 import BeautifulSoup
from init import DIR_SCRAPERS, L_EXT_DOWNLOAD
import argparse
import os
import sys
import pandas as pd
from utils.util import launch_browser, safely_launch_browser, create_google_query, take_unique_list, list_full_path
from utils.util import parse_search_pages, download_file_from_href, \
    convert_url_to_name, pdf_download_driver, slugify, validate_url
from utils.util import get_href_text



if __name__ == "__main__":
    ## Create the parser
    arg_parser = argparse.ArgumentParser(prog='search_web',
                                         usage='%(prog)s [options]',
                                         description='search the web for a keyword')
    ## add arg: root dir
    arg_parser.add_argument('-r', '--root_dir',
                            metavar='root_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}/airports',
                            help='root dir')
    ## add arg: min number of files (to skip a folder)
    arg_parser.add_argument('-min', '--min_files',
                            metavar='min_files',
                            nargs='?',
                            type=int,
                            default=20,
                            help='min number of files present (to skip a folder)')
    ## add arg: filetype
    arg_parser.add_argument('-f', '--file_type',
                            metavar='file_type',
                            nargs='?',
                            type=str,
                            default='',
                            help='filetype')
    ## add arg: search keyword
    arg_parser.add_argument('-k', '--keyword',
                            metavar='keyword',
                            nargs='?',
                            type=str,
                            default='airport sustainability',
                            help='keyword to search for')
    ## add arg: max pages to process in search results
    arg_parser.add_argument('-max', '--max_pagenum',
                            metavar='max_pagenum',
                            nargs='?',
                            type=int,
                            default=5,
                            help='max pagenum to search in google')
    ## add arg: max pages to process in search results
    arg_parser.add_argument('-dl', '--download_files',
                            metavar='download_files',
                            nargs='?',
                            type=int,
                            default=0,
                            help='flag to download files')
    ## add arg: max pages to process in search results
    arg_parser.add_argument('-psl', '--pdf_sleep_time',
                            metavar='pdf_sleep_time',
                            nargs='?',
                            type=int,
                            default=120,
                            help='sleep time for downloading pdf files')
    ## add arg: max pages to process in search results
    arg_parser.add_argument('-sl', '--sleep_time',
                            metavar='sleep_time',
                            nargs='?',
                            type=int,
                            default=5,
                            help='general sleep time')
    ## add arg: max pages to process in search results
    arg_parser.add_argument('-ss', '--screen_shot',
                            metavar='screen_shot',
                            nargs='?',
                            type=int,
                            default=1,
                            help='general sleep time')
    ## add arg: max pages to process in search results
    arg_parser.add_argument('-dcp', '--download_contained_pdfs',
                            metavar='download_contained_pdfs',
                            nargs='?',
                            type=int,
                            default=1,
                            help='download pdf files contained in the urls')
    ## add arg: max pages to process in search results
    arg_parser.add_argument('-v', '--verbose',
                            metavar='verbose',
                            nargs='?',
                            type=int,
                            default=1,
                            help='general sleep time')
    ## parse args
    args, unknown_args = arg_parser.parse_known_args()
    print(args)
    ## set root dir
    root_dir = args.root_dir
    os.makedirs(root_dir, exist_ok=True)
    ## min files
    min_files = args.min_files
    ## file type
    filetype = args.file_type
    ## keyword
    keyword = args.keyword
    ## max pagenum
    max_pagenum = args.max_pagenum
    ## max pagenum
    sleep_time = args.sleep_time
    ## max pagenum
    pdf_sleep_time = args.pdf_sleep_time
    ## download_files flag
    download_files = bool(args.download_files)
    ## max pagenum
    download_contained_pdfs = args.download_contained_pdfs
    ## screen shot flat
    screen_shot = bool(args.screen_shot)
    ## screen shot flat
    verbose = bool(args.verbose)
    ## create search url
    search_url = create_google_query(keywords=[keyword], filetype=filetype)
    ## parse url
    url_name = convert_url_to_name(search_url)
    ## make search dir
    search_dir = f"{root_dir}/{url_name}"
    os.makedirs(search_dir, exist_ok=True)
    ## create output file
    output_file = f"{search_dir}/google-result-href.csv"
    ## get all files in the dir
    l_files = list_full_path(search_dir, recursive=True)
    if len(l_files) > min_files: ## if there are already enough files in the dir
        print(f'Number of files in {search_dir} exceed min_files, {min_files}: exiting')
        sys.exit()
    else: ## otherwise
        ## parse search pages
        df_results = parse_search_pages([search_url], max_pagenum)
        # ## add base url
        # df_results['base_url'] = [urlparse(url).netloc for url in df_results['href']]
        # df_base_comp = pd.DataFrame([base.replace('www.', '').split('.') for base in df_results['base_url']])
        # df_base_comp.columns = [f"base-comp-{col}" for col in df_base_comp.columns]
        # df_results = pd.concat([df_results, df_base_comp], axis=1)
        # from collections import Counter
        # Counter(df_base_comp['base-comp-0'])
        # df_base_comp['base-comp-0']
        ## reset index
        df_results = df_results.reset_index(drop=True)
        print(f'{df_results.shape[0]} search results found')
        if df_results.shape[0] > 0:
            ## write search results to csv
            df_results.to_csv(output_file, index=False)
    if download_files:
        # ## read all google-result-href.csv files
        # l_files = [file for file in list_full_path(root_dir, recursive=True)
        #            if 'google-result-href.csv' in file]
        df_results = pd.read_csv(output_file)
        pdf_dir = '/'.join(output_file.split('/')[:-1]) + '/pdf'
        html_dir = '/'.join(output_file.split('/')[:-1]) + '/html'
        png_dir = '/'.join(output_file.split('/')[:-1]) + '/png'
        os.makedirs(pdf_dir, exist_ok=True)
        os.makedirs(html_dir, exist_ok=True)
        os.makedirs(png_dir, exist_ok=True)
        for row_num, row in df_results.iterrows():
            href = row.get('href')
            href_type = row.get('href_type')
            if not validate_url(href):
                continue
            try:
                if any([ext in str(href_type) for ext in L_EXT_DOWNLOAD]):
                    pdf_driver = pdf_download_driver(pdf_dir, headless=True)
                    pdf_driver.get(href)
                    time.sleep(pdf_sleep_time)
                    pdf_driver.quit()
                else:
                    web_driver = safely_launch_browser('Chrome', headless=True)
                    web_driver.get(href)
                    time.sleep(sleep_time)
                    ## save html page
                    page_src = web_driver.page_source
                    # ## check permission issue
                    # if 'Access Denied' in page_src:
                    #     href = href.replace('http', 'https')
                    ## filename
                    filename = slugify(href)
                    html_file = f"{html_dir}/{filename}.html"
                    with open(html_file, 'w') as f:
                        f.write(page_src)
                        f.close()
                    if screen_shot:
                        ## take screenshot
                        S = lambda X: web_driver.execute_script('return document.body.parentNode.scroll' + X)
                        web_driver.set_window_size(S('Width'), S('Height'))
                        ## save screenshot
                        screenshot_status = web_driver. \
                            find_element_by_tag_name('body').screenshot(f"{png_dir}/{filename}.png")
                    web_driver.quit()
                    if download_contained_pdfs:
                        # if verbose:
                        #     print('download pdf files within the search result href')
                        # page_src = web_driver.page_source
                        df_url = get_href_text(page_src, href)
                        l_href_ = df_url['href'].tolist()
                        l_href_ = [href for href in l_href_
                                   if '.pdf' in os.path.splitext(href)[1]]
                        if l_href_:
                            if verbose:
                                print(f'{len(l_href_)} pdf links found on the page')
                            pdf_driver = pdf_download_driver(pdf_dir, headless=True)
                            for href_ in l_href_:
                                try:
                                    pdf_driver.get(href_)
                                    time.sleep(pdf_sleep_time)
                                except Exception as e:
                                    print(e)
                            pdf_driver.quit()
            except Exception as e:
                print(e)










