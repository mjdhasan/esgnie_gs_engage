"""
Classify urls searched from the web
"""

import argparse
import os
from init import DIR_SCRAPERS
import pandas as pd
from utils.util import list_full_path
from urllib.parse import urlparse

if __name__ == "__main__":
    ## Create the parser
    arg_parser = argparse.ArgumentParser(prog='search_web',
                                         usage='%(prog)s [options]',
                                         description='search the web for a keyword')
    ## add arg: root dir
    arg_parser.add_argument('-r', '--root_dir',
                            metavar='root_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}',
                            help='root dir')
    ## add arg: constituents dir
    arg_parser.add_argument('-c', '--con_dir',
                            metavar='con_dir',
                            nargs='?',
                            type=str,
                            default=f'{DIR_SCRAPERS}',
                            help='constituents dir')
    ## add arg: filename for url files
    arg_parser.add_argument('-fname', '--url_file_name',
                            metavar='url_file_name',
                            nargs='?',
                            type=str,
                            default=f'url_google-search',
                            help='filename for url files')
    ## add arg: relevant subdir
    arg_parser.add_argument('-l', '--list_subdir',
                            metavar='list_subdir',
                            nargs='+',
                            type=str,
                            default=[],
                            help='list of subdir in root_dir to read urls from')
    ## add arg: verbose mode
    arg_parser.add_argument('-v', '--verbose',
                            metavar='verbose',
                            nargs='?',
                            type=bool,
                            default=True,
                            help='whether to run the code in verbose mode')
    ## parse args
    args, unknown_args = arg_parser.parse_known_args()
    print(args)
    ## set root dir
    root_dir = args.root_dir
    os.makedirs(root_dir, exist_ok=True)
    ## set constituents dir
    con_dir = args.con_dir
    os.makedirs(con_dir, exist_ok=True)
    ## relevant subdir to read urls from
    list_subdir = args.list_subdir
    ## relevant subdir to read urls from
    url_file_name = args.url_file_name
    ## relevant subdir to read urls from
    verbose = args.verbose
    ## list of subdir in root dir
    l_subdir = os.listdir(root_dir)
    if len(list_subdir) > 1:
        l_subdir = [subd for subd in l_subdir if subd in list_subdir]
    df_url = pd.DataFrame()
    for subd_num, subd in enumerate(l_subdir):
        if verbose:
            print(f"{subd_num}/{len(l_subdir)}: {subd}")
        l_files = [file for file in list_full_path(f"{root_dir}/{subd}/_search_web", recursive=True)
                   if 'google-result-href' in file]
        for file in l_files:
            df_ = pd.read_csv(file)
            df_['subdir'] = subd
            df_url = pd.concat([df_url, df_])
    ## get base url
    df_url['base_url'] = [urlparse(url).netloc for url in df_url['href']]
    ## count number of subdir by base url
    df_url['n-subd_by_base-url'] = df_url.groupby(['base_url'])['subdir'].transform('nunique')
    # ## analyse common base urls
    # df_url[df_url['n-subd_by_base-url'] > 1][['base_url', 'subdir', 'n-subd_by_base-url']].drop_duplicates().values
    # df_url[df_url['n-subd_by_base-url'] == 1][['base_url', 'subdir', 'n-subd_by_base-url']].drop_duplicates().values
    # df_url[df_url['base_url'].str.contains('oecd')][['base_url', 'href', 'subdir']].values
    # df_url[df_url['base_url'].str.contains('sandvik')][['base_url', 'href', 'subdir']].values
    ## ToDo: for all the base_url with n-subd_by_base-url==1, use base_url as constituent and url for downlaoding reports via download_file.py with the base_url
    if verbose:
        print(
            f"{df_url[df_url['n-subd_by_base-url'] == 1].shape[0]}/{df_url.shape[0]} base_urls appear in one subdir only")
    ## filter over n-subd_by_base-ur == 1
    df_url = df_url[df_url['n-subd_by_base-url'] == 1].reset_index(drop=True)
    ## write base_url as constituents into relevant subdir inside con_dir
    for subd in df_url['subdir'].unique().tolist():
        df_ = df_url[df_url['subdir'] == subd].reset_index(drop=True)
        df_['name'] = df_['base_url']
        subd_output_folder = f"{con_dir}/{subd}/urls"
        os.makedirs(subd_output_folder, exist_ok=True)
        df_.to_csv(f"{subd_output_folder}/{subd}_constituents.csv", index=False)
    ## write urls into specific subdir in root_dir
    for row_num, row in df_url.iterrows():
        base_url = row.get('base_url')
        subd = row.get('subdir')
        subd_output_folder = f"{root_dir}/{subd}/urls/{base_url}"
        os.makedirs(subd_output_folder, exist_ok=True)
        df_row_ = pd.DataFrame([row])
        df_row_['name'] = df_row_['base_url']
        df_row_ = df_row_[['name', 'base_url']].drop_duplicates().reset_index(drop=True)
        df_row_.to_csv(f"{subd_output_folder}/{url_file_name}.csv", index=False)
