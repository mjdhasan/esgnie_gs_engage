"""
Download files from annualreports.com
"""


import argparse
import os
from init import DIR_SCRAPERS
import pandas as pd
from urllib.parse import urlparse
from utils.util import launch_browser, safely_launch_browser, pdf_download_driver, take_unique_list, list_full_path
from utils.util import make_generic_search_url, parse_search_pages, download_file_from_href
import re
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
import time

dir_annual_reports = f"{DIR_SCRAPERS}/www.annualreports.com"


if __name__ == "__main__":
    source_url = 'https://www.annualreports.com/Companies?a=#'
    ## sleep time
    sleep_time = 5
    ## launch browser
    web_driver = safely_launch_browser('Chrome', headless=False)
    ## get url
    web_driver.get(source_url)
    ## get ul
    # l_ul = web_driver.find_elements_by_xpath('//ul')
    l_ul = web_driver.find_elements(By.XPATH, '//ul')
    l_ul_len = [len(elem.text) for elem in l_ul]
    ul = l_ul[l_ul_len.index(max(l_ul_len))]
    ## find all links
    l_li = ul.find_elements(By.XPATH, './/li')
    ## for each link, download
    for li_num, li in enumerate(l_li[1:]):
        print(f"{li_num}/{len(l_li)}: {li.text}")
        l_span = li.find_elements(By.XPATH, './/span')
        l_span_text = [elem.text for elem in l_span]
        # [elem.get_attribute('href') for elem in l_span]
        span_a = l_span[0].find_element(By.XPATH, './/a')
        company_href = span_a.get_attribute('href')
        df_ = pd.DataFrame([l_span_text], columns=['company', 'industry', 'sector', 'premium', 'request'])
        df_['company_url'] = company_href
        company = l_span[0].text
        output_dir = f"{DIR_SCRAPERS}/www.annualreports.com/{company}/csv"
        os.makedirs(output_dir, exist_ok=True)
        df_.to_csv(f"{output_dir}/company_url.csv", index=False)
    web_driver.quit()
    ## read all company_url files
    l_files = [file for file in list_full_path(f"{DIR_SCRAPERS}/www.annualreports.com", recursive=True)
               if 'company_url' in file]
    ## loop over each file and get company desc, and download reports
    web_driver = safely_launch_browser('Chrome', headless=True)
    for file_num, file in enumerate(l_files):
        print(f"{file_num}/{len(l_files)}: {file}")
        df_ = pd.read_csv(file)
        url = df_['company_url'].tolist()[0]
        company = df_['company'].tolist()[0]
        web_driver.get(url)
        ## html dir
        html_dir = f"{DIR_SCRAPERS}/www.annualreports.com/{company}/html"
        os.makedirs(html_dir, exist_ok=True)
        output_file = f"{html_dir}/page_source.html"
        with open(output_file, 'w') as f_:
            f_.write(web_driver.page_source)
            f_.close()
        ## df_comp
        df_comp = pd.DataFrame()
        ## get left section
        try:
            left_elem = web_driver.find_element(By.XPATH, "//div[@class='left_list_block']")
            left_text = left_elem.text
        except Exception as e:
            left_text = ''
            print('unable to find left text')
            print(e)
        ## company desc
        try:
            comp_desc = web_driver.find_element(By.XPATH, "//div[@class='company_description']")
            comp_desc = comp_desc.text
        except Exception as e:
            comp_desc = ''
            print('unable to find company desc')
            print(e)
        ## company url
        try:
            website = web_driver.find_element(By.XPATH, "//div[@class='btn_visit_website']")
            homepage = website.find_element(By.XPATH, ".//a").get_attribute('href')
        except Exception as e:
            homepage = ''
            print('unable to find homepage')
            print(e)
        ## find all a
        l_a = web_driver.find_elements(By.XPATH, '//a')
        l_a_href = [a.get_attribute('href') for a in l_a]
        # l_a_href = [href for href in l_a_href
        #             if ('HostedData/' in href) or
        #             ('Click/' in href)]
        # l_a_href = take_unique_list(l_a_href)
        l_a_text = [a.text for a in l_a]
        ## create df
        df = pd.DataFrame()
        df['href'] = l_a_href
        df['href_text'] = l_a_text
        df['left_text'] = left_text
        df['comp_desc'] = comp_desc
        df['comp_homepage'] = homepage
        ## html dir
        csv_dir = f"{DIR_SCRAPERS}/www.annualreports.com/{company}/csv"
        os.makedirs(csv_dir, exist_ok=True)
        output_file = f"{csv_dir}/report_urls.csv"
        df.to_csv(f"{output_file}", index=False)
    if web_driver is not None:
        web_driver.quit()
    ## list all report_urls.csv files
    l_files = [file for file in list_full_path(dir_annual_reports, recursive=True)
               if 'report_urls.csv' in file]
    for file_num, file in enumerate(l_files):
        print(f"{file_num}/{len(l_files)}: {file}")
        df_ = pd.read_csv(file)
        df_['href'] = df_['href'].map(str)
        ## filter relevant urls
        df_ = df_[df_['href'].str.contains('HostedData|Click')].reset_index(drop=True)
        ## company
        company = file.split('/')[-3]
        ## output dir
        output_dir = f"{dir_annual_reports}/{company}/pdf"
        ## download pdf files
        pdf_driver = pdf_download_driver(output_dir, headless=True)
        ## loop over all href
        for row_num, row in df_.iterrows():
            href = row.get('href')
            print(f"{row_num}/{df_.shape[0]}: {href}")
            # download_file_from_href(href, output_dir)
            ## get url
            pdf_driver.get(href)
            ## sleep
            time.sleep(120)
        pdf_driver.quit()




