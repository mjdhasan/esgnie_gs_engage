from init import UPDATE_CHROME_PATH
import os
import pandas as pd
from bs4 import BeautifulSoup
import unicodedata
import re
from urllib.parse import urlparse, urljoin
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
import json
from difflib import SequenceMatcher
import http.client as httplib
import socket
import time
# from selenium.webdriver.remote.command import Command
# Command.


def validate_url(url):
    regex = re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    return bool(re.match(regex, url))


def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')


def convert_url_to_name(url):
    url_parsed = urlparse(url)
    url_path = '-'.join(url_parsed.path.split('/')[1:])
    url_name = f"{url_parsed.netloc}_path={url_path}_params={url_parsed.params}_{url_parsed.query}"
    return url_name


def find_urls_within_elem(elem, sep='|'):
    l_a = elem.find_elements_by_xpath('.//a')
    l_href = [elem.get_attribute('href') if (elem.get_attribute('href') is not None) else '' for elem in l_a]
    return sep.join(l_href)


def parse_search_pages(l_search_url: list,
                       max_pagenum: int,
                       l_ext: list = None,
                       verbose: bool = False,
                       sleep_time: int = 5,
                       block_wait: int = 3600,
                       n_attempts: int = 100):
    ## get resutls for all search urls
    df_results = pd.DataFrame(l_search_url)
    for search_url in l_search_url:
        ## web driver
        web_driver = safely_launch_browser('Chrome', headless=True)
        if web_driver is None:
            continue
        web_driver.get(search_url)
        while '/sorry/' in web_driver.current_url:
            if verbose:
                print(f'google blocking access to {search_url}: pausing browsing')
            n_attempts_ = pause_browser(web_driver, search_url, block_wait=block_wait, n_attempts=n_attempts, verbose=verbose)
            web_driver.get(search_url)
            if n_attempts_ == n_attempts:
                if verbose:
                    print('n_attempts exceeded when waiting for google to unblock access.')
                web_driver.quit()
                return pd.DataFrame()
        l_footer_num = []
        try:
            l_footer_num = get_google_pagenum(web_driver)
            l_footer_num = [num for num in l_footer_num if int(num) < max_pagenum]
        except Exception as e:
            if verbose:
                print(e)
        for start_num in l_footer_num:
            page_url = f"{search_url}&start={start_num * 10}"
            try:
                web_driver.get(page_url)
                ## pause browser if needed
                if '/sorry/' in web_driver.current_url:
                    pause_browser(web_driver,
                                  url=page_url,
                                  block_wait=block_wait,
                                  n_attempts=n_attempts,
                                  verbose=verbose)
                if '/sorry/' in web_driver.current_url:
                    continue
                time.sleep(sleep_time)
                df_ = get_google_search_result_urls(web_driver)
                df_results = pd.concat([df_results, df_])  # df_results.append(df_)
            except Exception as e:
                print(e)
        web_driver.quit()
    ## get href for each search result
    df_results = process_googl_search_results_href(df_results)
    ## filter over useful href
    if 'href' in df_results.columns:
        df_results = df_results[df_results['href'].notnull()].reset_index(drop=True)
        df_results = df_results[df_results['href'] != ''].reset_index(drop=True)
        ## get href type
        df_results['href_type'] = [os.path.splitext(href)[1] for href in df_results['href']]
    if l_ext is not None:
        df_results = df_results[df_results['href_type'].isin(l_ext)].reset_index(drop=True)
    return df_results


def download_file_from_href(href, name_dir_pdf, sleep_time=120):
    pdf_driver = pdf_download_driver(directory=name_dir_pdf)
    pdf_driver.get(href)
    time.sleep(sleep_time)
    pdf_driver.quit()


def process_googl_search_results_href(df_results, verbose=False):
    df_results = df_results.reset_index(drop=True)
    df_results['href'] = ''
    for result_num, result_row in df_results.iterrows():
        try:
            soup = BeautifulSoup(df_results.loc[result_num, 'result_html'], 'html.parser')
            l_href_ = [a.get('href') for a in soup.find_all('a', href=True)]
            if l_href_:
                df_results.loc[result_num, 'href'] = l_href_[0]
        except Exception as e:
            if verbose:
                print(e)
    ## keep only unique href
    if ('href' in df_results.columns) and (df_results.shape[0] > 0):
        df_results = df_results.drop_duplicates(['href'])
        df_results = df_results[df_results['href'].notnull()].reset_index(drop=True)
        df_results = df_results[df_results['href'] != ''].reset_index(drop=True)
        ## deconstruct href
        l_href_parsed = [urlparse(href) for href in df_results['href'].tolist()]
        l_href_path = [href.path for href in l_href_parsed]
        df_path = pd.DataFrame([path.split('/') for path in l_href_path])
        df_path.columns = [f"path-{i}" for i in df_path.columns]
        df_results = pd.concat([df_results, df_path], axis=1)
    return df_results


def pause_browser(web_driver, url, block_wait=3600, n_attempts=100, verbose=False):
    n_attempt = 0
    while ('sorry' in web_driver.current_url) and (n_attempt < n_attempts):
        if verbose:
            print(f'access blocked: sleeping for {block_wait} seconds')
        time.sleep(block_wait)
        web_driver.get(url)
        n_attempt = n_attempt + 1
    return n_attempt


def get_google_pagenum(web_driver):
    l_footer_num = [0]
    # page_src = web_driver.page_source
    # soup = BeautifulSoup(page_src, 'html.parser')
    # soup.find_all('table', attrs={'class': 'AaVjTc'})
    # l_nav = web_driver.find_elements_by_xpath("//div[@role='navigation']")
    l_footer = web_driver.find_elements_by_xpath("//table[@class='AaVjTc']")
    # l_footer = web_driver.find_elements_by_xpath("//body")
    # l_footer = web_driver.find_elements_by_class_name("AaVjTc")
    if l_footer:
        footer = l_footer[0]
        l_footer_td = footer.find_elements_by_xpath(".//td")
        l_footer_text = [foot.text for foot in l_footer_td]
        l_footer_num_ = [re.findall('\d+', text)[0] if re.findall('\d+', text) else '' for text in l_footer_text]
        l_footer_num_ = take_unique_list(l_footer_num_)
        l_footer_num_ = [int(num) for num in l_footer_num_ if num != '']
        # max(l_footer_num)
        # l_footer_href = [foot.get_attribute('innerHTML') for foot in l_footer_td]
        l_footer_num = l_footer_num + l_footer_num_
    return l_footer_num


def make_generic_search_url(url):
    if 'http' not in url:
        url = 'http://' + url
        # url = '*.' + '.'.join(url.split('.')[1:])
        url = urlparse(url).netloc
        url = url.replace('www.', '')
        # url = '*.' + url
    return url


def get_html_tables(web_driver, verbose=True):
    l_table_df = []
    l_href_df = []
    ## find tables in the page
    l_table = web_driver.find_elements_by_xpath('//table')
    for table_num, table in enumerate(l_table):
        if verbose:
            print(f"{table_num}/{len(l_table)}: {table_num}")
        ## get table parent
        table_parent = table.find_elements_by_xpath('..')
        ## get inner html
        table_html = table_parent[0].get_attribute('innerHTML')
        ## get pandas table
        df_table = pd.read_html(str(table_html))[0]
        ## get all cells
        l_td = table_parent[0].find_elements_by_xpath('.//td')
        ## get text to url mapping for each cell
        l_text_href = [(elem.text, find_urls_within_elem(elem)) for elem in l_td]
        df_text_href = pd.DataFrame(l_text_href, columns=['text', 'href'])
        df_text_href = df_text_href[df_text_href['href'] != '']
        df_text_href = df_text_href.drop_duplicates().reset_index(drop=True)
        l_table_df = l_table_df + [df_table]
        l_href_df = l_href_df + [df_text_href]
    return l_table_df, l_href_df


def safely_launch_browser(name, headless, update_path=False, verbose=True):
    web_driver = None
    try:
        web_driver = launch_browser(name, headless)
    except Exception as e:
        if verbose:
            print(e)
    return web_driver


def get_google_search_result_urls(web_driver):
    search_results = web_driver.find_elements_by_xpath("//div[@id='search']")
    df_ = pd.DataFrame()
    if search_results:
        l_results = search_results[0].find_elements_by_xpath("//div[contains(@class, 'g')]")
        l_results_text = [result.text for result in l_results]
        l_results_html = [result.get_attribute('innerHTML') for result in l_results]
        df_ = pd.DataFrame()
        df_['result_html'] = l_results_html
        df_['result_text'] = l_results_text
    return df_


def pdf_download_driver(directory, headless=True):
    options = webdriver.ChromeOptions()
    if headless:
        options.headless = True
    # options.add_argument('--headless')
    options.add_experimental_option(
        'prefs', {
            "Browser.setDownloadBehavior": {'behavior': 'allow'},
            # 'plugins.plugins_disabled': ['Chrome PDF Viewer'],
            "download.default_directory": directory,
            "download.prompt_for_download": False,
            "download.directory_upgrade": True,
            "plugins.always_open_pdf_externally": True
        })
    driver_path = ChromeDriverManager().install()
    driver = webdriver.Chrome(executable_path=driver_path, options=options)
    return driver


def get_status(driver):
    try:
        # driver.execute(Command.STATUS)
        current_url = driver.current_url
        return "Alive"
    except (socket.error, httplib.CannotSendRequest):
        return "Dead"

    
def take_unique_list(seq):
    seen = set()
    seen_add = seen.add
    return [x for x in seq if not (x in seen or seen_add(x))]


def get_best_matching_substring(query, corpus, step=4, flex=3, case_sensitive=False, verbose=False):
    """Return best matching substring of corpus.
    Parameters
    ----------
    query : str
    corpus : str
    step : int
        Step size of first match-value scan through corpus. Can be thought of
        as a sort of "scan resolution". Should not exceed length of query.
    flex : int
        Max. left/right substring position adjustment value. Should not
        exceed length of query / 2.
    Outputs
    -------
    output0 : str
        Best matching substring.
    output1 : float
        Match ratio of best matching substring. 1 is perfect match.
    """
    def _match(a, b):
        """Compact alias for SequenceMatcher."""
        return SequenceMatcher(None, a, b).ratio()
    def scan_corpus(step):
        """Return list of match values from corpus-wide scan."""
        match_values = []
        m = 0
        while m + qlen - step <= len(corpus):
            match_values.append(_match(query, corpus[m : m-1+qlen]))
            if verbose:
                print(query, "-", corpus[m: m + qlen], _match(query, corpus[m: m + qlen]))
            m += step
        return match_values
    def index_max(v):
        """Return index of max value."""
        return max(range(len(v)), key=v.__getitem__)
    def adjust_left_right_positions():
        """Return left/right positions for best string match."""
        # bp_* is synonym for 'Best Position Left/Right' and are adjusted
        # to optimize bmv_*
        p_l, bp_l = [pos] * 2
        p_r, bp_r = [pos + qlen] * 2
        # bmv_* are declared here in case they are untouched in optimization
        bmv_l = match_values[p_l // step]
        bmv_r = match_values[p_l // step]
        for f in range(flex):
            ll = _match(query, corpus[p_l - f: p_r])
            if ll > bmv_l:
                bmv_l = ll
                bp_l = p_l - f
            lr = _match(query, corpus[p_l + f: p_r])
            if lr > bmv_l:
                bmv_l = lr
                bp_l = p_l + f
            rl = _match(query, corpus[p_l: p_r - f])
            if rl > bmv_r:
                bmv_r = rl
                bp_r = p_r - f
            rr = _match(query, corpus[p_l: p_r + f])
            if rr > bmv_r:
                bmv_r = rr
                bp_r = p_r + f
            if verbose:
                print("\n" + str(f))
                print("ll: -- value: %f -- snippet: %s" % (ll, corpus[p_l - f: p_r]))
                print("lr: -- value: %f -- snippet: %s" % (lr, corpus[p_l + f: p_r]))
                print("rl: -- value: %f -- snippet: %s" % (rl, corpus[p_l: p_r - f]))
                print("rr: -- value: %f -- snippet: %s" % (rl, corpus[p_l: p_r + f]))
        return bp_l, bp_r, _match(query, corpus[bp_l : bp_r])
    if not case_sensitive:
        query = query.lower()
        corpus = corpus.lower()
    qlen = len(query)
    if flex >= qlen/2:
        print("Warning: flex exceeds length of query / 2. Setting to default.")
        flex = 3
    match_values = scan_corpus(step)
    pos = index_max(match_values) * step
    pos_left, pos_right, match_value = adjust_left_right_positions()
    return corpus[pos_left: pos_right].strip(), match_value


def create_google_query(keywords, search_domain='https://google.com', quote_keywords=None, site='', filetype=''):
    keywords = [keyword for keyword in keywords if keyword not in ['']]
    if quote_keywords is None:
        quote_keywords = [0] * len(keywords)
    str = f'{search_domain}/search?q='
    if len(keywords) == 0:
        # logging.info('no keywords to search')
        return str
    if site != '':
        str = f"{str}site:{site} AND "
    if filetype != '':
        str = f"{str}filetype:{filetype} AND "
    for i in range(0, len(keywords)):
        if quote_keywords[i]:
            keywords[i] = f"\"{keywords[i]}\""
    str = f"{str}{keywords[0]}"
    for keyword in keywords[1:]:
        str = f"{str} AND {keyword}"

    return str


def process_href(source_url, href, remove_self_ref=False):
    # add base domain to partial href links
    if (len(href) > 0) and (href is not None) and (href != '') and ('http' not in href):  #  or ('https' not in href)
        if re.match('/|../', href):
            href = urljoin(source_url, href)
            # href = f"{source_url}{href}"
    # remove self-links (#xyz) that refer to different sections of the same page/url, and should not count as new pages
    if remove_self_ref and (len(href) > 0):
        last_frag = href.split('/')[-1]
        if re.match('#', last_frag):
            href = '' #  '/'.join(href.split('/')[:-1])
    # get url fragments
    url_fragments = urlparse(href)
    # if there is still no http in the url, make it an empty url
    if ('http' not in url_fragments.scheme) or (url_fragments == ''):
        href = ''
    return href


def get_href_text(page_html, source_url):
    soup = BeautifulSoup(page_html, 'html.parser')
    # find urls
    l_a = soup.find_all('a', href=True)
    # # dir(l_a[0])
    ## get url df
    df_url = pd.DataFrame([(elem.text, elem.attrs['href']) for elem in l_a], columns=['text', 'href'])
    ## drop duplicate href
    df_url = df_url.drop_duplicates(['href'])
    # process href (to complete relative links, etc)
    l_href = [process_href(source_url, href) for href in df_url['href']]
    df_url['href'] = l_href
    # df_url = df_url[df_url['href'].isin(l_href)]
    ## remove empty links
    l_href = [href for href in df_url['href'] if len(href) > 0]
    df_url = df_url[df_url['href'].isin(l_href)]
    ## drop duplicate href
    df_url = df_url.drop_duplicates(['href'])
    ## href to keep
    l_href = [href for href in df_url['href']
              if urlparse(href).netloc not in ['accounts.google.com',
                                               'www.google.com',
                                               'www.google.com.sg',
                                               'support.google.com',
                                               'policies.google.com',
                                               'google.com',
                                               'maps.google.com',
                                               'www.googleadservices.com']]
    df_url = df_url[df_url['href'].isin(l_href)]
    return df_url


def get_href_list(page_html, source_url):
    df_href = get_href_text(page_html, source_url)
    return df_href['href'].tolist()


def list_full_path(d, recursive=False):
    l_full_path = []
    for path in os.listdir(d):
        if recursive and os.path.isdir(os.path.join(d, path)):
            # print(path)
            l_full_path = l_full_path + list_full_path(d=os.path.join(d, path), recursive=recursive)
        else:
            l_full_path = l_full_path + [os.path.join(d, path)]
    return l_full_path


def launch_browser(name='Chrome', headless=True, print_page=False, directory=None, proxy=None,
                   remote_debugging_port=-1, update_path=UPDATE_CHROME_PATH, devport_sol=False):
    driver = None
    # open browser
    if name == 'Chrome':
        # driver = webdriver.Chrome(get_webdriver_path(driver_name='chromedriver'))
        if update_path:
            driver_path = ChromeDriverManager().install()
        else:
            driver_path = '/usr/bin/chromedriver'
        s = Service(driver_path)
        options = webdriver.ChromeOptions()
        # WINDOW_SIZE = "1920,1080"
        # options.add_argument("--window-size=%s" % WINDOW_SIZE)
        # options.add_argument("--no-sandbox")
        if headless:
            # options = webdriver.ChromeOptions()
            options.headless = True
        if proxy is not None:
            options.add_argument(f'--proxy-server={proxy}')
        if devport_sol:
            options.add_argument("--disable-dev-shm-usage")
            options.add_argument("--no-sandbox")
        if remote_debugging_port > 0:
            options.add_argument(f'--remote-debugging-port={remote_debugging_port}')
        if print_page:
            appState = {
                "recentDestinations": [
                    {
                        "id": "Save as PDF",
                        "origin": "local",
                        "account": ""
                    }
                ],
                "selectedDestinationId": "Save as PDF",
                "version": 2
            }
            profile = {
                "download.default_directory": directory,
                "download.prompt_for_download": False,
                "download.directory_upgrade": True,
                "plugins.always_open_pdf_externally": True,
                'printing.print_preview_sticky_settings.appState': json.dumps(appState),
                'savefile.default_directory': directory
            }
            options = webdriver.ChromeOptions()
            options.headless = True
            options.add_experimental_option('prefs', profile)
            options.add_argument('--kiosk-printing')
        ## set options in the driver
        # driver = webdriver.Chrome(executable_path=driver_path, options=options)
        driver = webdriver.Chrome(service=s, options=options)
    elif name == 'Firefox':
        # driver_path = get_webdriver_path(driver_name='geckodriver')
        driver = webdriver.Firefox(executable_path='/usr/local/bin/geckodriver')
    elif name == 'Safari':
        driver = webdriver.Safari(executable_path='/usr/bin/safaridriver')

    return driver
