####------------------------------------
#### Name: Sustainability Report Crawler
#### Author: Majid Hasan
####------------------------------------

import logging
import re
from init import DIR_SCRAPERS, LEN_FILENAME_MAX
import numpy as np
import os
import time
from urllib.parse import urlparse, urljoin
import pickle
import pandas as pd
from bs4 import BeautifulSoup
# import validators
from fuzzywuzzy import fuzz
from selenium.webdriver.common.keys import Keys
from utils.util import launch_browser, list_full_path, take_unique_list
from utils.util import read_sqlite_table
from init import DB_CDP_ACCOUNTS
from collections import Counter
from util import find_children_attrs, find_xpath_and_send_keys, get_html_tables

L_YEARS = [str(year) for year in np.arange(2010, 2020, 1)]
CRAWL_DEPTH = 1


def get_cdp_question_number(text):
    ## ToDo: change it to match the start of sentence
    # match = re.match('\([A-Z]\d+?\.\d+?.*?\)', text)
    match = re.search('^\([A-Z].*?\)(\s)([A-Z])', text)
    if match:
        return text[match.span()[0]: match.span()[1] - 2]
    else:
        return ''


def login_to_cdp(web_driver, email, pword, login_url='https://www.cdp.net/en/users/sign_in'):
    web_driver.get(login_url)
    l_elem = web_driver.find_elements_by_xpath('//*')
    ## find sigin in button
    l_signin = [elem for elem in l_elem
                if (fuzz.partial_ratio(elem.text, 'sign in') > 70) and
                (elem.tag_name == 'a')]
    # [elem.tag_name for elem in l_signin]
    for button in l_signin:
        try:
            button.click()
            break
        except Exception as e:
            print('unable to click signin button')
            print(e)
    ## find login and password inputs
    l_input = web_driver.find_elements_by_xpath('//input')
    l_input_email = [elem for elem in l_input if elem.get_attribute('type') == 'email']
    l_input_password = [elem for elem in l_input if elem.get_attribute('type') == 'password']
    if l_input_email and l_input_password:
        # l_input_email[0].send_keys('majid@esgnie.com')  ## mjdhasan@icloud.com
        # l_input_password[0].send_keys('T#2Nr6GD2s&-cEs') ## vufbyr-0Qyfji-cisjen
        l_input_email[0].send_keys(email)
        l_input_password[0].send_keys(pword)
    ## find login button
    l_button = web_driver.find_elements_by_xpath('//button')
    l_login_btn = [elem for elem in l_button if fuzz.partial_ratio(elem.text, 'login') > 70]
    if l_login_btn:
        l_login_btn[0].click()


def search_cdp(web_driver,
               search_term=None,
               search_url='https://www.cdp.net/en/search',
               program='Forest',
               sleep_time=10):
    web_driver.get(search_url)
    ## find program field
    l_program = web_driver.find_elements_by_xpath("//div[text()='Program']")
    if l_program:
        l_program[0].click()
        time.sleep(sleep_time)
        ## find climate change program
        l_ddown = web_driver.find_elements_by_xpath(f"//li[text()='{program}']")
        if l_ddown:
            l_ddown[0].click()
    ## find status field
    l_program = web_driver.find_elements_by_xpath("//div[text()='Status']")
    if l_program:
        l_program[0].click()
        time.sleep(sleep_time)
        ## find climate change program
        l_ddown = web_driver.find_elements_by_xpath("//li[text()='Submitted']")
        if l_ddown:
            l_ddown[0].click()
    ## find all input fields
    l_input = web_driver.find_elements_by_xpath('//input')
    # ## input text
    # l_input_text = [elem.get_attribute('placeholder') for elem in l_input]
    ## find search input fields
    l_search = [elem for elem in l_input if fuzz.partial_ratio('search', elem.get_attribute('placeholder')) > 70]
    ## get the first search element
    search_box = l_search[0]
    ## send text str to search
    if search_term is not None:
        search_box.send_keys(search_term)
    ## hit enter
    search_box.send_keys(Keys.RETURN)
    return web_driver


def diff_html_pages(l_elem, l_elem_new_text):
    ## find all elem in the new page
    l_elem = web_driver.find_elements_by_xpath('//*')
    ## find all elem in the new page
    l_elem_new_text = [elem.text for elem in web_driver.find_elements_by_xpath('//*')]
    ## find elements in page_source_new that are not in page_source
    l_diff = [text for text in l_elem_new_text if text not in l_elem_text]
    ## find the element types where the difference in text lies
    l_diff_elem = [elem for elem in l_elem if elem.text in l_diff]
    return l_diff_elem


def count_matching_tags(web_driver, search_term):
    l_elem = web_driver.find_elements_by_xpath('//*')
    ## find all elem in the new page
    l_elem_new_text = [elem.text for elem in l_elem]
    ## find elements matching the search term
    l_match = [fuzz.partial_ratio(search_term, elem) for elem in l_elem_new_text]
    l_max_match_idx = np.where(np.array(l_match) == max(l_match))[0]
    l_match_elem = [l_elem[i] for i in l_max_match_idx]
    # l_match_a = [l_elem[i] for i in l_max_match_idx if l_elem[i].tag_name == 'a']
    # l_match_text = [elem.text for elem in l_match_a]
    # l_match_href = [elem.get_attribute('href') for elem in l_match_a]
    ## check the type of elements matching search term
    l_match_tag = [elem.tag_name for elem in l_match_elem]
    tag_count = Counter(l_match_tag)
    return tag_count


# def get_html_tables(web_driver):
#     l_table_df = []
#     l_href_df = []
#     ## find tables in the page
#     l_table = web_driver.find_elements_by_xpath('//table')
#     for table_num, table in enumerate(l_table):
#         ## get table parent
#         table_parent = table.find_elements_by_xpath('..')
#         ## get inner html
#         table_html = table_parent[0].get_attribute('innerHTML')
#         ## get pandas table
#         df_table = pd.read_html(str(table_html))[0]
#         ## get all cells
#         l_td = table_parent[0].find_elements_by_xpath('.//td')
#         ## get text to url mapping for each cell
#         l_text_href = [(elem.text, find_urls_within_elem(elem)) for elem in l_td]
#         df_text_href = pd.DataFrame(l_text_href, columns=['text', 'href'])
#         df_text_href = df_text_href[df_text_href['href'] != '']
#         df_text_href = df_text_href.drop_duplicates().reset_index(drop=True)
#         l_table_df = l_table_df + [df_table]
#         l_href_df = l_href_df + [df_text_href]
#     return l_table_df, l_href_df


def parse_cdp_answer_row(row):
    ## question text
    question_text = get_html_text(row.get('question_header'))
    ## subquestion text
    subquestion_text = get_html_text(row.get('sub-question_html'))
    ## child html
    child_text = get_html_text(row.get('child_html'))
    ## get child tables
    if BeautifulSoup(row.get('child_html'), 'html.parser').find_all('tr'):
        df_answer = pd.read_html('<table>' + row.get('child_html') + '</table>')[0]
        df_answer['question_number'] = row.get('question_number')
        df_answer['question_text'] = question_text
        df_answer['sub-question_text'] = subquestion_text
        df_answer['child_text'] = child_text
    else:
        df_answer = pd.DataFrame({
            'question_number': [row.get('question_number')],
            'question_text': [question_text],
            'sub-question_text': [subquestion_text],
            'child_text': [child_text]
        })
    return df_answer


def get_html_text(html):
    text = ''
    if html is not None:
        if str(html) != 'nan':
            ## text
            text = BeautifulSoup(str(html), 'html.parser').text
    return text


def signup_cdp(web_driver,
               email='asdkfajiadg@grr.la',
               name=None,
               password='avyooxfr_%1dkd4',
               url='https://data.cdp.net/signup',
               sleep_time=5):
    if (name is None) or (str(name) == 'nan'):
        if email is not None:
            name = email.split('@')[0]
    if password is None:
        if email is not None:
            password = email.split('@')[0]
    web_driver.get(url)
    ## find email
    l_email = web_driver.find_elements_by_xpath("//input[@id='signup-email']")
    if l_email:
        l_email[0].send_keys(f'{email}')
    time.sleep(sleep_time)
    ## find screen name
    find_xpath_and_send_keys(web_driver, path="//input[@id='signup-screenName']", keys_to_send=name)
    time.sleep(sleep_time)
    ## find password
    find_xpath_and_send_keys(web_driver, path="//input[@id='signup-password']", keys_to_send=password)
    time.sleep(sleep_time)
    ## find password
    find_xpath_and_send_keys(web_driver, path="//input[@id='signup-passwordConfirm']", keys_to_send=password)
    time.sleep(sleep_time)
    ## find captcha
    # l_captcha = web_driver.find_elements_by_xpath("//textarea[@id='g-recaptcha-response']")
    l_captcha = web_driver.find_elements_by_xpath("//iframe[@title='reCAPTCHA']")
    if l_captcha:
        l_captcha[0].click()
    ## find create my account button
    l_button = web_driver.find_elements_by_xpath("//button[@type='submit']")
    if l_button:
        l_button[0].click()
    time.sleep(sleep_time)
    ## next page url
    next_page = web_driver.current_url
    return next_page


def read_cdp_emails(dir_emails):
    l_files = [file for file in list_full_path(dir_emails) if os.path.splitext(file)[1] == '.csv']
    df_emails = pd.DataFrame()
    for file in l_files:
        df = pd.read_csv(file)
        df_emails = df_emails.append(df)
    return df_emails


def change_proxy(proxy='23.23.23.23:3128'):
    ## pip install http-request-randomizer
    from http_request_randomizer.requests.proxy.requestProxy import RequestProxy
    from selenium import webdriver
    from webdriver_manager.chrome import ChromeDriverManager
    req_proxy = RequestProxy()
    proxies = req_proxy.get_proxy_list()
    PROXY = proxies[0].get_address()
    PROXY = '157.246.76.4:8080'
    webdriver.DesiredCapabilities.CHROME['proxy'] = {
        "httpProxy": PROXY,
        "ftpProxy": PROXY,
        "sslProxy": PROXY,
        "proxyType": "MANUAL",
    }
    driver_path = ChromeDriverManager().install()
    driver = webdriver.Chrome(executable_path=driver_path)
    driver.get("http://whatismyipaddress.com")
    driver.quit()
    web_driver = launch_browser('Chrome', headless=False, proxy='103.142.140.22:80')
    web_driver = launch_browser('Chrome', headless=False)
    web_driver.get("http://whatismyipaddress.com")
    web_driver.get('https://www.google.com')
    web_driver.quit()


def open_cdp_homepage(web_driver, source_url='https://www.cdp.net', sleep_time=3):
    web_driver.get(source_url)
    time.sleep(sleep_time)
    ## accept all cookies
    l_cookies = web_driver.find_elements_by_xpath("//button[@id='onetrust-accept-btn-handler']")
    if l_cookies:
        l_cookies[0].click()
    time.sleep(sleep_time)
    ## stay on global
    l_stay_global = web_driver.find_elements_by_xpath("//a[text()='Stay on global']")
    if l_stay_global:
        l_stay_global[0].click()
    web_driver.get(source_url)


def get_cdp_response_urls(web_driver, program, search_term=None):
    ## run search
    web_driver = search_cdp(web_driver, program=program, search_term=search_term)
    l_table_df = l_href_df = []
    page_url = web_driver.current_url
    pagenum_max = 7200
    l_navigation = web_driver.find_elements_by_xpath("//div[@role='navigation']")
    if l_navigation:
        l_navigation_a = l_navigation[0].find_elements_by_xpath(".//a")
        l_pagenum = [int(elem.text) for elem in l_navigation_a if elem.text != '>']
        pagenum_max = max(l_pagenum) + 1
    for i in np.arange(1, pagenum_max, 1):
        print(f"{i}: {page_url}")
        web_driver.get(page_url)
        time.sleep(3)
        l_table_df_, l_href_df_ = get_html_tables(web_driver)
        l_table_df = l_table_df + l_table_df_
        l_href_df = l_href_df + l_href_df_
        if 'page=' in page_url:
            page_url = page_url.replace(f'page={i}', f'page={i + 1}')
        else:
            page_url = page_url + f"&page={i + 1}"
    # [(table_num, dfs[0], dfs[1]) for table_num, dfs in enumerate(zip(l_table_df, l_href_df))]
    df_href = pd.DataFrame()
    for table_num, dfs in enumerate(zip(l_table_df, l_href_df)):
        print(f"{table_num}/{len(l_table_df)}")
        df_table = dfs[0]
        df_text_href = dfs[1]
        # ## write df_table
        # df_table.to_csv(f"{DIR_WEB}/{ticker}/df_table_{table_num}.csv", index=False)
        # df_text_href.to_csv(f"{DIR_WEB}/{ticker}/df_table_{table_num}_text-href.csv", index=False)
        ## ToDo: add code to automate the detection of Response-like columns
        ## get href type
        df_text_href['href_type'] = ['response' if 'cdp2/' in href else 'company'
                                     for href in df_text_href['href']]
        df_text_href['Name'] = np.nan
        df_text_href.loc[df_text_href['href_type'] == 'company', 'Name'] = \
            df_text_href.loc[df_text_href['href_type'] == 'company', 'text']
        ## ffill name
        df_text_href['Name'] = df_text_href['Name'].ffill()
        ## append company name to href
        df_href_ = pd.merge(left=df_text_href, right=df_table,
                            on=['Name'], how='left')
        # df_href_ = df_text_href[df_text_href['text'].isin(l_resp)]
        ## append to df_href
        df_href = df_href.append(df_href_)
        # [df_text_href[df_text_href['text'] == text]['href'].tolist()[0] for text in ]
    return df_href


def get_children(elem, child_type='section', class_name='ndp_formatted_response__question', recursive=False):
    return elem.findChildren(child_type,
                             attrs={'class': class_name},
                             recursive=recursive)


def get_terminal_children(section,
                          child_type='section',
                          class_name='ndp_formatted_response__question',
                          recursive=False):
    l_terminal = []
    l_terminal_header = []
    l_remaining = [section]
    while l_remaining:
        section = l_remaining[0]
        ## remove this section from list of remaining sections to iterate over
        l_remaining.remove(section)
        if get_children(section, child_type, class_name, recursive):
            l_add_ = get_children(section)
            l_remaining = l_remaining + l_add_
        else:
            parent_ = section.findParent()
            first_child = parent_.findChildren(recursive=True)[0]
            l_terminal = l_terminal + [section]
            l_terminal_header = l_terminal_header + [first_child]
    df_terminal = pd.DataFrame()
    df_terminal['child'] = l_terminal
    df_terminal['header'] = l_terminal_header
    return df_terminal


def get_response_qa(child):
    l_child_header = child.findChildren(attrs={'class': 'ndp_formatted_response__header'},
                                        recursive=False)
    l_child_answer = child.findChildren(attrs={'class': 'ndp_formatted_response__answer'},
                                        recursive=False)
    l_child_table = child.findChildren(attrs={'class': 'ndp_formatted_response__table'},
                                       recursive=False)
    df_ = pd.DataFrame()
    df_['child_header_html'] = [str(child) for child in l_child_header]
    if l_child_answer:
        df_['child_answer_html'] = [str(child) for child in l_child_answer]
    if l_child_table:
        df_['child_table_html'] = [str(child) for child in l_child_table]
    df_['question_html'] = str(question_html)
    df_['question_text'] = question_html.text
    df_['parent_header'] = str(child_row.get('header'))
    df_['section_heading_html'] = str(section_heading)
    df_['section_heading_text'] = section_heading.text
    return df_


def check_file_exists(dir_output, filename):
    exist_flag = 0
    if os.path.isfile(f"{dir_output}/csv/{filename}.csv") and \
            (os.path.getsize(f"{dir_output}/csv/{filename}.csv") > 1000):
        exist_flag = 1
    return exist_flag


if __name__ == "__main__":
    # dir_cdp_emails = '/Users/majid/Dropbox/data/scrapers/email/guerrillamail.com'
    # ## create cdp accounts
    # df_emails = read_cdp_emails(dir_cdp_emails)
    # ## create cdp account for each email
    # sleep_time = 10 * 60
    # l_rows = list(df_emails.iterrows())
    # for row_num, row in reversed(l_rows):
    #     print(f"{row_num}/{len(l_rows)}: {row.get('email')}")
    #     web_driver = launch_browser(name='Chrome', headless=False)
    #     # web_driver.get("http://whatismyipaddress.com")
    #     try:
    #         ## sign up for cdp
    #         signup_cdp(web_driver=web_driver, email=row.get('email'), name=row.get('name'), password=row.get('password'))
    #     except Exception as e:
    #         print(f'unable to create an account')
    #         print(e)
    #     time.sleep(sleep_time)
    #     web_driver.quit()

    ##-----------------------
    ## get all cdp responses
    ##-----------------------
    ## cdp accounts data
    df_emails = read_sqlite_table(db=DB_CDP_ACCOUNTS, table_name='cdp_accounts',
                                  where_={'account_status': 'activated'})
    ## set cdp source url
    source_url = 'https://www.cdp.net/en/search'
    ## set base url
    base_url = urlparse(source_url).netloc
    ## set output dir
    dir_output = f"{DIR_SCRAPERS}/{base_url}"
    ## launch browser
    web_driver = launch_browser(name='Chrome', headless=False)
    ## open cdp search page
    open_cdp_homepage(web_driver)
    ## login to cdp
    email = df_emails['email'].tolist()[-1]
    pwrod = df_emails['password'].tolist()[-1]
    login_to_cdp(web_driver, email=email, pword=pwrod)
    ## set cdp program
    program = 'Forest'
    ## get all cdp responses
    df_href = get_cdp_response_urls(web_driver, program=program, search_term=None)
    ## write df_href
    df_href.to_csv(f"{dir_output}/_responses/csv/df_href_{program}.csv", index=False)
    ## quit web_driver
    web_driver.quit()

    ##-----------------------------------
    ## read responses from the saved urls
    ##-----------------------------------
    ## general sleep time
    sleep_time = 5
    ## verbose flag
    verbose = True
    ## set cdp program
    program = 'Forest'
    ## set cdp source url
    source_url = 'https://www.cdp.net/en/search'
    ## cdp sign out url
    logout_url = 'https://www.cdp.net/en/users/sign_out'
    ## set base url
    base_url = urlparse(source_url).netloc
    ## set input dir
    dir_input = f"{DIR_SCRAPERS}/{base_url}"
    ## overwrite flag
    overwrite_page_flag = False
    overwrite_response_flag = True
    ## get href file
    href_file = [file for file in list_full_path(f"{dir_input}/_responses/csv/")
                 if (program in file) and not (re.search('v\d+', file))
                 ][0]
    ## read href data
    df_href = pd.read_csv(href_file)
    df_href = df_href[df_href['href_type'] == 'response']
    df_href = df_href.sort_values(['Name', 'Year'], ascending=False).reset_index(drop=True)
    ## cdp accounts data
    df_emails = read_sqlite_table(db=DB_CDP_ACCOUNTS,
                                  table_name='cdp_accounts',
                                  where_={'account_status': 'activated'})  ##
    # df_emails = df_emails.sort_values('timestamp', ascending=False).reset_index(drop=True)
    ## launch browser
    web_driver = launch_browser(name='Chrome', headless=True)
    ## open cdp search page
    open_cdp_homepage(web_driver)
    ## set cdp account number to iterate over
    n_acc = 0
    ## login to cdp
    login_to_cdp(web_driver,
                 email=df_emails.loc[n_acc, 'email'],
                 pword=df_emails.loc[n_acc, 'password'])
    # web_driver.current_url
    # href = [href for href in df_href['href'] if 'X5' in href]
    # href = href[0]
    ## loop over each href and get the tables
    row_num_start = 1790
    for row_num, row in list(df_href.iterrows())[row_num_start:]:  ## [21:30]
        href = row.get('href')
        name = row.get('Name')
        name = name.replace('/', '|')  ## name.split('/')[0]
        href_name = row.get('text')
        filename = href_name[:LEN_FILENAME_MAX]
        print(f"{row_num} (acc_num: {n_acc}): {name}; {href}")
        ## set output dir
        dir_output = f"{dir_input}/{name}"
        ## make dir
        os.makedirs(f"{dir_output}/csv", exist_ok=True)
        os.makedirs(f"{dir_output}/html", exist_ok=True)
        ## check if output file exists
        if (check_file_exists(dir_output, filename) > 0) and \
                (not overwrite_response_flag):
            print(f"skipping {filename}")
            continue
        if (check_file_exists(dir_output, f"{filename}_pages") > 0) and \
                (not overwrite_page_flag):
            print(f"skipping {filename}_pages")
            continue
        ## get page source
        web_driver.get(href)
        ## check if the href is accessible
        n_try = 0
        while ('formatted_responses/' not in web_driver.current_url) and \
                (n_try < 3):
            print('unable to access the webpage: moving to a new account')
            ## sign out
            find_xpath_and_send_keys(web_driver, path=f"//a[@href='{logout_url}']", click=True)
            ## update account number to use
            n_acc = n_acc + 1
            if n_acc < df_emails.shape[0]:
                ## login to a different account
                login_to_cdp(web_driver,
                             email=df_emails.loc[n_acc, 'email'],
                             pword=df_emails.loc[n_acc, 'password'])
            else:
                ## re-load emails
                df_emails = read_sqlite_table(db=DB_CDP_ACCOUNTS,
                                              table_name='cdp_accounts',
                                              where_={'account_status': 'activated'})
                ## set account number back to zero
                n_acc = 0
                ## login to the account
                login_to_cdp(web_driver,
                             email=df_emails.loc[n_acc, 'email'],
                             pword=df_emails.loc[n_acc, 'password'])
            ## get page source
            web_driver.get(href)
            ## sleep for a bit
            time.sleep(sleep_time)
            ## update number of tries
            n_try = n_try + 1
        if 'formatted_responses/' not in web_driver.current_url:
            print('unable to access the href, even after trying 5 accounts; breaking the for loop')
            continue
        try:
            page_type = ''
            if 'formatted_responses/page' in web_driver.current_url:
                page_type = 'page'
            elif 'formatted_responses/response' in web_driver.current_url:
                page_type = 'response'
            ## write page_source
            os.makedirs(f"{dir_output}/html", exist_ok=True)
            with open(f"{dir_output}/html/{filename}.html", "w") as file:
                file.write(web_driver.page_source)
                file.close()
            time.sleep(sleep_time)
            if page_type == 'response':
                soup = BeautifulSoup(web_driver.page_source, 'html.parser')
                body = soup.find('body')
                ## get all secitons
                l_sections = body.findChildren('section',
                                               attrs={'class': 'ndp_formatted_response__section'},
                                               recursive=True)
                # l_sections = web_driver.find_elements_by_xpath("//section[@class='ndp_formatted_response__section']")
                # section = [section for section in l_sections if '(F3.1)' in section.text]
                # section = section[-1]
                df_sections = pd.DataFrame()
                for section_num, section in enumerate(l_sections):
                    l_section_ = section.findChildren('section',
                                                      attrs={'class': 'ndp_formatted_response__section'},
                                                      recursive=True)
                    if l_section_:
                        continue
                    ## get section heading
                    section_heading = section.find('h2', attrs={'class': 'ndp_formatted_response__header'})
                    question_html = section.find('h3', attrs={'class': 'ndp_formatted_response__header'})
                    ## get terminal qa sections
                    df_child = get_terminal_children(section,
                                                     child_type='section',
                                                     class_name='ndp_formatted_response__question',
                                                     recursive=False)
                    df_child = df_child.drop_duplicates().reset_index(drop=True)
                    # l_q = df_child['child'].tolist()
                    for child_num, child_row in list(df_child.iterrows()):
                        child = child_row.get('child')
                        df_ = get_response_qa(child)
                        df_sections = df_sections.append(df_)
                    ## drop duplicates
                    df_sections = df_sections.drop_duplicates().reset_index(drop=True)
                ## set output file
                output_file = f"{dir_output}/csv/{filename}.csv"
                if verbose:
                    print(f"writing {output_file}")
                ## write df_sections
                os.makedirs(f"{dir_output}/csv", exist_ok=True)
                df_sections.to_csv(output_file, index=False)
            elif page_type == 'page':
                ## get all pages
                l_sections = web_driver.find_elements_by_xpath("//div[@class='cdp-page']")
                df_qa = pd.DataFrame()
                for section_num, section in enumerate(l_sections):
                    l_q = section.find_elements_by_xpath(".//div[@class='cdp-question']")
                    for q in l_q:
                        try:
                            ## question num
                            l_head_num = q.find_elements_by_xpath(".//span[@class='cdp-question-head-num']")
                            q_num = '|'.join([head.text for head in l_head_num])
                            ## question head text
                            l_head_text = q.find_elements_by_xpath(".//span[@class='cdp-question-head-text']")
                            q_head_text = '|'.join([head.text for head in l_head_text])
                            ## question body
                            l_q_body = q.find_elements_by_xpath(".//div[@class='cdp-question-body']")
                            l_q_body_html = [body.get_attribute('innerHTML') for body in l_q_body]
                            l_q_body_html = [body for body in l_q_body_html if body != '']
                            if len(l_q_body_html) > 1:
                                len_q_body_html = [len(body) for body in l_q_body_html]
                                q_body_html = l_q_body_html[len_q_body_html.index(max(len_q_body_html))]
                            else:
                                q_body_html = l_q_body_html[0]
                            df_ = pd.DataFrame()
                            df_['q_body_html'] = [q_body_html]
                            df_['q_html'] = [q.get_attribute('innerHTML')]
                            # q.text.split('\n')
                            # df_['q_text'] = [q.text]
                            df_['q_num'] = [q_num]
                            df_['q_head_text'] = [q_head_text]
                            df_['section_html'] = [section_num]
                            df_['section_html'] = [section.get_attribute('innerHTML')]
                            df_qa = df_qa.append(df_)
                        except Exception as e:
                            print(e)
                ## write qa
                if df_qa.shape[0] > 0:
                    df_qa['ticker'] = name
                    df_qa['response'] = filename
                    df_qa.to_csv(f"{dir_output}/csv/{filename}_pages.csv", index=False)
        except Exception as e:
            print(e)
    ## quit driver
    web_driver.quit()
