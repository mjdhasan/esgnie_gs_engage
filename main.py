"""
----------
ESGnie API
----------
This is an API for unstructured data sourcing and processing, that can take source unstructured documents from the web,
extract and structure relevant data, and store the structured data in Athena tables that can be connected to BI tools.

It is designed to perform the following key tasks:
1) Sourcing corporate disclosures, such as sustainability and annual reports
2) Processing unstructured pdf documents
3) Standardizing quantitative variable names and units
4) Text categorisation and analysis using QA models
5) Creating AWS Athena databases to store and query data for BI
6) An API to expose these modules to the end-user through various endpoints

More details on each module
1) Document Sourcingis handled in 'download_genie' module
    - download_genie has a few key submodules including
        + find_homepage.py: identifies the official url of a company, given a company name
        + download_files.py: searches the company's official url for disclosures related to a given keyword
2) Document processing is handled by 'pdf_genie' and 'img_genie' modules
    - pdf_genie has a few key submodules including
        + text extraction from pdf files, along with the text coordinates (x0, y0, x1, y1)
        + identification of quantitative values
        + structuring quantitative values into a standardized form (variable, unit, date, value, pagenum)
    - img_genie performs image segmentation analysis, and contains following key modules
        + text segmentation based on the layout analysis of the document to separate passages, tables, etc
3) Variable unit standardization is done by 'std_genie' module
    - std_genie module has a few key submodules inlcluding
        +  predict_var_unit_std, which predicts the standardized variable name and unit for a given input string.
4) Text categorisation & analysis is handled by 'qa_genie' module
    - qa module has a few key submodules including
        + extractive_qa, which runs extractive question answering
        + implied_qa, which checks whether a passage implies something or not
        + mixed_qa, which can run a variety of QA tasks, such as extractive QA, multiple choice, etc.
5) Cloud data storage is handled by 'aws_genie' module
    - aws_genie module has a few key submodules, including
        + db_upload, which uploads data from a local dir to s3 in parquet format, and creates athena tables out of it
        + aws_genie includes utility functions to recognise column type, even if all values are saved as string
        + aws_genie includes utility functions to standardise column names across data files based on column values
6) API code is handled by main.py, which creates all the end points
"""


import re
from init import DIR_OUTPUT, DIR_CODE, DIR_VENV, AUTHORISED_USERS_FILE
import subprocess
import urllib
import json
import psutil
import requests
import os
import pickle
import numpy as np
import multiprocessing as mp
import uuid
from typing import Optional, List
from fastapi import Depends, FastAPI, HTTPException, status, Query
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from pydantic import BaseModel
import pandas as pd
from utils.common import list_full_path
from pdf_genie.extract_pdf_quants import extract_pdf_quants
from img_genie.segment_words_files import segment_word_files
from download_genie.find_homepage import find_homepage
from download_genie.download_files import find_page_urls
from qa_genie.mixed_qa import mixed_qa
from aws_genie.s3.s3_utils import read_s3_file_contents, get_matching_s3_keys, read_data


def get_module_for_task(query_dict: dict) -> str:
    """
    Identify the right module to run based on input task
    :param query_dict: dictionary derived from input query
    :return: module name
    """
    module = 'NA'
    if 'source' in query_dict['task']:
        module = 'download_genie'
        if 'web search' in query_dict['source']:
            module = 'search_web.py'
    if 'process' in query_dict['task']:
        if 'pdf' in query_dict['filetype']:
            module = 'pdf_genie'
        elif 'png' in query_dict['filetype']:
            module = 'img_genie'
    if 'qa' in query_dict['task']:
        module = 'qa_genie'
    return module


def hash_password(password: str):
    ## proprietary function to hash password removed
    pass


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


class User(BaseModel):
    username: str
    email: Optional[str] = None
    full_name: Optional[str] = None
    disabled: Optional[bool] = None


class UserInDB(User):
    hashed_password: str
    token: str


def get_user(db, username: str):
    if username in db:
        user_dict = db[username]
        return UserInDB(**user_dict)



def get_user_from_token(token):
    user = df_users[df_users['token'] == token]['username'].tolist()[0]
    return user


async def get_current_user(token: str = Depends(oauth2_scheme)):
    user = get_user_from_token(token)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Invalid authentication credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return user


async def get_current_active_user(current_user: User = Depends(get_current_user)):
    if current_user.disabled:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user

## read authorised users
df_users = pd.read_csv(AUTHORISED_USERS_FILE)
users_db = df_users.to_dict('list')

## initiate API
app = FastAPI(title='ESGnie API')


@app.get('/')
async def read_user_profile(current_user: User = Depends(get_current_active_user)):
    return current_user


@app.post("/token")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    user_dict = users_db.get(form_data.username)
    if not user_dict:
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    user = UserInDB(**user_dict)
    hashed_password = hash_password(form_data.password)
    if not hashed_password == user.hashed_password:
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    ## ToDo: change access token as uuid
    return {"access_token": user.token,
            "token_type": "bearer"}


@app.get("/data")
def read_data(db, dataset,
              current_user: User = Depends(get_current_user)):
    if db == 'gcf':
        output = read_data(dataset=dataset)
    else:
        output = {'msg': 'database not found'}
    return output


@app.get("/qa")
def qa(q: str,
       passage: str,
       qa_type: str = 'mixed_qa'):
    results = eval(qa_type)(q=q, passage=passage)
    return results


@app.post("/task")
def task(query: str,
         current_user: User = Depends(get_current_user)):
    """
    Schedule a task (in the background) based on the user query
    :param query:
    :param current_user:
    :return:
    """
    task_id = str(uuid.uuid4())
    ## split query
    query = urllib.parse.unquote_plus(query)
    query_json = json.dumps(urllib.parse.parse_qs(query))
    query_dict = json.loads(query_json)
    # print(query_dict)
    dir_task = f"{DIR_OUTPUT}/{task_id}"
    os.makedirs(dir_task, exist_ok=True)
    ## pick a module to run based on task
    if ('analyse' in query_dict['task']) and \
            ('qa' in query_dict['objective']):
        # print('inside if condition')
        python = f'{DIR_VENV}/qa-genie/bin/python'
        module = f'{DIR_CODE}/qa-genie/qa.py'
        output_file = f'{dir_task}/{task_id}_output.csv'
        ## start a process in the background
        out = subprocess.Popen(['nohup',
                                python, module,
                                '-o', output_file,
                                '-q', query_dict['question'][0],
                                '-p', query_dict['passage'][0],
                                '&'
                                ])
        return {'query': query,
                'task_id': task_id,
                'pid': out.pid}


@app.get("/task")
def task(task_id: str,
         pid: int,
         current_user: User = Depends(get_current_user)):
    """
    Read task output based on input task and process id
    :param task_id: task id
    :param pid: process id
    :param current_user: user
    :return:
    """
    output_file = f"{DIR_OUTPUT}/{task_id}/{task_id}_output.csv"
    if os.path.isfile(output_file):
        df_output = pd.read_csv(output_file)
        df_output = df_output.reset_index(drop=True)
        output = df_output.to_dict('list')
    else:
        ## check the status of pid
        if psutil.pid_exists(pid):
            output = {'status': 'Task still running.'}
        else:
            output = {'status': 'Task failed to produce any output.'}
    return output


@app.get('/parse')
def extract_quants(url: str):
    task_id = str(uuid.uuid4())
    dir_task = f"{DIR_OUTPUT}/{task_id}"
    os.makedirs(dir_task, exist_ok=True)
    pdf_dir = f"{dir_task}/pdf"
    os.makedirs(pdf_dir, exist_ok=True)
    ## downlod pdf file
    resp = requests.get(url)
    with open(f'{pdf_dir}/{task_id}.pdf', 'wb') as f:
        f.write(resp.content)
    ## extract pdf quants
    print('starting quant extraction:')
    extract_pdf_quants(root_dir=dir_task,
                       file_types=['words', 'quants'],
                       parallel=0,
                       verbose=1)
    ## segment words files
    print('starting word segmentation:')
    segment_word_files(root_dir=dir_task,
                       file_pattern='df_quants_',
                       verbose=1,
                       parallel=0,
                       n_cpu_frac=1,
                       max_files=-1,
                       reverse=0)
    ## get value context
    print('starting to value context:')
    extract_pdf_quants(root_dir=dir_task,
                       file_types=['context'],
                       parallel=0,
                       verbose=1)
    ## value context
    l_value_files = [file for file in list_full_path(dir_task, recursive=True)
                     if re.search('_value-.*?.csv', file)]
    ## value dataframe (variable, unit, date, value)
    df_val = pd.DataFrame()
    for file in l_value_files:
        df_ = pd.read_csv(file)
        df_val = pd.concat([df_val, df_])
    df_val = df_val.reset_index(drop=True)
    df_val = df_val.fillna('')
    return df_val.to_dict('list')


@app.get('/homepage')
def homepage(name: str):
    df_url = find_homepage(name)
    return df_url.to_dict('list')


@app.get('/documents')
def source_documents(keyword, url, filetype='pdf'):
    df_urls = find_page_urls(keyword, url, filetype='pdf')
    return df_urls['href'].unique().tolist()


## run api: sudo uvicorn main:app --reload
